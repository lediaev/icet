


typedef char                IceTInt8;
typedef unsigned char       IceTUnsignedInt8;
typedef short               IceTInt16;
typedef unsigned short      IceTUnsignedInt16;
typedef int                 IceTInt32;
typedef unsigned int        IceTUnsignedInt32;
typedef long                IceTInt64;
typedef unsigned long       IceTUnsignedInt64;
typedef float               IceTFloat32;
typedef double              IceTFloat64;
typedef IceTInt64           IceTPointerArithmetic;
typedef long unsigned int   size_t;
typedef int                 wchar_t;
typedef struct
{
  int quot;
  int rem;
} div_t;
typedef struct
{
  long int quot;
  long int rem;
} ldiv_t;
extern size_t __ctype_get_mb_cur_max(void) __attribute__ ((__nothrow__));
extern double atof(__const char *__nptr)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1)));
extern int atoi(__const char *__nptr)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1)));
extern long int atol(__const char *__nptr)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1)));
extern double strtod(__const char *__restrict __nptr,
                     char **__restrict __endptr)
__attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1)));
extern long int strtol(__const char *__restrict __nptr,
                       char **__restrict __endptr,int __base)
__attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1)));
extern unsigned long int strtoul(__const char *__restrict __nptr,
                                 char **__restrict __endptr,int __base)
__attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1)));
extern int rand(void) __attribute__ ((__nothrow__));
extern void srand(unsigned int __seed) __attribute__ ((__nothrow__));
extern void *malloc(size_t __size) __attribute__ ((__nothrow__)) __attribute__ ((__malloc__));
extern void *calloc(size_t __nmemb,size_t __size)
__attribute__ ((__nothrow__)) __attribute__ ((__malloc__));
extern void *realloc(void *__ptr,size_t __size)
__attribute__ ((__nothrow__)) __attribute__ ((__warn_unused_result__));
extern void free(void *__ptr) __attribute__ ((__nothrow__));
extern void abort(void) __attribute__ ((__nothrow__)) __attribute__ ((__noreturn__));
extern int atexit(void (*__func)(void)) __attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1)));
extern void exit(int __status) __attribute__ ((__nothrow__)) __attribute__ ((__noreturn__));
extern char *getenv(__const char *__name) __attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1)));
extern char *__secure_getenv(__const char *__name)
__attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1)));
extern int system(__const char *__command);
typedef int (*__compar_fn_t) (__const void *,__const void *);
extern void *bsearch(__const void *__key,__const void *__base,
                     size_t __nmemb,size_t __size,__compar_fn_t __compar)
__attribute__ ((__nonnull__(1,2,5)));
extern void qsort(void *__base,size_t __nmemb,size_t __size,
                  __compar_fn_t __compar) __attribute__ ((__nonnull__(1,4)));
extern int abs(int __x) __attribute__ ((__nothrow__)) __attribute__ ((__const__));
extern long int labs(long int __x) __attribute__ ((__nothrow__)) __attribute__ ((__const__));
extern div_t div(int __numer,int __denom)
__attribute__ ((__nothrow__)) __attribute__ ((__const__));
extern ldiv_t ldiv(long int __numer,long int __denom)
__attribute__ ((__nothrow__)) __attribute__ ((__const__));
extern int mblen(__const char *__s,size_t __n) __attribute__ ((__nothrow__));
extern int mbtowc(wchar_t *__restrict __pwc,
                  __const char *__restrict __s,size_t __n) __attribute__ ((__nothrow__));
extern int wctomb(char *__s,wchar_t __wchar) __attribute__ ((__nothrow__));
extern size_t mbstowcs(wchar_t *__restrict __pwcs,
                       __const char *__restrict __s,size_t __n) __attribute__ ((__nothrow__));
extern size_t wcstombs(char *__restrict __s,
                       __const wchar_t *__restrict __pwcs,size_t __n)
__attribute__ ((__nothrow__));
typedef IceTUnsignedInt32          IceTEnum;
typedef IceTUnsignedInt32          IceTBitField;
typedef IceTFloat64                IceTDouble;
typedef IceTFloat32                IceTFloat;
typedef IceTInt32                  IceTInt;
typedef IceTUnsignedInt32          IceTUInt;
typedef IceTInt16                  IceTShort;
typedef IceTUnsignedInt16          IceTUShort;
typedef IceTInt8                   IceTByte;
typedef IceTUnsignedInt8           IceTUByte;
typedef IceTUnsignedInt8           IceTBoolean;
typedef void                       IceTVoid;
typedef IceTInt32                  IceTSizeType;
struct IceTContextStruct;
typedef struct IceTContextStruct * IceTContext;
struct IceTCommunicatorStruct;
typedef struct IceTCommRequestStruct
{
  IceTEnum magic_number;
  IceTVoid *internals;
} *IceTCommRequest;
struct IceTCommunicatorStruct
{
  struct IceTCommunicatorStruct *
       (*Duplicate)(struct IceTCommunicatorStruct *self);
  void (*Destroy)(struct IceTCommunicatorStruct *self);
  struct IceTCommunicatorStruct *
       (*Subset)(struct IceTCommunicatorStruct *self,
            int count,
            const IceTInt32 *ranks);
  void (*Barrier)(struct IceTCommunicatorStruct *self);
  void (*Send)(struct IceTCommunicatorStruct *self,
               const void *buf,
               int count,
               IceTEnum datatype,
               int dest,
               int tag);
  void (*Recv)(struct IceTCommunicatorStruct *self,
               void *buf,
               int count,
               IceTEnum datatype,
               int src,
               int tag);
  void (*Sendrecv)(struct IceTCommunicatorStruct *self,
                   const void *sendbuf,
                   int sendcount,
                   IceTEnum sendtype,
                   int dest,
                   int sendtag,
                   void *recvbuf,
                   int recvcount,
                   IceTEnum recvtype,
                   int src,
                   int recvtag);
  void (*Gather)(struct IceTCommunicatorStruct *self,
                 const void *sendbuf,
                 int sendcount,
                 IceTEnum datatype,
                 void *recvbuf,
                 int root);
  void (*Gatherv)(struct IceTCommunicatorStruct *self,
                  const void *sendbuf,
                  int sendcount,
                  IceTEnum datatype,
                  void *recvbuf,
                  const int *recvcounts,
                  const int *recvoffsets,
                  int root);
  void (*Allgather)(struct IceTCommunicatorStruct *self,
                    const void *sendbuf,
                    int sendcount,
                    IceTEnum datatype,
                    void *recvbuf);
  void (*Alltoall)(struct IceTCommunicatorStruct *self,
                   const void *sendbuf,
                   int sendcount,
                   IceTEnum datatype,
                   void *recvbuf);
  IceTCommRequest (*Isend)(struct IceTCommunicatorStruct *self,
                           const void *buf,
                           int count,
                           IceTEnum datatype,
                           int dest,
                           int tag);
  IceTCommRequest (*Irecv)(struct IceTCommunicatorStruct *self,
                           void *buf,
                           int count,
                           IceTEnum datatype,
                           int src,
                           int tag);
  void (*Wait)(struct IceTCommunicatorStruct *self,IceTCommRequest *request);
  int  (*Waitany)(struct IceTCommunicatorStruct *self,
                  int count,IceTCommRequest *array_of_requests);
  int  (*Comm_size)(struct IceTCommunicatorStruct *self);
  int  (*Comm_rank)(struct IceTCommunicatorStruct *self);
  void *data;
};
typedef struct IceTCommunicatorStruct * IceTCommunicator;
IceTDouble icetWallTime(void);
IceTContext icetCreateContext(IceTCommunicator comm);
void icetDestroyContext(IceTContext context);
IceTContext icetGetContext(void);
void icetSetContext(IceTContext context);
void icetCopyState(IceTContext dest,const IceTContext src);
void icetSetMatt(int *);
void icetBoundingVertices(IceTInt size,IceTEnum type,
                          IceTSizeType stride,IceTSizeType count,
                          const IceTVoid *pointer);
void icetBoundingBoxd(IceTDouble x_min,IceTDouble x_max,
                      IceTDouble y_min,IceTDouble y_max,
                      IceTDouble z_min,IceTDouble z_max);
void icetBoundingBoxf(IceTFloat x_min,IceTFloat x_max,
                      IceTFloat y_min,IceTFloat y_max,
                      IceTFloat z_min,IceTFloat z_max);
void icetResetTiles(void);
int icetAddTile(IceTInt x,IceTInt y,
                IceTSizeType width,IceTSizeType height,
                int display_rank);
void icetPhysicalRenderSize(IceTInt width,IceTInt height);
typedef struct {IceTVoid *opaque_internals;
} IceTImage;
void icetSetColorFormat(IceTEnum color_format);
void icetSetDepthFormat(IceTEnum depth_format);
IceTImage icetImageNull(void);
IceTBoolean icetImageIsNull(const IceTImage image);
IceTEnum icetImageGetColorFormat(const IceTImage image);
IceTEnum icetImageGetDepthFormat(const IceTImage image);
IceTSizeType icetImageGetWidth(const IceTImage image);
IceTSizeType icetImageGetHeight(const IceTImage image);
IceTSizeType icetImageGetNumPixels(const IceTImage image);
IceTUByte *icetImageGetColorub(IceTImage image);
IceTUInt *icetImageGetColorui(IceTImage image);
IceTFloat *icetImageGetColorf(IceTImage image);
IceTFloat *icetImageGetDepthf(IceTImage image);
const IceTUByte *icetImageGetColorcub(const IceTImage image);
const IceTUInt *icetImageGetColorcui(const IceTImage image);
const IceTFloat *icetImageGetColorcf(const IceTImage image);
const IceTFloat *icetImageGetDepthcf(const IceTImage image);
void icetImageCopyColorub(const IceTImage image,
                          IceTUByte *color_buffer,
                          IceTEnum color_format);
void icetImageCopyColorf(const IceTImage image,
                         IceTFloat *color_buffer,
                         IceTEnum color_format);
void icetImageCopyDepthf(const IceTImage image,
                         IceTFloat *depth_buffer,
                         IceTEnum depth_format);
void icetStrategy(IceTEnum strategy);
const char *icetGetStrategyName(void);
void icetSingleImageStrategy(IceTEnum strategy);
const char *icetGetSingleImageStrategyName(void);
void icetCompositeMode(IceTEnum mode);
void icetCompositeOrder(const IceTInt *process_ranks);
void icetDataReplicationGroup(IceTInt size,
                              const IceTInt *processes);
void icetDataReplicationGroupColor(IceTInt color);
typedef void (*IceTDrawCallbackType)(const IceTDouble *projection_matrix,
                                     const IceTDouble *modelview_matrix,
                                     const IceTFloat *background_color,
                                     const IceTInt *readback_viewport,
                                     IceTImage result);
void icetDrawCallback(IceTDrawCallbackType callback);
IceTImage icetDrawFrame(const IceTDouble *projection_matrix,
                        const IceTDouble *modelview_matrix,
                        const IceTFloat *background_color);
IceTImage icetCompositeImage(const IceTVoid *color_buffer,
                             const IceTVoid *depth_buffer,
                             const IceTInt *valid_pixels_viewport,
                             const IceTDouble *projection_matrix,
                             const IceTDouble *modelview_matrix,
                             const IceTFloat *background_color);
void icetDiagnostics(IceTBitField mask);
void icetGetDoublev(IceTEnum pname,IceTDouble *params);
void icetGetFloatv(IceTEnum pname,IceTFloat *params);
void icetGetIntegerv(IceTEnum pname,IceTInt *params);
void icetGetBooleanv(IceTEnum pname,IceTBoolean *params);
void icetGetEnumv(IceTEnum pname,IceTEnum *params);
void icetGetBitFieldv(IceTEnum pname,IceTBitField *bitfield);
void icetGetPointerv(IceTEnum pname,IceTVoid **params);
void icetEnable(IceTEnum pname);
void icetDisable(IceTEnum pname);
IceTBoolean icetIsEnabled(IceTEnum pname);
IceTEnum icetGetError(void);
typedef IceTUnsignedInt64       IceTTimeStamp;
struct IceTStateValue;
typedef struct IceTStateValue * IceTState;
struct IceTStateValue
{
  IceTEnum      type;
  IceTSizeType  num_entries;
  IceTSizeType  buffer_size;
  void          *data;
  IceTTimeStamp mod_time;
};
struct IceTContextStruct
{
  IceTEnum         magic_number;
  IceTState        state;
  IceTCommunicator communicator;
};
IceTState icetStateCreate(void);
void icetStateDestroy(IceTState state);
void icetStateCopy(IceTState dest,const IceTState src);
void icetStateSetDefaults(void);
void icetStateCheckMemory(void);
void icetStateSetDoublev(IceTEnum pname,
                         IceTSizeType num_entries,
                         const IceTDouble *data);
void icetStateSetFloatv(IceTEnum pname,
                        IceTSizeType num_entries,
                        const IceTFloat *data);
void icetStateSetIntegerv(IceTEnum pname,
                          IceTSizeType num_entries,
                          const IceTInt *data);
void icetStateSetBooleanv(IceTEnum pname,
                          IceTSizeType num_entries,
                          const IceTBoolean *data);
void icetStateSetPointerv(IceTEnum pname,
                          IceTSizeType num_entries,
                          const IceTVoid **data);
void icetStateSetDouble(IceTEnum pname,IceTDouble value);
void icetStateSetFloat(IceTEnum pname,IceTFloat value);
void icetStateSetInteger(IceTEnum pname,IceTInt value);
void icetStateSetBoolean(IceTEnum pname,IceTBoolean value);
void icetStateSetPointer(IceTEnum pname,const IceTVoid *value);
IceTEnum icetStateGetType(IceTEnum pname);
IceTSizeType icetStateGetNumEntries(IceTEnum pname);
IceTTimeStamp icetStateGetTime(IceTEnum pname);
const IceTDouble *icetUnsafeStateGetDouble(IceTEnum pname);
const IceTFloat *icetUnsafeStateGetFloat(IceTEnum pname);
const IceTInt *icetUnsafeStateGetInteger(IceTEnum pname);
const IceTBoolean *icetUnsafeStateGetBoolean(IceTEnum pname);
const IceTVoid **icetUnsafeStateGetPointer(IceTEnum pname);
const IceTVoid *icetUnsafeStateGetBuffer(IceTEnum pname);
IceTDouble *icetStateAllocateDouble(IceTEnum pname,
                                    IceTSizeType num_entries);
IceTFloat *icetStateAllocateFloat(IceTEnum pname,
                                  IceTSizeType num_entries);
IceTInt *icetStateAllocateInteger(IceTEnum pname,
                                  IceTSizeType num_entries);
IceTBoolean *icetStateAllocateBoolean(IceTEnum pname,
                                      IceTSizeType num_entries);
IceTVoid **icetStateAllocatePointer(IceTEnum pname,
                                    IceTSizeType num_entries);
IceTVoid *icetGetStateBuffer(IceTEnum pname,
                             IceTSizeType num_bytes);
IceTTimeStamp icetGetTimeStamp(void);
void icetStateDump(void);
IceTImage icetGetStateBufferImage(IceTEnum pname,
                                  IceTSizeType width,
                                  IceTSizeType height);
IceTImage icetRetrieveStateImage(IceTEnum pname);
IceTSizeType icetImageBufferSize(IceTSizeType width,
                                 IceTSizeType height);
IceTSizeType icetImageBufferSizeType(IceTEnum color_format,
                                     IceTEnum depth_format,
                                     IceTSizeType width,
                                     IceTSizeType height);
IceTImage icetImageAssignBuffer(IceTVoid *buffer,
                                IceTSizeType width,
                                IceTSizeType height);
IceTImage icetGetStatePointerImage(IceTEnum pname,
                                   IceTSizeType width,
                                   IceTSizeType height,
                                   const IceTVoid *color_buffer,
                                   const IceTVoid *depth_buffer);
IceTSizeType icetImagePointerBufferSize(void);
IceTImage icetImagePointerAssignBuffer(IceTVoid *buffer,
                                       IceTSizeType width,
                                       IceTSizeType height,
                                       const IceTVoid *color_buf,
                                       const IceTVoid *depth_buf);
void icetImageAdjustForOutput(IceTImage image);
void icetImageAdjustForInput(IceTImage image);
void icetImageSetDimensions(IceTImage image,
                            IceTSizeType width,
                            IceTSizeType height);
IceTVoid *icetImageGetColorVoid(IceTImage image,
                                IceTSizeType *pixel_size);
const IceTVoid *icetImageGetColorConstVoid(
  const IceTImage image,
  IceTSizeType *pixel_size);
IceTVoid *icetImageGetDepthVoid(IceTImage image,
                                IceTSizeType *pixel_size);
const IceTVoid *icetImageGetDepthConstVoid(
  const IceTImage image,
  IceTSizeType *pixel_size);
IceTBoolean icetImageEqual(const IceTImage image1,
                           const IceTImage image2);
void icetImageCopyPixels(const IceTImage in_image,
                         IceTSizeType in_offset,
                         IceTImage out_image,
                         IceTSizeType out_offset,
                         IceTSizeType num_pixels);
void icetImageCopyRegion(const IceTImage in_image,
                         const IceTInt *in_viewport,
                         IceTImage out_image,
                         const IceTInt *out_viewport);
void icetImageClearAroundRegion(IceTImage image,
                                const IceTInt *region);
void icetImagePackageForSend(IceTImage image,
                             IceTVoid **buffer,
                             IceTSizeType *size);
IceTImage icetImageUnpackageFromReceive(IceTVoid *buffer);
typedef struct {IceTVoid *opaque_internals;
} IceTSparseImage;
IceTSizeType icetSparseImageBufferSize(IceTSizeType width,
                                       IceTSizeType height);
IceTSizeType icetSparseImageBufferSizeType(IceTEnum color_format,
                                           IceTEnum depth_format,
                                           IceTSizeType width,
                                           IceTSizeType height);
IceTSparseImage icetGetStateBufferSparseImage(IceTEnum pname,
                                              IceTSizeType width,
                                              IceTSizeType height);
IceTSparseImage icetSparseImageAssignBuffer(IceTVoid *buffer,
                                            IceTSizeType width,
                                            IceTSizeType height);
IceTSparseImage icetSparseImageNull(void);
IceTBoolean icetSparseImageIsNull(const IceTSparseImage image);
IceTEnum icetSparseImageGetColorFormat(const IceTSparseImage image);
IceTEnum icetSparseImageGetDepthFormat(const IceTSparseImage image);
IceTSizeType icetSparseImageGetWidth(const IceTSparseImage image);
IceTSizeType icetSparseImageGetHeight(const IceTSparseImage image);
IceTSizeType icetSparseImageGetNumPixels(
  const IceTSparseImage image);
void icetSparseImageSetDimensions(IceTSparseImage image,
                                  IceTSizeType width,
                                  IceTSizeType height);
IceTSizeType icetSparseImageGetCompressedBufferSize(
  const IceTSparseImage image);
void icetSparseImagePackageForSend(IceTSparseImage image,
                                   IceTVoid **buffer,
                                   IceTSizeType *size);
IceTSparseImage icetSparseImageUnpackageFromReceive(
  IceTVoid *buffer);
IceTBoolean icetSparseImageEqual(const IceTSparseImage image1,
                                 const IceTSparseImage image2);
void icetSparseImageCopyPixels(const IceTSparseImage in_image,
                               IceTSizeType in_offset,
                               IceTSizeType num_pixels,
                               IceTSparseImage out_image);
void icetSparseImageSplit(const IceTSparseImage in_image,
                          IceTSizeType in_image_offset,
                          IceTInt num_partitions,
                          IceTInt eventual_num_partitions,
                          IceTSparseImage *out_images,
                          IceTSizeType *offsets);
IceTSizeType icetSparseImageSplitPartitionNumPixels(
  IceTSizeType input_num_pixels,
  IceTInt num_partitions,
  IceTInt eventual_num_partitions);
void icetSparseImageInterlace(const IceTSparseImage in_image,
                              IceTInt eventual_num_partitions,
                              IceTEnum scratch_state_buffer,
                              IceTSparseImage out_image);
IceTSizeType icetGetInterlaceOffset(
  IceTInt partition_index,
  IceTInt eventual_num_partitions,
  IceTSizeType original_image_size);
void icetClearImage(IceTImage image);
void icetClearSparseImage(IceTSparseImage image);
void icetGetTileImage(IceTInt tile,IceTImage image);
void icetGetCompressedTileImage(IceTInt tile,
                                IceTSparseImage compressed_image);
void icetCompressImage(const IceTImage image,
                       IceTSparseImage compressed_image);
void icetCompressSubImage(const IceTImage image,
                          IceTSizeType offset,
                          IceTSizeType pixels,
                          IceTSparseImage compressed_image);
void icetDecompressImage(const IceTSparseImage compressed_image,
                         IceTImage image);
void icetDecompressSubImage(const IceTSparseImage compressed_image,
                            IceTSizeType offset,
                            IceTImage image);
void icetDecompressImageCorrectBackground(
  const IceTSparseImage compressed_image,
  IceTImage image);
void icetDecompressSubImageCorrectBackground(
  const IceTSparseImage compressed_image,
  IceTSizeType offset,
  IceTImage image);
void icetComposite(IceTImage destBuffer,
                   const IceTImage srcBuffer,
                   int srcOnTop);
void icetCompressedComposite(IceTImage destBuffer,
                             const IceTSparseImage srcBuffer,
                             int srcOnTop);
void icetCompressedSubComposite(IceTImage destBuffer,
                                IceTSizeType offset,
                                const IceTSparseImage srcBuffer,
                                int srcOnTop);
void icetCompressedCompressedComposite(
  const IceTSparseImage front_buffer,
  const IceTSparseImage back_buffer,
  IceTSparseImage dest_buffer);
void icetImageCorrectBackground(IceTImage image);
void icetClearImageTrueBackground(IceTImage image);
void icetProjectTile(IceTInt tile,IceTDouble *mat_out);
void icetGetViewportProject(IceTInt x,IceTInt y,
                            IceTSizeType width,IceTSizeType height,
                            IceTDouble *mat_out);
void icetIntersectViewports(const IceTInt *src_viewport1,
                            const IceTInt *src_viewport2,
                            IceTInt *dest_viewport);
typedef unsigned char        __u_char;
typedef unsigned short int   __u_short;
typedef unsigned int         __u_int;
typedef unsigned long int    __u_long;
typedef signed char          __int8_t;
typedef unsigned char        __uint8_t;
typedef signed short int     __int16_t;
typedef unsigned short int   __uint16_t;
typedef signed int           __int32_t;
typedef unsigned int         __uint32_t;
typedef signed long int      __int64_t;
typedef unsigned long int    __uint64_t;
typedef long int             __quad_t;
typedef unsigned long int    __u_quad_t;
typedef unsigned long int    __dev_t;
typedef unsigned int         __uid_t;
typedef unsigned int         __gid_t;
typedef unsigned long int    __ino_t;
typedef unsigned long int    __ino64_t;
typedef unsigned int         __mode_t;
typedef unsigned long int    __nlink_t;
typedef long int             __off_t;
typedef long int             __off64_t;
typedef int                  __pid_t;
typedef struct {int __val[2];
} __fsid_t;
typedef long int             __clock_t;
typedef unsigned long int    __rlim_t;
typedef unsigned long int    __rlim64_t;
typedef unsigned int         __id_t;
typedef long int             __time_t;
typedef unsigned int         __useconds_t;
typedef long int             __suseconds_t;
typedef int                  __daddr_t;
typedef long int             __swblk_t;
typedef int                  __key_t;
typedef int                  __clockid_t;
typedef void *               __timer_t;
typedef long int             __blksize_t;
typedef long int             __blkcnt_t;
typedef long int             __blkcnt64_t;
typedef unsigned long int    __fsblkcnt_t;
typedef unsigned long int    __fsblkcnt64_t;
typedef unsigned long int    __fsfilcnt_t;
typedef unsigned long int    __fsfilcnt64_t;
typedef long int             __ssize_t;
typedef __off64_t            __loff_t;
typedef __quad_t *           __qaddr_t;
typedef char *               __caddr_t;
typedef long int             __intptr_t;
typedef unsigned int         __socklen_t;
struct _IO_FILE;
typedef struct _IO_FILE      FILE;
typedef struct _IO_FILE      __FILE;
typedef struct
{
  int __count;
  union
  {
    unsigned int __wch;
    char         __wchb[4];
  } __value;
} __mbstate_t;
typedef struct
{
  __off_t     __pos;
  __mbstate_t __state;
} _G_fpos_t;
typedef struct
{
  __off64_t   __pos;
  __mbstate_t __state;
} _G_fpos64_t;
typedef int                 _G_int16_t __attribute__ ((__mode__(__HI__)));
typedef int                 _G_int32_t __attribute__ ((__mode__(__SI__)));
typedef unsigned int        _G_uint16_t __attribute__ ((__mode__(__HI__)));
typedef unsigned int        _G_uint32_t __attribute__ ((__mode__(__SI__)));
typedef __builtin_va_list   __gnuc_va_list;
struct _IO_jump_t;
struct _IO_FILE;
typedef void                _IO_lock_t;
struct _IO_marker
{
  struct _IO_marker *_next;
  struct _IO_FILE   *_sbuf;
  int               _pos;
};
enum __codecvt_result
{
  __codecvt_ok,
  __codecvt_partial,
  __codecvt_error,
  __codecvt_noconv
};
struct _IO_FILE
{
  int               _flags;
  char              * _IO_read_ptr;
  char              * _IO_read_end;
  char              * _IO_read_base;
  char              * _IO_write_base;
  char              * _IO_write_ptr;
  char              * _IO_write_end;
  char              * _IO_buf_base;
  char              * _IO_buf_end;
  char              *_IO_save_base;
  char              *_IO_backup_base;
  char              *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE   *_chain;
  int               _fileno;
  int               _flags2;
  __off_t           _old_offset;
  unsigned short    _cur_column;
  signed char       _vtable_offset;
  char              _shortbuf[1];
  _IO_lock_t        *_lock;
  __off64_t         _offset;
  void              *__pad1;
  void              *__pad2;
  void              *__pad3;
  void              *__pad4;
  size_t            __pad5;
  int               _mode;
  char              _unused2[15*sizeof(int)-4*sizeof(void *)-sizeof(size_t)];
};
typedef struct _IO_FILE   _IO_FILE;
struct _IO_FILE_plus;
extern struct _IO_FILE_plus _IO_2_1_stdin_;
extern struct _IO_FILE_plus _IO_2_1_stdout_;
extern struct _IO_FILE_plus _IO_2_1_stderr_;
typedef __ssize_t __io_read_fn (void *__cookie,char *__buf,size_t __nbytes);
typedef __ssize_t __io_write_fn (void *__cookie,__const char *__buf,
                                 size_t __n);
typedef int __io_seek_fn (void *__cookie,__off64_t *__pos,int __w);
typedef int __io_close_fn (void *__cookie);
extern int __underflow(_IO_FILE *);
extern int __uflow(_IO_FILE *);
extern int __overflow(_IO_FILE *,int);
extern int _IO_getc(_IO_FILE *__fp);
extern int _IO_putc(int __c,_IO_FILE *__fp);
extern int _IO_feof(_IO_FILE *__fp) __attribute__ ((__nothrow__));
extern int _IO_ferror(_IO_FILE *__fp) __attribute__ ((__nothrow__));
extern int _IO_peekc_locked(_IO_FILE *__fp);
extern void _IO_flockfile(_IO_FILE *) __attribute__ ((__nothrow__));
extern void _IO_funlockfile(_IO_FILE *) __attribute__ ((__nothrow__));
extern int _IO_ftrylockfile(_IO_FILE *) __attribute__ ((__nothrow__));
extern int _IO_vfscanf(_IO_FILE * __restrict,const char * __restrict,
                       __gnuc_va_list,int *__restrict);
extern int _IO_vfprintf(_IO_FILE *__restrict,const char *__restrict,
                        __gnuc_va_list);
extern __ssize_t _IO_padn(_IO_FILE *,int,__ssize_t);
extern size_t _IO_sgetn(_IO_FILE *,void *,size_t);
extern __off64_t _IO_seekoff(_IO_FILE *,__off64_t,int,int);
extern __off64_t _IO_seekpos(_IO_FILE *,__off64_t,int);
extern void _IO_free_backup_area(_IO_FILE *) __attribute__ ((__nothrow__));
typedef _G_fpos_t   fpos_t;
extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;
extern int remove(__const char *__filename) __attribute__ ((__nothrow__));
extern int rename(__const char *__old,__const char *__new) __attribute__ ((__nothrow__));
extern FILE *tmpfile(void);
extern char *tmpnam(char *__s) __attribute__ ((__nothrow__));
extern int fclose(FILE *__stream);
extern int fflush(FILE *__stream);
extern FILE *fopen(__const char *__restrict __filename,
                   __const char *__restrict __modes);
extern FILE *freopen(__const char *__restrict __filename,
                     __const char *__restrict __modes,
                     FILE *__restrict __stream);
extern void setbuf(FILE *__restrict __stream,char *__restrict __buf) __attribute__ ((__nothrow__));
extern int setvbuf(FILE *__restrict __stream,char *__restrict __buf,
                   int __modes,size_t __n) __attribute__ ((__nothrow__));
extern int fprintf(FILE *__restrict __stream,
                   __const char *__restrict __format,...);
extern int printf(__const char *__restrict __format,...);
extern int sprintf(char *__restrict __s,
                   __const char *__restrict __format,...) __attribute__ ((__nothrow__));
extern int vfprintf(FILE *__restrict __s,__const char *__restrict __format,
                    __gnuc_va_list __arg);
extern int vprintf(__const char *__restrict __format,__gnuc_va_list __arg);
extern int vsprintf(char *__restrict __s,__const char *__restrict __format,
                    __gnuc_va_list __arg) __attribute__ ((__nothrow__));
extern int fscanf(FILE *__restrict __stream,
                  __const char *__restrict __format,...);
extern int scanf(__const char *__restrict __format,...);
extern int sscanf(__const char *__restrict __s,
                  __const char *__restrict __format,...) __attribute__ ((__nothrow__));
extern int fgetc(FILE *__stream);
extern int getc(FILE *__stream);
extern int getchar(void);
extern int fputc(int __c,FILE *__stream);
extern int putc(int __c,FILE *__stream);
extern int putchar(int __c);
extern char *fgets(char *__restrict __s,int __n,FILE *__restrict __stream)
;
extern char *gets(char *__s);
extern int fputs(__const char *__restrict __s,FILE *__restrict __stream);
extern int puts(__const char *__s);
extern int ungetc(int __c,FILE *__stream);
extern size_t fread(void *__restrict __ptr,size_t __size,
                    size_t __n,FILE *__restrict __stream);
extern size_t fwrite(__const void *__restrict __ptr,size_t __size,
                     size_t __n,FILE *__restrict __s);
extern int fseek(FILE *__stream,long int __off,int __whence);
extern long int ftell(FILE *__stream);
extern void rewind(FILE *__stream);
extern int fgetpos(FILE *__restrict __stream,fpos_t *__restrict __pos);
extern int fsetpos(FILE *__stream,__const fpos_t *__pos);
extern void clearerr(FILE *__stream) __attribute__ ((__nothrow__));
extern int feof(FILE *__stream) __attribute__ ((__nothrow__));
extern int ferror(FILE *__stream) __attribute__ ((__nothrow__));
extern void perror(__const char *__s);
void icetRaiseDiagnostic(const char *msg,IceTEnum type,
                         IceTBitField level,
                         const char *file,int line);
void icetDebugBreak(void);
void icetMatrixMultiply(IceTDouble *C,
                        const IceTDouble *A,
                        const IceTDouble *B);
void icetMatrixPostMultiply(IceTDouble *A,
                            const IceTDouble *B);
void icetMatrixVectorMultiply(IceTDouble *out,
                              const IceTDouble *A,
                              const IceTDouble *v);
void icetMatrixCopy(IceTDouble *matrix_dest,
                    const IceTDouble *matrix_src);
void icetMatrixIdentity(IceTDouble *mat_out);
void icetMatrixOrtho(IceTDouble left,IceTDouble right,
                     IceTDouble bottom,IceTDouble top,
                     IceTDouble znear,IceTDouble zfar,
                     IceTDouble *mat_out);
void icetMatrixFrustum(IceTDouble left,IceTDouble right,
                       IceTDouble bottom,IceTDouble top,
                       IceTDouble znear,IceTDouble zfar,
                       IceTDouble *mat_out);
void icetMatrixScale(IceTDouble x,IceTDouble y,IceTDouble z,
                     IceTDouble *mat_out);
void icetMatrixTranslate(IceTDouble x,IceTDouble y,IceTDouble z,
                         IceTDouble *mat_out);
void icetMatrixRotate(IceTDouble angle,
                      IceTDouble x,IceTDouble y,IceTDouble z,
                      IceTDouble *mat_out);
void icetMatrixMultiplyScale(IceTDouble *mat_out,
                             IceTDouble x,
                             IceTDouble y,
                             IceTDouble z);
void icetMatrixMultiplyTranslate(IceTDouble *mat_out,
                                 IceTDouble x,
                                 IceTDouble y,
                                 IceTDouble z);
void icetMatrixMultiplyRotate(IceTDouble *mat_out,
                              IceTDouble angle,
                              IceTDouble x,
                              IceTDouble y,
                              IceTDouble z);
IceTBoolean icetMatrixInverse(const IceTDouble *matrix_in,
                              IceTDouble *matrix_out);
void icetMatrixTranspose(const IceTDouble *matrix_in,
                         IceTDouble *matrix_out);
IceTBoolean icetMatrixInverseTranspose(const IceTDouble *matrix_in,
                                       IceTDouble *matrix_out);
void icetStateResetTiming(void);
void icetTimingRenderBegin(void);
void icetTimingRenderEnd(void);
void icetTimingBufferReadBegin(void);
void icetTimingBufferReadEnd(void);
void icetTimingBufferWriteBegin(void);
void icetTimingBufferWriteEnd(void);
void icetTimingCompressBegin(void);
void icetTimingCompressEnd(void);
void icetTimingInterlaceBegin(void);
void icetTimingInterlaceEnd(void);
void icetTimingBlendBegin(void);
void icetTimingBlendEnd(void);
void icetTimingCollectBegin(void);
void icetTimingCollectEnd(void);
void icetTimingDrawFrameBegin(void);
void icetTimingDrawFrameEnd(void);
extern void *memcpy(void *__restrict __dest,
                    __const void *__restrict __src,size_t __n)
__attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1,2)));
extern void *memmove(void *__dest,__const void *__src,size_t __n)
__attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1,2)));
extern void *memset(void *__s,int __c,size_t __n) __attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1)));
extern int memcmp(__const void *__s1,__const void *__s2,size_t __n)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1,2)));
extern void *memchr(__const void *__s,int __c,size_t __n)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1)));
extern char *strcpy(char *__restrict __dest,__const char *__restrict __src)
__attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1,2)));
extern char *strncpy(char *__restrict __dest,
                     __const char *__restrict __src,size_t __n)
__attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1,2)));
extern char *strcat(char *__restrict __dest,__const char *__restrict __src)
__attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1,2)));
extern char *strncat(char *__restrict __dest,__const char *__restrict __src,
                     size_t __n) __attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1,2)));
extern int strcmp(__const char *__s1,__const char *__s2)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1,2)));
extern int strncmp(__const char *__s1,__const char *__s2,size_t __n)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1,2)));
extern int strcoll(__const char *__s1,__const char *__s2)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1,2)));
extern size_t strxfrm(char *__restrict __dest,
                      __const char *__restrict __src,size_t __n)
__attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(2)));
extern char *strchr(__const char *__s,int __c)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1)));
extern char *strrchr(__const char *__s,int __c)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1)));
extern size_t strcspn(__const char *__s,__const char *__reject)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1,2)));
extern size_t strspn(__const char *__s,__const char *__accept)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1,2)));
extern char *strpbrk(__const char *__s,__const char *__accept)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1,2)));
extern char *strstr(__const char *__haystack,__const char *__needle)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1,2)));
extern char *strtok(char *__restrict __s,__const char *__restrict __delim)
__attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(2)));
extern char *__strtok_r(char *__restrict __s,
                        __const char *__restrict __delim,
                        char **__restrict __save_ptr)
__attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(2,3)));
extern size_t strlen(__const char *__s)
__attribute__ ((__nothrow__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__(1)));
extern char *strerror(int __errnum) __attribute__ ((__nothrow__));
extern void __bzero(void *__s,size_t __n) __attribute__ ((__nothrow__)) __attribute__ ((__nonnull__(1)));
typedef IceTUnsignedInt32   IceTRunLengthType;
int * vp_ptr;

void icetSetMatt(int *ptr)
{
  vp_ptr= ptr;
}

static void ICET_TEST_IMAGE_HEADER(IceTImage image)
{
  if (!icetImageIsNull(image))
  {
    IceTEnum magic_num=
      ((IceTInt *) image.opaque_internals)[0];
    if ((magic_num != (IceTEnum) 0x004D5000)
        &&(magic_num != (IceTEnum) 0x004D5100))
    {
      icetRaiseDiagnostic("Detected invalid image header.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,62);
    }
  }
}

static void ICET_TEST_SPARSE_IMAGE_HEADER(IceTSparseImage image)
{
  if (!icetSparseImageIsNull(image))
  {
    if (((IceTInt *) image.opaque_internals)[0]
        != (IceTEnum) 0x004D6000)
    {
      icetRaiseDiagnostic("Detected invalid image header.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,72);
    }
  }
}
static IceTSizeType colorPixelSize(IceTEnum color_format);
static IceTSizeType depthPixelSize(IceTEnum depth_format);
static void icetSparseImageSetActualSize(IceTSparseImage image,
                                         const IceTVoid *data_end);
static void icetSparseImageScanPixels(const IceTVoid **in_data_p,
                                      IceTSizeType *inactive_before_p,
                                      IceTSizeType *active_till_next_runl_p,
                                      IceTVoid **last_in_run_length_p,
                                      IceTSizeType pixels_to_skip,
                                      IceTSizeType pixel_size,
                                      IceTVoid **out_data_p,
                                      IceTVoid **out_run_length_p);
static void icetSparseImageCopyPixelsInternal(
  const IceTVoid **data_p,
  IceTSizeType *inactive_before_p,
  IceTSizeType *active_till_next_runl_p,
  IceTSizeType pixels_to_copy,
  IceTSizeType pixel_size,
  IceTSparseImage out_image);
static void icetSparseImageCopyPixelsInPlaceInternal(
  const IceTVoid **data_p,
  IceTSizeType *inactive_before_p,
  IceTSizeType *active_till_next_runl_p,
  IceTSizeType pixels_to_copy,
  IceTSizeType pixel_size,
  IceTSparseImage out_image);
static void icetSparseImageSplitChoosePartitions(
  IceTInt num_partitions,
  IceTSizeType eventual_num_partitions,
  IceTSizeType size,
  IceTSizeType first_offset,
  IceTSizeType *offsets);
static IceTImage generateTile(int tile,
                              IceTInt *screen_viewport,
                              IceTInt *target_viewport,
                              IceTImage tile_buffer);
static IceTImage renderTile(int tile,
                            IceTInt *screen_viewport,
                            IceTInt *target_viewport,
                            IceTImage tile_buffer);
static IceTImage prerenderedTile(int tile,
                                 IceTInt *screen_viewport,
                                 IceTInt *target_viewport);
static IceTImage getRenderBuffer(void);

static IceTSizeType colorPixelSize(IceTEnum color_format)
{
  switch (color_format)
  {
  case (IceTEnum) 0xC001: return 4;
  case (IceTEnum) 0xC002: return 4*sizeof(IceTFloat);
  case (IceTEnum) 0xC000: return 0;
  default:
    icetRaiseDiagnostic("Invalid color format.",(IceTEnum) 0xFFFFFFFE,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",248);
    return 0;
  }
}

static IceTSizeType depthPixelSize(IceTEnum depth_format)
{
  switch (depth_format)
  {
  case (IceTEnum) 0xD001: return sizeof(IceTFloat);
  case (IceTEnum) 0xD000: return 0;
  default:
    icetRaiseDiagnostic("Invalid depth format.",(IceTEnum) 0xFFFFFFFE,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",259);
    return 0;
  }
}

IceTSizeType icetImageBufferSize(IceTSizeType width,IceTSizeType height)
{
  IceTEnum color_format,depth_format;
  icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0009),&color_format);
  icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x000A),&depth_format);
  return icetImageBufferSizeType(color_format,depth_format,
                                 width,height);
}

IceTSizeType icetImageBufferSizeType(IceTEnum color_format,
                                     IceTEnum depth_format,
                                     IceTSizeType width,
                                     IceTSizeType height)
{
  IceTSizeType color_pixel_size= colorPixelSize(color_format);
  IceTSizeType depth_pixel_size= depthPixelSize(depth_format);
  return(7*sizeof(IceTUInt)
         +width*height*(color_pixel_size+depth_pixel_size));
}

IceTSizeType icetImagePointerBufferSize(void)
{
  return(7*sizeof(IceTUInt)
         +2*(sizeof(const IceTVoid *)));
}

IceTSizeType icetSparseImageBufferSize(IceTSizeType width,IceTSizeType height)
{
  IceTEnum color_format,depth_format;
  icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0009),&color_format);
  icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x000A),&depth_format);
  return icetSparseImageBufferSizeType(color_format,depth_format,
                                       width,height);
}

IceTSizeType icetSparseImageBufferSizeType(IceTEnum color_format,
                                           IceTEnum depth_format,
                                           IceTSizeType width,
                                           IceTSizeType height)
{
  IceTSizeType size;
  IceTSizeType pixel_size;
  size= (((IceTSizeType) (2*sizeof(IceTRunLengthType)))
         +icetImageBufferSizeType(color_format,depth_format,width,height));
  pixel_size= colorPixelSize(color_format)+depthPixelSize(depth_format);
  if (pixel_size < ((IceTSizeType) (2*sizeof(IceTRunLengthType))))
  {
    size+= (((IceTSizeType) (2*sizeof(IceTRunLengthType)))-pixel_size)*((width*height+1)/2);
  }
  return size;
}

IceTImage icetGetStateBufferImage(IceTEnum pname,
                                  IceTSizeType width,
                                  IceTSizeType height)
{
  IceTVoid     *buffer;
  IceTSizeType buffer_size;
  buffer_size= icetImageBufferSize(width,height);
  buffer     = icetGetStateBuffer(pname,buffer_size);
  return icetImageAssignBuffer(buffer,width,height);
}

IceTImage icetRetrieveStateImage(IceTEnum pname)
{
  return icetImageUnpackageFromReceive(
    (IceTVoid *) icetUnsafeStateGetBuffer(pname));
}

IceTImage icetGetStatePointerImage(IceTEnum pname,
                                   IceTSizeType width,
                                   IceTSizeType height,
                                   const IceTVoid *color_buffer,
                                   const IceTVoid *depth_buffer)
{
  IceTVoid     *buffer;
  IceTSizeType buffer_size;
  buffer_size= icetImagePointerBufferSize();
  buffer     = icetGetStateBuffer(pname,buffer_size);
  return icetImagePointerAssignBuffer(
    buffer,width,height,color_buffer,depth_buffer);
}

IceTImage icetImageAssignBuffer(IceTVoid *buffer,
                                IceTSizeType width,
                                IceTSizeType height)
{
  IceTImage image;
  IceTEnum  color_format,depth_format;
  IceTInt   *header;
  image.opaque_internals= buffer;
  if (buffer == ((void *) 0))
  {
    icetRaiseDiagnostic("Tried to create image with NULL buffer.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,382);
    return icetImageNull();
  }
  icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0009),&color_format);
  icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x000A),&depth_format);
  header= ((IceTInt *) image.opaque_internals);
  if ((color_format != (IceTEnum) 0xC001)
      &&(color_format != (IceTEnum) 0xC002)
      &&(color_format != (IceTEnum) 0xC000))
  {
    icetRaiseDiagnostic("Invalid color format.",(IceTEnum) 0xFFFFFFFE,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",394);
    color_format= (IceTEnum) 0xC000;
  }
  if ((depth_format != (IceTEnum) 0xD001)
      &&(depth_format != (IceTEnum) 0xD000))
  {
    icetRaiseDiagnostic("Invalid depth format.",(IceTEnum) 0xFFFFFFFE,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",399);
    depth_format= (IceTEnum) 0xD000;
  }
  header[0]= (IceTEnum) 0x004D5000;
  header[1]= color_format;
  header[2]= depth_format;
  header[3]= (IceTInt) width;
  header[4]= (IceTInt) height;
  header[5]= (IceTInt) (width*height);
  header[6]
    = (IceTInt) icetImageBufferSizeType(color_format,
                                        depth_format,
                                        width,
                                        height);
  return image;
}

IceTImage icetImagePointerAssignBuffer(IceTVoid *buffer,
                                       IceTSizeType width,
                                       IceTSizeType height,
                                       const IceTVoid *color_buffer,
                                       const IceTVoid *depth_buffer)
{
  IceTImage image= icetImageAssignBuffer(buffer,width,height);
  {
    IceTInt *header= ((IceTInt *) image.opaque_internals);
    header[0]= (IceTEnum) 0x004D5100;
    header[6]= -1;
  }
  if (icetImageGetColorFormat(image) == (IceTEnum) 0xC000)
  {
    if (color_buffer != ((void *) 0))
    {
      icetRaiseDiagnostic("Given a color buffer when color format is set to none.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,442);
    }
  } else {
    if (color_buffer == ((void *) 0))
    {
      icetRaiseDiagnostic("Not given a color buffer when color format requires one.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,448);
    }
  }
  if (icetImageGetDepthFormat(image) == (IceTEnum) 0xD000)
  {
    if (depth_buffer != ((void *) 0))
    {
      icetRaiseDiagnostic("Given a depth buffer when depth format is set to none.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,455);
    }
  } else {
    if (depth_buffer == ((void *) 0))
    {
      icetRaiseDiagnostic("Not given a depth buffer when depth format requires one.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,461);
    }
  } {
    const IceTVoid **data= ((IceTVoid *) &(((IceTInt *) image.opaque_internals)[7]));
    data[0]= color_buffer;
    data[1]= depth_buffer;
  }
  return image;
}

IceTImage icetImageNull(void)
{
  IceTImage image;
  image.opaque_internals= ((void *) 0);
  return image;
}

IceTBoolean icetImageIsNull(const IceTImage image)
{
  if (image.opaque_internals == ((void *) 0))
  {
    return 1;
  } else {
    return 0;
  }
}

IceTSparseImage icetGetStateBufferSparseImage(IceTEnum pname,
                                              IceTSizeType width,
                                              IceTSizeType height)
{
  IceTVoid     *buffer;
  IceTSizeType buffer_size;
  buffer_size= icetSparseImageBufferSize(width,height);
  buffer     = icetGetStateBuffer(pname,buffer_size);
  return icetSparseImageAssignBuffer(buffer,width,height);
}

IceTSparseImage icetSparseImageAssignBuffer(IceTVoid *buffer,
                                            IceTSizeType width,
                                            IceTSizeType height)
{
  IceTSparseImage image;
  IceTEnum        color_format,depth_format;
  IceTInt         *header;
  image.opaque_internals= buffer;
  if (buffer == ((void *) 0))
  {
    icetRaiseDiagnostic("Tried to create sparse image with NULL buffer.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,515);
    return image;
  }
  icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0009),&color_format);
  icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x000A),&depth_format);
  header= ((IceTInt *) image.opaque_internals);
  if ((color_format != (IceTEnum) 0xC001)
      &&(color_format != (IceTEnum) 0xC002)
      &&(color_format != (IceTEnum) 0xC000))
  {
    icetRaiseDiagnostic("Invalid color format.",(IceTEnum) 0xFFFFFFFE,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",527);
    color_format= (IceTEnum) 0xC000;
  }
  if ((depth_format != (IceTEnum) 0xD001)
      &&(depth_format != (IceTEnum) 0xD000))
  {
    icetRaiseDiagnostic("Invalid depth format.",(IceTEnum) 0xFFFFFFFE,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",532);
    depth_format= (IceTEnum) 0xD000;
  }
  header[0]= (IceTEnum) 0x004D6000;
  header[1]= color_format;
  header[2]= depth_format;
  header[3]= (IceTInt) width;
  header[4]= (IceTInt) height;
  header[5]= (IceTInt) (width*height);
  header[6]= 0;
  icetClearSparseImage(image);
  return image;
}

IceTSparseImage icetSparseImageNull(void)
{
  IceTSparseImage image;
  image.opaque_internals= ((void *) 0);
  return image;
}

IceTBoolean icetSparseImageIsNull(const IceTSparseImage image)
{
  if (image.opaque_internals == ((void *) 0))
  {
    return 1;
  } else {
    return 0;
  }
}

void icetImageAdjustForOutput(IceTImage image)
{
  IceTEnum color_format;
  if (icetImageIsNull(image)) return;
  ICET_TEST_IMAGE_HEADER(image);
  if (icetIsEnabled(((IceTEnum) 0x00000140|(IceTEnum) 0x0004)))
  {
    color_format= icetImageGetColorFormat(image);
    if (color_format != (IceTEnum) 0xC000)
    {
      ((IceTInt *) image.opaque_internals)[2]
        = (IceTEnum) 0xD000;
      icetImageSetDimensions(image,
                             icetImageGetWidth(image),
                             icetImageGetHeight(image));
    }
  }
}

void icetImageAdjustForInput(IceTImage image)
{
  IceTEnum color_format,depth_format;
  if (icetImageIsNull(image)) return;
  ICET_TEST_IMAGE_HEADER(image);
  icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0009),&color_format);
  icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x000A),&depth_format);
  ((IceTInt *) image.opaque_internals)[1]= color_format;
  ((IceTInt *) image.opaque_internals)[2]= depth_format;
  icetImageSetDimensions(image,
                         icetImageGetWidth(image),
                         icetImageGetHeight(image));
}

IceTEnum icetImageGetColorFormat(const IceTImage image)
{
  ICET_TEST_IMAGE_HEADER(image);
  if (!image.opaque_internals) return (IceTEnum) 0xC000;
  return ((IceTInt *) image.opaque_internals)[1];
}

IceTEnum icetImageGetDepthFormat(const IceTImage image)
{
  ICET_TEST_IMAGE_HEADER(image);
  if (!image.opaque_internals) return (IceTEnum) 0xD000;
  return ((IceTInt *) image.opaque_internals)[2];
}

IceTSizeType icetImageGetWidth(const IceTImage image)
{
  ICET_TEST_IMAGE_HEADER(image);
  if (!image.opaque_internals) return 0;
  return ((IceTInt *) image.opaque_internals)[3];
}

IceTSizeType icetImageGetHeight(const IceTImage image)
{
  ICET_TEST_IMAGE_HEADER(image);
  if (!image.opaque_internals) return 0;
  return ((IceTInt *) image.opaque_internals)[4];
}

IceTSizeType icetImageGetNumPixels(const IceTImage image)
{
  ICET_TEST_IMAGE_HEADER(image);
  if (!image.opaque_internals) return 0;
  return(((IceTInt *) image.opaque_internals)[3]
         *((IceTInt *) image.opaque_internals)[4]);
}

IceTEnum icetSparseImageGetColorFormat(const IceTSparseImage image)
{
  ICET_TEST_SPARSE_IMAGE_HEADER(image);
  if (!image.opaque_internals) return (IceTEnum) 0xC000;
  return ((IceTInt *) image.opaque_internals)[1];
}

IceTEnum icetSparseImageGetDepthFormat(const IceTSparseImage image)
{
  ICET_TEST_SPARSE_IMAGE_HEADER(image);
  if (!image.opaque_internals) return (IceTEnum) 0xD000;
  return ((IceTInt *) image.opaque_internals)[2];
}

IceTSizeType icetSparseImageGetWidth(const IceTSparseImage image)
{
  ICET_TEST_SPARSE_IMAGE_HEADER(image);
  if (!image.opaque_internals) return 0;
  return ((IceTInt *) image.opaque_internals)[3];
}

IceTSizeType icetSparseImageGetHeight(const IceTSparseImage image)
{
  ICET_TEST_SPARSE_IMAGE_HEADER(image);
  if (!image.opaque_internals) return 0;
  return ((IceTInt *) image.opaque_internals)[4];
}

IceTSizeType icetSparseImageGetNumPixels(const IceTSparseImage image)
{
  ICET_TEST_SPARSE_IMAGE_HEADER(image);
  if (!image.opaque_internals) return 0;
  return(((IceTInt *) image.opaque_internals)[3]
         *((IceTInt *) image.opaque_internals)[4]);
}

IceTSizeType icetSparseImageGetCompressedBufferSize(
  const IceTSparseImage image)
{
  ICET_TEST_SPARSE_IMAGE_HEADER(image);
  if (!image.opaque_internals) return 0;
  return ((IceTInt *) image.opaque_internals)[6];
}

void icetImageSetDimensions(IceTImage image,
                            IceTSizeType width,
                            IceTSizeType height)
{
  ICET_TEST_IMAGE_HEADER(image);
  if (icetImageIsNull(image))
  {
    icetRaiseDiagnostic("Cannot set number of pixels on null image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,688);
    return;
  }
  if (width*height
      > ((IceTInt *) image.opaque_internals)[5])
  {
    icetRaiseDiagnostic("Cannot set an image size to greater than what the" " image was originally created.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,695);
    return;
  }
  ((IceTInt *) image.opaque_internals)[3]= (IceTInt) width;
  ((IceTInt *) image.opaque_internals)[4]= (IceTInt) height;
  if (((IceTInt *) image.opaque_internals)[0]
      == (IceTEnum) 0x004D5000)
  {
    ((IceTInt *) image.opaque_internals)[6]
      = (IceTInt) icetImageBufferSizeType(icetImageGetColorFormat(image),
                                          icetImageGetDepthFormat(image),
                                          width,
                                          height);
  }
}

void icetSparseImageSetDimensions(IceTSparseImage image,
                                  IceTSizeType width,
                                  IceTSizeType height)
{
  ICET_TEST_SPARSE_IMAGE_HEADER(image);
  if (image.opaque_internals == ((void *) 0))
  {
    icetRaiseDiagnostic("Cannot set number of pixels on null image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,719);
    return;
  }
  if (width*height
      > ((IceTInt *) image.opaque_internals)[5])
  {
    icetRaiseDiagnostic("Cannot set an image size to greater than what the" " image was originally created.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,726);
    return;
  }
  ((IceTInt *) image.opaque_internals)[3]= (IceTInt) width;
  ((IceTInt *) image.opaque_internals)[4]= (IceTInt) height;
  icetClearSparseImage(image);
}

static void icetSparseImageSetActualSize(IceTSparseImage image,
                                         const IceTVoid *data_end)
{
  IceTPointerArithmetic buffer_begin
    = (IceTPointerArithmetic) ((IceTInt *) image.opaque_internals);
  IceTPointerArithmetic buffer_end
    = (IceTPointerArithmetic) data_end;
  IceTPointerArithmetic compressed_size= buffer_end-buffer_begin;
  ((IceTInt *) image.opaque_internals)[6]
    = (IceTInt) compressed_size;
}

const IceTVoid *icetImageGetColorConstVoid(const IceTImage image,
                                           IceTSizeType *pixel_size)
{
  if (pixel_size)
  {
    IceTEnum color_format= icetImageGetColorFormat(image);
    *pixel_size= colorPixelSize(color_format);
  }
  switch (((IceTInt *) image.opaque_internals)[0])
  {
  case (IceTEnum) 0x004D5000:
    return((IceTVoid *) &(((IceTInt *) image.opaque_internals)[7]));
  case (IceTEnum) 0x004D5100:
    return ((const IceTVoid * *) ((IceTVoid *) &(((IceTInt *) image.opaque_internals)[7])))[0];
  default:
    icetRaiseDiagnostic("Detected invalid image header.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,765);
    return((void *) 0);
  }
}

IceTVoid *icetImageGetColorVoid(IceTImage image,IceTSizeType *pixel_size)
{
  const IceTVoid *const_buffer= icetImageGetColorConstVoid(image,pixel_size);
  if (((IceTInt *) image.opaque_internals)[0]
      == (IceTEnum) 0x004D5100)
  {
    icetRaiseDiagnostic("Images of pointers are for reading only.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,778);
  }
  return (IceTVoid *) const_buffer;
}

const IceTUByte *icetImageGetColorcub(const IceTImage image)
{
  IceTEnum color_format= icetImageGetColorFormat(image);
  if (color_format != (IceTEnum) 0xC001)
  {
    icetRaiseDiagnostic("Color format is not of type ubyte.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,792);
    return((void *) 0);
  }
  return icetImageGetColorConstVoid(image,((void *) 0));
}

IceTUByte *icetImageGetColorub(IceTImage image)
{
  IceTEnum color_format= icetImageGetColorFormat(image);
  if (color_format != (IceTEnum) 0xC001)
  {
    icetRaiseDiagnostic("Color format is not of type ubyte.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,804);
    return((void *) 0);
  }
  return icetImageGetColorVoid(image,((void *) 0));
}

const IceTUInt *icetImageGetColorcui(const IceTImage image)
{
  return (const IceTUInt *) icetImageGetColorcub(image);
}

IceTUInt *icetImageGetColorui(IceTImage image)
{
  return (IceTUInt *) icetImageGetColorub(image);
}

const IceTFloat *icetImageGetColorcf(const IceTImage image)
{
  IceTEnum color_format= icetImageGetColorFormat(image);
  if (color_format != (IceTEnum) 0xC002)
  {
    icetRaiseDiagnostic("Color format is not of type float.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,824);
    return((void *) 0);
  }
  return icetImageGetColorConstVoid(image,((void *) 0));
}

IceTFloat *icetImageGetColorf(IceTImage image)
{
  IceTEnum color_format= icetImageGetColorFormat(image);
  if (color_format != (IceTEnum) 0xC002)
  {
    icetRaiseDiagnostic("Color format is not of type float.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,836);
    return((void *) 0);
  }
  return icetImageGetColorVoid(image,((void *) 0));
}

const IceTVoid *icetImageGetDepthConstVoid(const IceTImage image,
                                           IceTSizeType *pixel_size)
{
  IceTEnum color_format= icetImageGetColorFormat(image);
  if (pixel_size)
  {
    IceTEnum depth_format= icetImageGetDepthFormat(image);
    *pixel_size= depthPixelSize(depth_format);
  }
  switch (((IceTInt *) image.opaque_internals)[0])
  {
  case (IceTEnum) 0x004D5000:
  {
    IceTSizeType   color_format_bytes= (icetImageGetNumPixels(image)
                                        *colorPixelSize(color_format));
    const IceTByte *image_data_pointer=
      (const IceTByte *) ((IceTVoid *) &(((IceTInt *) image.opaque_internals)[7]));
    return image_data_pointer+color_format_bytes;
  }
  case (IceTEnum) 0x004D5100:
    return ((const IceTVoid * *) ((IceTVoid *) &(((IceTInt *) image.opaque_internals)[7])))[1];
  default:
    icetRaiseDiagnostic("Detected invalid image header.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,869);
    return((void *) 0);
  }
}

IceTVoid *icetImageGetDepthVoid(IceTImage image,IceTSizeType *pixel_size)
{
  const IceTVoid *const_buffer= icetImageGetDepthConstVoid(image,pixel_size);
  if (((IceTInt *) image.opaque_internals)[0]
      == (IceTEnum) 0x004D5100)
  {
    icetRaiseDiagnostic("Images of pointers are for reading only.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,882);
  }
  return (IceTVoid *) const_buffer;
}

const IceTFloat *icetImageGetDepthcf(const IceTImage image)
{
  IceTEnum depth_format= icetImageGetDepthFormat(image);
  if (depth_format != (IceTEnum) 0xD001)
  {
    icetRaiseDiagnostic("Depth format is not of type float.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,895);
    return((void *) 0);
  }
  return icetImageGetDepthConstVoid(image,((void *) 0));
}

IceTFloat *icetImageGetDepthf(IceTImage image)
{
  IceTEnum depth_format= icetImageGetDepthFormat(image);
  if (depth_format != (IceTEnum) 0xD001)
  {
    icetRaiseDiagnostic("Depth format is not of type float.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,907);
    return((void *) 0);
  }
  return icetImageGetDepthVoid(image,((void *) 0));
}

void icetImageCopyColorub(const IceTImage image,
                          IceTUByte *color_buffer,
                          IceTEnum out_color_format)
{
  IceTEnum in_color_format= icetImageGetColorFormat(image);
  if (out_color_format != (IceTEnum) 0xC001)
  {
    icetRaiseDiagnostic("Color format is not of type ubyte.",(IceTEnum) 0xFFFFFFFE,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,922);
    return;
  }
  if (in_color_format == (IceTEnum) 0xC000)
  {
    icetRaiseDiagnostic("Input image has no color data.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,927);
    return;
  }
  if (in_color_format == out_color_format)
  {
    const IceTUByte *in_buffer        = icetImageGetColorcub(image);
    IceTSizeType    color_format_bytes= (icetImageGetNumPixels(image)
                                         *colorPixelSize(in_color_format));
    memcpy(color_buffer,in_buffer,color_format_bytes);
  } else if ((in_color_format == (IceTEnum) 0xC002)
             &&(out_color_format == (IceTEnum) 0xC001))
  {
    const IceTFloat *in_buffer= icetImageGetColorcf(image);
    IceTSizeType    num_pixels= icetImageGetNumPixels(image);
    IceTSizeType    i;
    const IceTFloat *in;
    IceTUByte       *out;
    for (i= 0,in= in_buffer,out= color_buffer; i < 4*num_pixels;
         i++,in++,out++)
    {
      out[0]= (IceTUByte) (255*in[0]);
    }
  } else {
    icetRaiseDiagnostic("Encountered unexpected color format combination.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,949);
  }
}

void icetImageCopyColorf(const IceTImage image,
                         IceTFloat *color_buffer,
                         IceTEnum out_color_format)
{
  IceTEnum in_color_format= icetImageGetColorFormat(image);
  if (out_color_format != (IceTEnum) 0xC002)
  {
    icetRaiseDiagnostic("Color format is not of type float.",(IceTEnum) 0xFFFFFFFE,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,961);
    return;
  }
  if (in_color_format == (IceTEnum) 0xC000)
  {
    icetRaiseDiagnostic("Input image has no color data.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,966);
    return;
  }
  if (in_color_format == out_color_format)
  {
    const IceTFloat *in_buffer        = icetImageGetColorcf(image);
    IceTSizeType    color_format_bytes= (icetImageGetNumPixels(image)
                                         *colorPixelSize(in_color_format));
    memcpy(color_buffer,in_buffer,color_format_bytes);
  } else if ((in_color_format == (IceTEnum) 0xC001)
             &&(out_color_format == (IceTEnum) 0xC002))
  {
    const IceTUByte *in_buffer= icetImageGetColorcub(image);
    IceTSizeType    num_pixels= icetImageGetNumPixels(image);
    IceTSizeType    i;
    const IceTUByte *in;
    IceTFloat       *out;
    for (i= 0,in= in_buffer,out= color_buffer; i < 4*num_pixels;
         i++,in++,out++)
    {
      out[0]= (IceTFloat) in[0]/255.0f;
    }
  } else {
    icetRaiseDiagnostic("Unexpected format combination.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,988);
  }
}

void icetImageCopyDepthf(const IceTImage image,
                         IceTFloat *depth_buffer,
                         IceTEnum out_depth_format)
{
  IceTEnum in_depth_format= icetImageGetDepthFormat(image);
  if (out_depth_format != (IceTEnum) 0xD001)
  {
    icetRaiseDiagnostic("Depth format is not of type float.",(IceTEnum) 0xFFFFFFFE,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1000);
    return;
  }
  if (in_depth_format == (IceTEnum) 0xD000)
  {
    icetRaiseDiagnostic("Input image has no depth data.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1005);
    return;
  }
  {
    const IceTFloat *in_buffer        = icetImageGetDepthcf(image);
    IceTSizeType    depth_format_bytes= (icetImageGetNumPixels(image)
                                         *depthPixelSize(in_depth_format));
    memcpy(depth_buffer,in_buffer,depth_format_bytes);
  }
}

IceTBoolean icetImageEqual(const IceTImage image1,const IceTImage image2)
{
  return image1.opaque_internals == image2.opaque_internals;
}

void icetImageCopyPixels(const IceTImage in_image,IceTSizeType in_offset,
                         IceTImage out_image,IceTSizeType out_offset,
                         IceTSizeType num_pixels)
{
  IceTEnum color_format,depth_format;
  color_format= icetImageGetColorFormat(in_image);
  depth_format= icetImageGetDepthFormat(in_image);
  if ((color_format != icetImageGetColorFormat(out_image))
      ||(depth_format != icetImageGetDepthFormat(out_image)))
  {
    icetRaiseDiagnostic("Cannot copy pixels of images with different formats.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1035);
    return;
  }
  if ((in_offset < 0)
      ||(in_offset+num_pixels > icetImageGetNumPixels(in_image)))
  {
    icetRaiseDiagnostic("Pixels to copy are outside of range of source image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1042);
  }
  if ((out_offset < 0)
      ||(out_offset+num_pixels > icetImageGetNumPixels(out_image)))
  {
    icetRaiseDiagnostic("Pixels to copy are outside of range of source image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1047);
  }
  if (color_format != (IceTEnum) 0xC000)
  {
    const IceTByte *in_colors;
    IceTByte       *out_colors;
    IceTSizeType   pixel_size;
    in_colors = icetImageGetColorConstVoid(in_image,&pixel_size);
    out_colors= icetImageGetColorVoid(out_image,((void *) 0));
    memcpy(out_colors+pixel_size*out_offset,
           in_colors+pixel_size*in_offset,
           pixel_size*num_pixels);
  }
  if (depth_format != (IceTEnum) 0xD000)
  {
    const IceTByte *in_depths;
    IceTByte       *out_depths;
    IceTSizeType   pixel_size;
    in_depths = icetImageGetDepthConstVoid(in_image,&pixel_size);
    out_depths= icetImageGetDepthVoid(out_image,((void *) 0));
    memcpy(out_depths+pixel_size*out_offset,
           in_depths+pixel_size*in_offset,
           pixel_size*num_pixels);
  }
}

void icetImageCopyRegion(const IceTImage in_image,
                         const IceTInt *in_viewport,
                         IceTImage out_image,
                         const IceTInt *out_viewport)
{
  IceTEnum color_format= icetImageGetColorFormat(in_image);
  IceTEnum depth_format= icetImageGetDepthFormat(in_image);
  if ((color_format != icetImageGetColorFormat(out_image))
      ||(depth_format != icetImageGetDepthFormat(out_image)))
  {
    icetRaiseDiagnostic("icetImageCopyRegion only supports copying images" " of the same format.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1084);
    return;
  }
  if ((in_viewport[2] != out_viewport[2])
      ||(in_viewport[3] != out_viewport[3]))
  {
    icetRaiseDiagnostic("Sizes of input and output regions must be the same.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1091);
    return;
  }
  if (color_format != (IceTEnum) 0xC000)
  {
    IceTSizeType   pixel_size;
    const IceTByte *src = icetImageGetColorConstVoid(in_image,&pixel_size);
    IceTByte       *dest= icetImageGetColorVoid(out_image,&pixel_size);
    IceTSizeType   y;
    src += in_viewport[1]*icetImageGetWidth(in_image)*pixel_size;
    dest+= out_viewport[1]*icetImageGetWidth(out_image)*pixel_size;
    src += in_viewport[0]*pixel_size;
    dest+= out_viewport[0]*pixel_size;
    for (y= 0; y < in_viewport[3]; y++)
    {
      memcpy(dest,src,in_viewport[2]*pixel_size);
      src += icetImageGetWidth(in_image)*pixel_size;
      dest+= icetImageGetWidth(out_image)*pixel_size;
    }
  }
  if (depth_format != (IceTEnum) 0xD000)
  {
    IceTSizeType   pixel_size;
    const IceTByte *src = icetImageGetDepthConstVoid(in_image,&pixel_size);
    IceTByte       *dest= icetImageGetDepthVoid(out_image,&pixel_size);
    IceTSizeType   y;
    src += in_viewport[1]*icetImageGetWidth(in_image)*pixel_size;
    dest+= out_viewport[1]*icetImageGetWidth(out_image)*pixel_size;
    src += in_viewport[0]*pixel_size;
    dest+= out_viewport[0]*pixel_size;
    for (y= 0; y < in_viewport[3]; y++)
    {
      memcpy(dest,src,in_viewport[2]*pixel_size);
      src += icetImageGetWidth(in_image)*pixel_size;
      dest+= icetImageGetWidth(out_image)*pixel_size;
    }
  }
}

void icetImageClearAroundRegion(IceTImage image,const IceTInt *region)
{
  IceTSizeType width= icetImageGetWidth(image);
  IceTSizeType height= icetImageGetHeight(image);
  IceTEnum     color_format= icetImageGetColorFormat(image);
  IceTEnum     depth_format= icetImageGetDepthFormat(image);
  IceTSizeType x,y;
  if (color_format == (IceTEnum) 0xC001)
  {
    IceTUInt *color_buffer= icetImageGetColorui(image);
    IceTUInt background_color;
    icetGetIntegerv(((IceTEnum) 0x00000000|(IceTEnum) 0x0006),(IceTInt *) &background_color);
    for (y= 0; y < region[1]; y++)
    {
      for (x= 0; x < width; x++)
      {
        color_buffer[y*width+x]= background_color;
      }
    }
    if ((region[0] > 0)||(region[0]+region[2] < width))
    {
      for (y= region[1]; y < region[1]+region[3]; y++)
      {
        for (x= 0; x < region[0]; x++)
        {
          color_buffer[y*width+x]= background_color;
        }
        for (x= region[0]+region[2]; x < width; x++)
        {
          color_buffer[y*width+x]= background_color;
        }
      }
    }
    for (y= region[1]+region[3]; y < height; y++)
    {
      for (x= 0; x < width; x++)
      {
        color_buffer[y*width+x]= background_color;
      }
    }
  } else if (color_format == (IceTEnum) 0xC002)
  {
    IceTFloat *color_buffer= icetImageGetColorf(image);
    IceTFloat background_color[4];
    icetGetFloatv(((IceTEnum) 0x00000000|(IceTEnum) 0x0005),background_color);
    for (y= 0; y < region[1]; y++)
    {
      for (x= 0; x < width; x++)
      {
        color_buffer[4*(y*width+x)+0]= background_color[0];
        color_buffer[4*(y*width+x)+1]= background_color[1];
        color_buffer[4*(y*width+x)+2]= background_color[2];
        color_buffer[4*(y*width+x)+3]= background_color[3];
      }
    }
    if ((region[0] > 0)||(region[0]+region[2] < width))
    {
      for (y= region[1]; y < region[1]+region[3]; y++)
      {
        for (x= 0; x < region[0]; x++)
        {
          color_buffer[4*(y*width+x)+0]= background_color[0];
          color_buffer[4*(y*width+x)+1]= background_color[1];
          color_buffer[4*(y*width+x)+2]= background_color[2];
          color_buffer[4*(y*width+x)+3]= background_color[3];
        }
        for (x= region[0]+region[2]; x < width; x++)
        {
          color_buffer[4*(y*width+x)+0]= background_color[0];
          color_buffer[4*(y*width+x)+1]= background_color[1];
          color_buffer[4*(y*width+x)+2]= background_color[2];
          color_buffer[4*(y*width+x)+3]= background_color[3];
        }
      }
    }
    for (y= region[1]+region[3]; y < height; y++)
    {
      for (x= 0; x < width; x++)
      {
        color_buffer[4*(y*width+x)+0]= background_color[0];
        color_buffer[4*(y*width+x)+1]= background_color[1];
        color_buffer[4*(y*width+x)+2]= background_color[2];
        color_buffer[4*(y*width+x)+3]= background_color[3];
      }
    }
  } else if (color_format != (IceTEnum) 0xC000)
  {
    icetRaiseDiagnostic("Invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",1219);
  }
  if (depth_format == (IceTEnum) 0xD001)
  {
    IceTFloat *depth_buffer= icetImageGetDepthf(image);
    for (y= 0; y < region[1]; y++)
    {
      for (x= 0; x < width; x++)
      {
        depth_buffer[y*width+x]= 1.0f;
      }
    }
    if ((region[0] > 0)||(region[0]+region[2] < width))
    {
      for (y= region[1]; y < region[1]+region[3]; y++)
      {
        for (x= 0; x < region[0]; x++)
        {
          depth_buffer[y*width+x]= 1.0f;
        }
        for (x= region[0]+region[2]; x < width; x++)
        {
          depth_buffer[y*width+x]= 1.0f;
        }
      }
    }
    for (y= region[1]+region[3]; y < height; y++)
    {
      for (x= 0; x < width; x++)
      {
        depth_buffer[y*width+x]= 1.0f;
      }
    }
  } else if (depth_format != (IceTEnum) 0xD000)
  {
    icetRaiseDiagnostic("Invalid depth format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",1249);
  }
}

void icetImagePackageForSend(IceTImage image,
                             IceTVoid **buffer,IceTSizeType *size)
{
  ICET_TEST_IMAGE_HEADER(image);
  *buffer= image.opaque_internals;
  *size  = ((IceTInt *) image.opaque_internals)[6];
  if (*size < 0)
  {
    icetRaiseDiagnostic("Attempting to package an image that is not a single buffer.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1266);
  }
  if (*size != icetImageBufferSizeType(icetImageGetColorFormat(image),
                                       icetImageGetDepthFormat(image),
                                       icetImageGetWidth(image),
                                       icetImageGetHeight(image)))
  {
    icetRaiseDiagnostic("Inconsistent buffer size detected.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1274);
  }
}

IceTImage icetImageUnpackageFromReceive(IceTVoid *buffer)
{
  IceTImage image;
  IceTEnum  magic_number;
  IceTEnum  color_format,depth_format;
  image.opaque_internals= buffer;
  magic_number          = ((IceTInt *) image.opaque_internals)[0];
  if ((magic_number != (IceTEnum) 0x004D5000)
      &&(magic_number != (IceTEnum) 0x004D5100))
  {
    icetRaiseDiagnostic("Invalid image buffer: no magic number.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1291);
    image.opaque_internals= ((void *) 0);
    return image;
  }
  color_format= icetImageGetColorFormat(image);
  if ((color_format != (IceTEnum) 0xC001)
      &&(color_format != (IceTEnum) 0xC002)
      &&(color_format != (IceTEnum) 0xC000))
  {
    icetRaiseDiagnostic("Invalid image buffer: invalid color format.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1301);
    image.opaque_internals= ((void *) 0);
    return image;
  }
  depth_format= icetImageGetDepthFormat(image);
  if ((depth_format != (IceTEnum) 0xD001)
      &&(depth_format != (IceTEnum) 0xD000))
  {
    icetRaiseDiagnostic("Invalid image buffer: invalid depth format.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1310);
    image.opaque_internals= ((void *) 0);
    return image;
  }
  if (magic_number == (IceTEnum) 0x004D5000)
  {
    IceTSizeType buffer_size=
      ((IceTInt *) image.opaque_internals)[6];
    if (icetImageBufferSizeType(color_format,depth_format,
                                icetImageGetWidth(image),
                                icetImageGetHeight(image))
        != buffer_size)
    {
      icetRaiseDiagnostic("Inconsistent sizes in image data.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",1322);
      image.opaque_internals= ((void *) 0);
      return image;
    }
  } else {
    IceTSizeType buffer_size=
      ((IceTInt *) image.opaque_internals)[6];
    if (buffer_size != -1)
    {
      icetRaiseDiagnostic("Size information not consistent with image type.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,1331);
      image.opaque_internals= ((void *) 0);
      return image;
    }
  }
  ((IceTInt *) image.opaque_internals)[5]
    = (IceTInt) icetImageGetNumPixels(image);
  return image;
}

void icetSparseImagePackageForSend(IceTSparseImage image,
                                   IceTVoid **buffer,IceTSizeType *size)
{
  ICET_TEST_SPARSE_IMAGE_HEADER(image);
  if (icetSparseImageIsNull(image))
  {
    icetRaiseDiagnostic("Cannot package NULL image for send.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1355);
    *buffer= ((void *) 0);
    *size  = 0;
    return;
  }
  *buffer= image.opaque_internals;
  *size  = ((IceTInt *) image.opaque_internals)[6];
}

IceTSparseImage icetSparseImageUnpackageFromReceive(IceTVoid *buffer)
{
  IceTSparseImage image;
  IceTEnum        color_format,depth_format;
  image.opaque_internals= buffer;
  if (((IceTInt *) image.opaque_internals)[0]
      != (IceTEnum) 0x004D6000)
  {
    icetRaiseDiagnostic("Invalid image buffer: no magic number.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1376);
    image.opaque_internals= ((void *) 0);
    return image;
  }
  color_format= icetSparseImageGetColorFormat(image);
  if ((color_format != (IceTEnum) 0xC001)
      &&(color_format != (IceTEnum) 0xC002)
      &&(color_format != (IceTEnum) 0xC000))
  {
    icetRaiseDiagnostic("Invalid image buffer: invalid color format.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1386);
    image.opaque_internals= ((void *) 0);
    return image;
  }
  depth_format= icetSparseImageGetDepthFormat(image);
  if ((depth_format != (IceTEnum) 0xD001)
      &&(depth_format != (IceTEnum) 0xD000))
  {
    icetRaiseDiagnostic("Invalid image buffer: invalid depth format.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1395);
    image.opaque_internals= ((void *) 0);
    return image;
  }
  if (icetSparseImageBufferSizeType(color_format,depth_format,
                                    icetSparseImageGetWidth(image),
                                    icetSparseImageGetHeight(image))
      < ((IceTInt *) image.opaque_internals)[6])
  {
    icetRaiseDiagnostic("Inconsistent sizes in image data.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",1404);
    image.opaque_internals= ((void *) 0);
    return image;
  }
  ((IceTInt *) image.opaque_internals)[5]
    = (IceTInt) icetSparseImageGetNumPixels(image);
  return image;
}

IceTBoolean icetSparseImageEqual(const IceTSparseImage image1,
                                 const IceTSparseImage image2)
{
  return image1.opaque_internals == image2.opaque_internals;
}

static void icetSparseImageScanPixels(const IceTVoid **in_data_p,
                                      IceTSizeType *inactive_before_p,
                                      IceTSizeType *active_till_next_runl_p,
                                      IceTVoid **last_in_run_length_p,
                                      IceTSizeType pixels_to_skip,
                                      IceTSizeType pixel_size,
                                      IceTVoid **out_data_p,
                                      IceTVoid **out_run_length_p)
{
  const IceTByte *in_data             = *in_data_p;
  IceTSizeType   inactive_before      = *inactive_before_p;
  IceTSizeType   active_till_next_runl= *active_till_next_runl_p;
  IceTSizeType   pixels_left          = pixels_to_skip;
  const IceTVoid *last_in_run_length  = ((void *) 0);
  IceTByte       *out_data;
  IceTVoid       *last_out_run_length;
  if (pixels_left < 1){return;}
  if (out_data_p != ((void *) 0))
  {
    out_data= *out_data_p;
    if (out_run_length_p != ((void *) 0))
    {
      last_out_run_length= *out_run_length_p;
    } else {
      {last_out_run_length                               = out_data;
       out_data                                         += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
       (((IceTRunLengthType *) (last_out_run_length))[0])= 0;
       (((IceTRunLengthType *) (last_out_run_length))[1])= 0;
      }
    }
  } else {
    out_data           = ((void *) 0);
    last_out_run_length= ((void *) 0);
  }
  while (pixels_left > 0)
  {
    IceTSizeType count;
    if ((inactive_before == 0)&&(active_till_next_runl == 0))
    {
      last_in_run_length   = in_data;
      inactive_before      = (((IceTRunLengthType *) (in_data))[0]);
      active_till_next_runl= (((IceTRunLengthType *) (in_data))[1]);
      in_data             += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
    }
    count= ((inactive_before) < (pixels_left) ? (inactive_before) : (pixels_left));
    if (count > 0)
    {
      if (out_data != ((void *) 0))
      {
        if ((((IceTRunLengthType *) (last_out_run_length))[1]) > 0)
        {
          {last_out_run_length                               = out_data;
           out_data                                         += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
           (((IceTRunLengthType *) (last_out_run_length))[0])= 0;
           (((IceTRunLengthType *) (last_out_run_length))[1])= 0;
          }
        }
        (((IceTRunLengthType *) (last_out_run_length))[0])+= count;
      }
      inactive_before-= count;
      pixels_left    -= count;
    }
    count= ((active_till_next_runl) < (pixels_left) ? (active_till_next_runl) : (pixels_left));
    if (count > 0)
    {
      if (out_data != ((void *) 0))
      {
        (((IceTRunLengthType *) (last_out_run_length))[1])+= count;
        memcpy(out_data,in_data,count*pixel_size);
        out_data+= count*pixel_size;
      }
      in_data              += count*pixel_size;
      active_till_next_runl-= count;
      pixels_left          -= count;
    }
  }
  if (pixels_left < 0)
  {
    icetRaiseDiagnostic("Miscounted pixels",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",1497);
  }
  *in_data_p              = in_data;
  *inactive_before_p      = inactive_before;
  *active_till_next_runl_p= active_till_next_runl;
  if (last_in_run_length_p)
  {
    *last_in_run_length_p= (IceTVoid *) last_in_run_length;
  }
  if (out_data_p)
  {
    *out_data_p= out_data;
  }
  if (out_run_length_p)
  {
    *out_run_length_p= last_out_run_length;
  }
}

static void icetSparseImageCopyPixelsInternal(
  const IceTVoid **in_data_p,
  IceTSizeType *inactive_before_p,
  IceTSizeType *active_till_next_runl_p,
  IceTSizeType pixels_to_copy,
  IceTSizeType pixel_size,
  IceTSparseImage out_image)
{
  IceTVoid *out_data= ((IceTVoid *) &(((IceTInt *) out_image.opaque_internals)[7]));
  icetSparseImageSetDimensions(out_image,pixels_to_copy,1);
  icetSparseImageScanPixels(in_data_p,
                            inactive_before_p,
                            active_till_next_runl_p,
                            ((void *) 0),
                            pixels_to_copy,
                            pixel_size,
                            &out_data,
                            ((void *) 0));
  icetSparseImageSetActualSize(out_image,out_data);
}

static void icetSparseImageCopyPixelsInPlaceInternal(
  const IceTVoid **in_data_p,
  IceTSizeType *inactive_before_p,
  IceTSizeType *active_till_next_runl_p,
  IceTSizeType pixels_to_copy,
  IceTSizeType pixel_size,
  IceTSparseImage out_image)
{
  IceTVoid *last_run_length= ((void *) 0);
  if ((*in_data_p != ((IceTVoid *) &(((IceTInt *) out_image.opaque_internals)[7])))
      ||(*inactive_before_p != 0)
      ||(*active_till_next_runl_p != 0))
  {
    icetRaiseDiagnostic("icetSparseImageCopyPixelsInPlaceInternal not called" " at beginning of buffer.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1556);
  }
  icetSparseImageScanPixels(in_data_p,
                            inactive_before_p,
                            active_till_next_runl_p,
                            &last_run_length,
                            pixels_to_copy,
                            pixel_size,
                            ((void *) 0),
                            ((void *) 0));
  ((IceTInt *) out_image.opaque_internals)[3]
                                             = (IceTInt) pixels_to_copy;
  ((IceTInt *) out_image.opaque_internals)[4]= (IceTInt) 1;
  if (last_run_length != ((void *) 0))
  {
    (((IceTRunLengthType *) (last_run_length))[0])-= *inactive_before_p;
    (((IceTRunLengthType *) (last_run_length))[1])-= *active_till_next_runl_p;
  }
  icetSparseImageSetActualSize(out_image,*in_data_p);
}

void icetSparseImageCopyPixels(const IceTSparseImage in_image,
                               IceTSizeType in_offset,
                               IceTSizeType num_pixels,
                               IceTSparseImage out_image)
{
  IceTEnum       color_format;
  IceTEnum       depth_format;
  IceTSizeType   pixel_size;
  const IceTVoid *in_data;
  IceTSizeType   start_inactive;
  IceTSizeType   start_active;
  icetTimingCompressBegin();
  color_format= icetSparseImageGetColorFormat(in_image);
  depth_format= icetSparseImageGetDepthFormat(in_image);
  if ((color_format != icetSparseImageGetColorFormat(out_image))
      ||(depth_format != icetSparseImageGetDepthFormat(out_image)))
  {
    icetRaiseDiagnostic("Cannot copy pixels of images with different formats.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1601);
    icetTimingCompressEnd();
    return;
  }
  if ((in_offset == 0)
      &&(num_pixels == icetSparseImageGetNumPixels(in_image)))
  {
    IceTSizeType bytes_to_copy
      = ((IceTInt *) in_image.opaque_internals)[6];
    IceTSizeType max_pixels
      = ((IceTInt *) out_image.opaque_internals)[5];
    ICET_TEST_SPARSE_IMAGE_HEADER(out_image);
    if (max_pixels < num_pixels)
    {
      icetRaiseDiagnostic("Cannot set an image size to greater than what the" " image was originally created.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,1620);
      icetTimingCompressEnd();
      return;
    }
    memcpy(((IceTInt *) out_image.opaque_internals),
           ((IceTInt *) in_image.opaque_internals),
           bytes_to_copy);
    ((IceTInt *) out_image.opaque_internals)[5]
      = max_pixels;
    icetTimingCompressEnd();
    return;
  }
  pixel_size    = colorPixelSize(color_format)+depthPixelSize(depth_format);
  in_data       = ((IceTVoid *) &(((IceTInt *) in_image.opaque_internals)[7]));
  start_inactive= start_active= 0;
  icetSparseImageScanPixels(&in_data,
                            &start_inactive,
                            &start_active,
                            ((void *) 0),
                            in_offset,
                            pixel_size,
                            ((void *) 0),
                            ((void *) 0));
  icetSparseImageCopyPixelsInternal(&in_data,
                                    &start_inactive,
                                    &start_active,
                                    num_pixels,
                                    pixel_size,
                                    out_image);
  icetTimingCompressEnd();
}

IceTSizeType icetSparseImageSplitPartitionNumPixels(
  IceTSizeType input_num_pixels,
  IceTInt num_partitions,
  IceTInt eventual_num_partitions)
{
  IceTInt sub_partitions= eventual_num_partitions/num_partitions;
  if (eventual_num_partitions%num_partitions != 0)
  {
    icetRaiseDiagnostic("num_partitions not a factor" " of eventual_num_partitions.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1670);
  }
  return input_num_pixels/num_partitions+sub_partitions;
}

static void icetSparseImageSplitChoosePartitions(
  IceTInt num_partitions,
  IceTInt eventual_num_partitions,
  IceTSizeType size,
  IceTSizeType first_offset,
  IceTSizeType *offsets)
{
  IceTSizeType remainder     = size%eventual_num_partitions;
  IceTInt      sub_partitions= eventual_num_partitions/num_partitions;
  IceTSizeType partition_lower_size
    = (size/eventual_num_partitions)*sub_partitions;
  IceTSizeType this_offset= first_offset;
  IceTInt      partition_idx;
  if (eventual_num_partitions%num_partitions != 0)
  {
    icetRaiseDiagnostic("num_partitions not a factor" " of eventual_num_partitions.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1695);
  }
  for (partition_idx= 0; partition_idx < num_partitions; partition_idx++)
  {
    offsets[partition_idx]= this_offset;
    this_offset          += partition_lower_size;
    if (remainder > sub_partitions)
    {
      this_offset+= sub_partitions;
      remainder  -= sub_partitions;
    } else {
      this_offset+= remainder;
      remainder   = 0;
    }
  }
}

void icetSparseImageSplit(const IceTSparseImage in_image,
                          IceTSizeType in_image_offset,
                          IceTInt num_partitions,
                          IceTInt eventual_num_partitions,
                          IceTSparseImage *out_images,
                          IceTSizeType *offsets)
{
  IceTSizeType   total_num_pixels;
  IceTEnum       color_format;
  IceTEnum       depth_format;
  IceTSizeType   pixel_size;
  const IceTVoid *in_data;
  IceTSizeType   start_inactive;
  IceTSizeType   start_active;
  IceTInt        partition;
  icetTimingCompressBegin();
  if (num_partitions < 2)
  {
    icetRaiseDiagnostic("It does not make sense to call icetSparseImageSplit" " with less than 2 partitions.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1736);
    icetTimingCompressEnd();
    return;
  }
  total_num_pixels= icetSparseImageGetNumPixels(in_image);
  color_format    = icetSparseImageGetColorFormat(in_image);
  depth_format    = icetSparseImageGetDepthFormat(in_image);
  pixel_size      = colorPixelSize(color_format)+depthPixelSize(depth_format);
  in_data         = ((IceTVoid *) &(((IceTInt *) in_image.opaque_internals)[7]));
  start_inactive  = start_active= 0;
  icetSparseImageSplitChoosePartitions(num_partitions,
                                       eventual_num_partitions,
                                       total_num_pixels,
                                       in_image_offset,
                                       offsets);
  for (partition= 0; partition < num_partitions; partition++)
  {
    IceTSparseImage out_image= out_images[partition];
    IceTSizeType    partition_num_pixels;
    if ((color_format != icetSparseImageGetColorFormat(out_image))
        ||(depth_format != icetSparseImageGetDepthFormat(out_image)))
    {
      icetRaiseDiagnostic("Cannot copy pixels of images with different" " formats.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,1764);
      icetTimingCompressEnd();
      return;
    }
    if (partition < num_partitions-1)
    {
      partition_num_pixels= offsets[partition+1]-offsets[partition];
    } else {
      partition_num_pixels
        = total_num_pixels+in_image_offset-offsets[partition];
    }
    if (icetSparseImageEqual(in_image,out_image))
    {
      if (partition == 0)
      {
        icetSparseImageCopyPixelsInPlaceInternal(&in_data,
                                                 &start_inactive,
                                                 &start_active,
                                                 partition_num_pixels,
                                                 pixel_size,
                                                 out_image);
      } else {
        icetRaiseDiagnostic("icetSparseImageSplit copy in place only allowed" " in first partition.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                            ,1787);
      }
    } else {
      icetSparseImageCopyPixelsInternal(&in_data,
                                        &start_inactive,
                                        &start_active,
                                        partition_num_pixels,
                                        pixel_size,
                                        out_image);
    }
  }
  if ((start_inactive != 0)
      ||(start_active != 0))
  {
    icetRaiseDiagnostic("Counting problem.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",1802);
  }
  icetTimingCompressEnd();
}

void icetSparseImageInterlace(const IceTSparseImage in_image,
                              IceTInt eventual_num_partitions,
                              IceTEnum scratch_state_buffer,
                              IceTSparseImage out_image)
{
  IceTSizeType   num_pixels          = icetSparseImageGetNumPixels(in_image);
  IceTEnum       color_format        = icetSparseImageGetColorFormat(in_image);
  IceTEnum       depth_format        = icetSparseImageGetDepthFormat(in_image);
  IceTSizeType   lower_partition_size= num_pixels/eventual_num_partitions;
  IceTSizeType   remaining_pixels    = num_pixels%eventual_num_partitions;
  IceTSizeType   pixel_size;
  IceTInt        original_partition_idx;
  IceTInt        interlaced_partition_idx;
  const IceTVoid **in_data_array;
  IceTSizeType   *inactive_before_array;
  IceTSizeType   *active_till_next_runl_array;
  const IceTVoid *in_data;
  IceTVoid       *out_data;
  IceTSizeType   inactive_before;
  IceTSizeType   active_till_next_runl;
  IceTVoid       *last_run_length;
  if (eventual_num_partitions < 2)
  {
    icetSparseImageCopyPixels(in_image,0,num_pixels,out_image);
    return;
  }
  if ((color_format != icetSparseImageGetColorFormat(out_image))
      ||(depth_format != icetSparseImageGetDepthFormat(out_image)))
  {
    icetRaiseDiagnostic("Cannot copy pixels of images with different formats.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1840);
    return;
  }
  icetTimingInterlaceBegin();
  pixel_size= colorPixelSize(color_format)+depthPixelSize(depth_format);
  {
    IceTByte *buffer= icetGetStateBuffer(
      scratch_state_buffer,
      eventual_num_partitions*sizeof(IceTVoid*)
      +2*eventual_num_partitions*sizeof(IceTSizeType));
    in_data_array= (const IceTVoid * *) buffer;
    inactive_before_array
      = (IceTSizeType *) (buffer
                          +eventual_num_partitions*sizeof(IceTVoid*));
    active_till_next_runl_array
      = inactive_before_array+eventual_num_partitions;
  }
  in_data              = ((IceTVoid *) &(((IceTInt *) in_image.opaque_internals)[7]));
  inactive_before      = 0;
  active_till_next_runl= 0;
  for (original_partition_idx= 0;
       original_partition_idx < eventual_num_partitions;
       original_partition_idx++)
  {
    IceTSizeType pixels_to_skip;
    {int placeholder;
     int input= (original_partition_idx);
     (interlaced_partition_idx)= 0;
     for (placeholder= 0x0001; placeholder < eventual_num_partitions; placeholder<<= 1)
     {
       (interlaced_partition_idx)<<= 1;
       (interlaced_partition_idx) += input&0x0001;
       input                     >>= 1;
     }
    }
    if (eventual_num_partitions <= interlaced_partition_idx)
    {
      interlaced_partition_idx= original_partition_idx;
    }
    pixels_to_skip= lower_partition_size;
    if (interlaced_partition_idx < remaining_pixels)
    {
      pixels_to_skip+= 1;
    }
    in_data_array[interlaced_partition_idx]        = in_data;
    inactive_before_array[interlaced_partition_idx]= inactive_before;
    active_till_next_runl_array[interlaced_partition_idx]
      = active_till_next_runl;
    if (original_partition_idx < eventual_num_partitions-1)
    {
      icetSparseImageScanPixels((const IceTVoid * *) &in_data,
                                &inactive_before,
                                &active_till_next_runl,
                                ((void *) 0),
                                pixels_to_skip,
                                pixel_size,
                                ((void *) 0),
                                ((void *) 0));
    }
  }
  icetSparseImageSetDimensions(out_image,
                               icetSparseImageGetWidth(in_image),
                               icetSparseImageGetHeight(in_image));
  out_data                               = ((IceTVoid *) &(((IceTInt *) out_image.opaque_internals)[7]));
  (((IceTRunLengthType *) (out_data))[0])= 0;
  (((IceTRunLengthType *) (out_data))[1])= 0;
  last_run_length                        = out_data;
  out_data                               = (IceTByte *) out_data+((IceTSizeType) (2*sizeof(IceTRunLengthType)));
  for (interlaced_partition_idx= 0;
       interlaced_partition_idx < eventual_num_partitions;
       interlaced_partition_idx++)
  {
    IceTSizeType pixels_left;
    pixels_left= lower_partition_size;
    if (interlaced_partition_idx < remaining_pixels)
    {
      pixels_left+= 1;
    }
    in_data        = in_data_array[interlaced_partition_idx];
    inactive_before= inactive_before_array[interlaced_partition_idx];
    active_till_next_runl
      = active_till_next_runl_array[interlaced_partition_idx];
    icetSparseImageScanPixels((const IceTVoid * *) &in_data,
                              &inactive_before,
                              &active_till_next_runl,
                              ((void *) 0),
                              pixels_left,
                              pixel_size,
                              (IceTVoid * *) &out_data,
                              &last_run_length);
  }
  icetSparseImageSetActualSize(out_image,out_data);
  icetTimingInterlaceEnd();
}

IceTSizeType icetGetInterlaceOffset(IceTInt partition_index,
                                    IceTInt eventual_num_partitions,
                                    IceTSizeType original_image_size)
{
  IceTSizeType lower_partition_size;
  IceTSizeType remaining_pixels;
  IceTSizeType offset;
  IceTInt      original_partition_idx;
  if ((partition_index < 0)||(eventual_num_partitions <= partition_index))
  {
    icetRaiseDiagnostic("Invalid partition for interlace offset",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,1951);
    return 0;
  }
  icetTimingInterlaceBegin();
  lower_partition_size= original_image_size/eventual_num_partitions;
  remaining_pixels    = original_image_size%eventual_num_partitions;
  offset              = 0;
  for (original_partition_idx= 0;
       original_partition_idx < eventual_num_partitions;
       original_partition_idx++)
  {
    IceTInt      interlaced_partition_idx;
    IceTSizeType partition_size;
    {int placeholder;
     int input= (original_partition_idx);
     (interlaced_partition_idx)= 0;
     for (placeholder= 0x0001; placeholder < eventual_num_partitions; placeholder<<= 1)
     {
       (interlaced_partition_idx)<<= 1;
       (interlaced_partition_idx) += input&0x0001;
       input                     >>= 1;
     }
    }
    if (eventual_num_partitions <= interlaced_partition_idx)
    {
      interlaced_partition_idx= original_partition_idx;
    }
    if (interlaced_partition_idx == partition_index)
    {
      icetTimingInterlaceEnd();
      return offset;
    }
    partition_size= lower_partition_size;
    if (interlaced_partition_idx < remaining_pixels)
    {
      partition_size+= 1;
    }
    offset+= partition_size;
  }
  icetRaiseDiagnostic("Could not find partition index.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",1989);
  icetTimingInterlaceEnd();
  return 0;
}

void icetClearImage(IceTImage image)
{
  IceTInt region[4]= {0,0,0,0};
  icetImageClearAroundRegion(image,region);
}

void icetClearSparseImage(IceTSparseImage image)
{
  IceTByte *data;
  ICET_TEST_SPARSE_IMAGE_HEADER(image);
  if (icetSparseImageIsNull(image)){return;}
  data                               = ((IceTVoid *) &(((IceTInt *) image.opaque_internals)[7]));
  (((IceTRunLengthType *) (data))[0])= icetSparseImageGetNumPixels(image);
  (((IceTRunLengthType *) (data))[1])= 0;
  icetSparseImageSetActualSize(image,data+((IceTSizeType) (2*sizeof(IceTRunLengthType))));
}

void icetSetColorFormat(IceTEnum color_format)
{
  IceTBoolean isDrawing;
  icetGetBooleanv(((IceTEnum) 0x00000080|(IceTEnum) 0x0000),&isDrawing);
  if (isDrawing)
  {
    icetRaiseDiagnostic("Attempted to change the color format while drawing." " This probably means that you called icetSetColorFormat" " in a drawing callback. You cannot do that. Call this" " function before starting the draw operation.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,2026);
    return;
  }
  if ((color_format == (IceTEnum) 0xC001)
      ||(color_format == (IceTEnum) 0xC002)
      ||(color_format == (IceTEnum) 0xC000))
  {
    icetStateSetInteger(((IceTEnum) 0x00000000|(IceTEnum) 0x0009),color_format);
  } else {
    icetRaiseDiagnostic("Invalid IceT color format.",(IceTEnum) 0xFFFFFFFE,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",2035);
  }
}

void icetSetDepthFormat(IceTEnum depth_format)
{
  IceTBoolean isDrawing;
  icetGetBooleanv(((IceTEnum) 0x00000080|(IceTEnum) 0x0000),&isDrawing);
  if (isDrawing)
  {
    icetRaiseDiagnostic("Attempted to change the depth format while drawing." " This probably means that you called icetSetDepthFormat" " in a drawing callback. You cannot do that. Call this" " function before starting the draw operation.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,2049);
    return;
  }
  if ((depth_format == (IceTEnum) 0xD001)
      ||(depth_format == (IceTEnum) 0xD000))
  {
    icetStateSetInteger(((IceTEnum) 0x00000000|(IceTEnum) 0x000A),depth_format);
  } else {
    icetRaiseDiagnostic("Invalid IceT depth format.",(IceTEnum) 0xFFFFFFFE,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",2057);
  }
}

void icetGetTileImage(IceTInt tile,IceTImage image)
{
  IceTInt       screen_viewport[4],target_viewport[4];
  const IceTInt *viewports;
  IceTSizeType  width,height;
  IceTImage     rendered_image;
  viewports= icetUnsafeStateGetInteger(((IceTEnum) 0x00000000|(IceTEnum) 0x0011));
  width    = viewports[4*tile+2];
  height   = viewports[4*tile+3];
  icetImageSetDimensions(image,width,height);
  rendered_image=
    generateTile(tile,screen_viewport,target_viewport,image);
  icetTimingBufferReadBegin();
  if (icetImageEqual(rendered_image,image))
  {
    if ((screen_viewport[0] != target_viewport[0])
        ||(screen_viewport[1] != target_viewport[1])
        ||(screen_viewport[2] != target_viewport[2])
        ||(screen_viewport[3] != target_viewport[3]))
    {
      icetRaiseDiagnostic("Inconsistent values returned from generateTile.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,2085);
    }
  } else {
    icetImageCopyRegion(rendered_image,screen_viewport,
                        image,target_viewport);
  }
  icetImageClearAroundRegion(image,target_viewport);
  icetTimingBufferReadEnd();
}

#define ICET_STATE_ENGINE_START (IceTEnum)0x00000000
#define ICET_RANK               (ICET_STATE_ENGINE_START | (IceTEnum)0x0002)
#define ICET_NUM_PROCESSES      (ICET_STATE_ENGINE_START | (IceTEnum)0x0003)

void icetGetCompressedTileImage(IceTInt tile,IceTSparseImage compressed_image)
{

    IceTInt r=-1;
    icetGetIntegerv(ICET_RANK, &r);
    printf("\n+ [%d] icetGetCompressedTileImage\n",r);

  IceTInt       screen_viewport[4],target_viewport[4];
  IceTImage     raw_image;
  const IceTInt *viewports;
  IceTSizeType  width,height;
  IceTSizeType  space_left,space_right,space_bottom,space_top;
  if (vp_ptr == ((void *) 0)) {
      /*printf("VP NULL\n");*/
  } else {
    int numV= vp_ptr[0];
    int i   = 0;
    for (i= 0; i < numV; ++i)
      printf("(%d,%d,%d,%d) ",vp_ptr[1+i*4+0],vp_ptr[1+i*4+1],vp_ptr[1+i*4+2],vp_ptr[1+i*4+3]);
    printf("\n");
  }
  viewports= icetUnsafeStateGetInteger(((IceTEnum) 0x00000000|(IceTEnum) 0x0011));
  width    = viewports[4*tile+2];
  height   = viewports[4*tile+3];
  icetSparseImageSetDimensions(compressed_image,width,height);
  raw_image= generateTile(tile,screen_viewport,target_viewport,
                          icetImageNull());
  if ((target_viewport[2] < 1)||(target_viewport[3] < 1))
  {
    icetClearSparseImage(compressed_image);
    return;
  }
  space_left  = target_viewport[0];
  space_right = width-target_viewport[2]-space_left;
  space_bottom= target_viewport[1];
  space_top   = height-target_viewport[3]-space_bottom;
  icetSparseImageSetDimensions(compressed_image,width,height);
  {
    IceTEnum     _color_format,_depth_format;
    IceTSizeType _pixel_count;
    IceTEnum     _composite_mode;
    IceTSizeType _input_width  = icetImageGetWidth(raw_image);
    IceTSizeType _region_width = screen_viewport[2];
    IceTSizeType _region_x_skip= _input_width-(screen_viewport[2]);
    icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0028),&_composite_mode);
    _color_format= icetImageGetColorFormat(raw_image);
    _depth_format= icetImageGetDepthFormat(raw_image);
    _pixel_count = (screen_viewport[2])*(screen_viewport[3]);
    if ((icetSparseImageGetColorFormat(compressed_image) != _color_format)||
        (icetSparseImageGetDepthFormat(compressed_image) != _depth_format))
    {
      icetRaiseDiagnostic(
        "Format of input and output to compress do not match.",
        (IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
        "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h",92);
    }
    if (icetSparseImageGetNumPixels(compressed_image)
        != (_pixel_count+(width)*(space_top+space_bottom)
            +((height)-(space_top+space_bottom))*(space_left+space_right))
        )
    {
      icetRaiseDiagnostic("Size of input and output to compress do not match.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                          ,100);
    }
    if ((screen_viewport[0] < 0)||(screen_viewport[1] < 0)
        ||(screen_viewport[0]+screen_viewport[2] > icetImageGetWidth(raw_image))
        ||(screen_viewport[1]+screen_viewport[3] > icetImageGetHeight(raw_image)))
    {
      icetRaiseDiagnostic("Size of input incompatible with region.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                          ,113);
    }
    if (_composite_mode == (IceTEnum) 0x0301)
    {
      if (_depth_format == (IceTEnum) 0xD001)
      {
        const IceTFloat *_depth= icetImageGetDepthcf(raw_image);
        _depth+= (screen_viewport[0]+_input_width*(screen_viewport[1]));
        if (_color_format == (IceTEnum) 0xC001)
        {
          const IceTUInt *_color;
          IceTUInt       *_c_out;
          IceTFloat      *_d_out;
          IceTSizeType   _region_count= 0;
          _color = icetImageGetColorcui(raw_image);
          _color+= (screen_viewport[0]+_input_width*(screen_viewport[1]));
          {
            IceTByte     *_dest;
            IceTSizeType _pixels= _pixel_count;
            IceTSizeType _p;
            IceTSizeType _count;
            IceTSizeType _totalcount= 0;
            IceTSizeType _compressed_size;
            icetTimingCompressBegin();
            _dest = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
            _count= space_bottom*width;
            if ((space_left != 0)||(space_right != 0))
            {
              IceTSizeType _line,_lastline;
              for (_line= space_bottom,_lastline= height-space_top;
                   _line < _lastline; _line++)
              {
                IceTSizeType _x    = space_left;
                IceTSizeType _lastx= width-space_right;
                _count+= space_left;
                while (1)
                {
                  IceTVoid *_runlengths;
                  while ((_x < _lastx)&&(!(_depth[0] < 1.0)))
                  {
                    _x++;
                    _count++;
                    _color++;
                    _depth++;
                    _region_count++;
                    if (_region_count >= _region_width){_color+= _region_x_skip;_depth+= _region_x_skip;_region_count= 0;}
                  }
                  if (_x >= _lastx) break;
                  _runlengths                               = _dest;
                  _dest                                    += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                  (((IceTRunLengthType *) (_runlengths))[0])= _count;
                  _totalcount                              += _count;
                  _count                                    = 0;
                  while ((_x < _lastx)&&(_depth[0] < 1.0))
                  {
                    _c_out   = (IceTUInt *) _dest;
                    _c_out[0]= _color[0];
                    _dest   += sizeof(IceTUInt);
                    _d_out   = (IceTFloat *) _dest;
                    _d_out[0]= _depth[0];
                    _dest   += sizeof(IceTFloat);
                    _color++;
                    _depth++;
                    _region_count++;
                    if (_region_count >= _region_width){_color+= _region_x_skip;_depth+= _region_x_skip;_region_count= 0;}
                    _count++;
                    _x++;
                  }
                  (((IceTRunLengthType *) (_runlengths))[1])= _count;
                  _totalcount                              += _count;
                  _count                                    = 0;
                  if (_x >= _lastx) break;
                }
                _count+= space_right;
              }
            } else {
              _pixels= (height-space_bottom-space_top)*width;
              _p     = 0;
              while (_p < _pixels)
              {
                IceTVoid *_runlengths= _dest;
                _dest+= ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                while ((_p < _pixels)&&(!(_depth[0] < 1.0)))
                {
                  _p++;
                  _count++;
                  _color++;
                  _depth++;
                  _region_count++;
                  if (_region_count >= _region_width){_color+= _region_x_skip;_depth+= _region_x_skip;_region_count= 0;}
                }
                (((IceTRunLengthType *) (_runlengths))[0])= _count;
                _totalcount                              += _count;
                _count                                    = 0;
                while ((_p < _pixels)&&(_depth[0] < 1.0))
                {
                  _c_out   = (IceTUInt *) _dest;
                  _c_out[0]= _color[0];
                  _dest   += sizeof(IceTUInt);
                  _d_out   = (IceTFloat *) _dest;
                  _d_out[0]= _depth[0];
                  _dest   += sizeof(IceTFloat);
                  _color++;
                  _depth++;
                  _region_count++;
                  if (_region_count >= _region_width){_color+= _region_x_skip;_depth+= _region_x_skip;_region_count= 0;}
                  _count++;
                  _p++;
                }
                (((IceTRunLengthType *) (_runlengths))[1])= _count;
                _totalcount                              += _count;
                _count                                    = 0;
              }
            }
            _count+= space_top*width;
            if (_count > 0)
            {
              (((IceTRunLengthType *) (_dest))[0])= _count;
              (((IceTRunLengthType *) (_dest))[1])= 0;
              _dest                              += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _totalcount                        += _count;
            }
            _totalcount-= (width)*(space_top+space_bottom);
            _totalcount-= ((height-(space_top+space_bottom))
                           *(space_left+space_right));
            if (_totalcount != (IceTSizeType) _pixel_count)
            {
              char msg[256];
              sprintf(msg,"Total run lengths don't equal pixel count: %d != %d",
                      (int) _totalcount,(int) (_pixel_count));
              icetRaiseDiagnostic(msg,(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/compress_template_body.h",183);
            }
            icetTimingCompressEnd();
            _compressed_size
              = (IceTSizeType)
                ((IceTPointerArithmetic) _dest
                 -(IceTPointerArithmetic) ((IceTInt *) compressed_image.opaque_internals));

            ((IceTInt *) compressed_image.opaque_internals)[6]
              = (IceTInt) _compressed_size;
          }
        } else if (_color_format == (IceTEnum) 0xC002)
        {
          const IceTFloat *_color;
          IceTFloat       *_out;
          IceTSizeType    _region_count= 0;
          _color = icetImageGetColorcf(raw_image);
          _color+= 4*((screen_viewport[0]+_input_width*(screen_viewport[1])));
          {
            IceTByte     *_dest;
            IceTSizeType _pixels= _pixel_count;
            IceTSizeType _p;
            IceTSizeType _count;
            IceTSizeType _totalcount= 0;
            IceTSizeType _compressed_size;
            icetTimingCompressBegin();
            _dest = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
            _count= space_bottom*width;
            if ((space_left != 0)||(space_right != 0))
            {
              IceTSizeType _line,_lastline;
              for (_line= space_bottom,_lastline= height-space_top;
                   _line < _lastline; _line++)
              {
                IceTSizeType _x    = space_left;
                IceTSizeType _lastx= width-space_right;
                _count+= space_left;
                while (1)
                {
                  IceTVoid *_runlengths;
                  while ((_x < _lastx)&&(!(_depth[0] < 1.0)))
                  {
                    _x++;
                    _count++;
                    _color+= 4;
                    _depth++;
                    _region_count++;
                    if (_region_count >= _region_width){_color+= 4*_region_x_skip;_depth+= _region_x_skip;_region_count= 0;}
                  }
                  if (_x >= _lastx) break;
                  _runlengths                               = _dest;
                  _dest                                    += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                  (((IceTRunLengthType *) (_runlengths))[0])= _count;
                  _totalcount                              += _count;
                  _count                                    = 0;
                  while ((_x < _lastx)&&(_depth[0] < 1.0))
                  {
                    _out   = (IceTFloat *) _dest;
                    _out[0]= _color[0];
                    _out[1]= _color[1];
                    _out[2]= _color[2];
                    _out[3]= _color[3];
                    _out[4]= _depth[0];
                    _dest += 5*sizeof(IceTFloat);
                    _color+= 4;
                    _depth++;
                    _region_count++;
                    if (_region_count >= _region_width){_color+= 4*_region_x_skip;_depth+= _region_x_skip;_region_count= 0;}
                    _count++;
                    _x++;
                  }
                  (((IceTRunLengthType *) (_runlengths))[1])= _count;
                  _totalcount                              += _count;
                  _count                                    = 0;
                  if (_x >= _lastx) break;
                }
                _count+= space_right;
              }
            } else {
              _pixels= (height-space_bottom-space_top)*width;
              _p     = 0;
              while (_p < _pixels)
              {
                IceTVoid *_runlengths= _dest;
                _dest+= ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                while ((_p < _pixels)&&(!(_depth[0] < 1.0)))
                {
                  _p++;
                  _count++;
                  _color+= 4;
                  _depth++;
                  _region_count++;
                  if (_region_count >= _region_width){_color+= 4*_region_x_skip;_depth+= _region_x_skip;_region_count= 0;}
                }
                (((IceTRunLengthType *) (_runlengths))[0])= _count;
                _totalcount                              += _count;
                _count                                    = 0;
                while ((_p < _pixels)&&(_depth[0] < 1.0))
                {
                  _out   = (IceTFloat *) _dest;
                  _out[0]= _color[0];
                  _out[1]= _color[1];
                  _out[2]= _color[2];
                  _out[3]= _color[3];
                  _out[4]= _depth[0];
                  _dest += 5*sizeof(IceTFloat);
                  _color+= 4;
                  _depth++;
                  _region_count++;
                  if (_region_count >= _region_width){_color+= 4*_region_x_skip;_depth+= _region_x_skip;_region_count= 0;}
                  _count++;
                  _p++;
                }
                (((IceTRunLengthType *) (_runlengths))[1])= _count;
                _totalcount                              += _count;
                _count                                    = 0;
              }
            }
            _count+= space_top*width;
            if (_count > 0)
            {
              (((IceTRunLengthType *) (_dest))[0])= _count;
              (((IceTRunLengthType *) (_dest))[1])= 0;
              _dest                              += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _totalcount                        += _count;
            }
            _totalcount-= (width)*(space_top+space_bottom);
            _totalcount-= ((height-(space_top+space_bottom))
                           *(space_left+space_right));
            if (_totalcount != (IceTSizeType) _pixel_count)
            {
              char msg[256];
              sprintf(msg,"Total run lengths don't equal pixel count: %d != %d",
                      (int) _totalcount,(int) (_pixel_count));
              icetRaiseDiagnostic(msg,(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/compress_template_body.h",183);
            }
            icetTimingCompressEnd();
            _compressed_size
              = (IceTSizeType)
                ((IceTPointerArithmetic) _dest
                 -(IceTPointerArithmetic) ((IceTInt *) compressed_image.opaque_internals));
            ((IceTInt *) compressed_image.opaque_internals)[6]
              = (IceTInt) _compressed_size;
          }
        } else if (_color_format == (IceTEnum) 0xC000)
        {
          IceTFloat    *_out;
          IceTSizeType _region_count= 0;
          {
            IceTByte     *_dest;
            IceTSizeType _pixels= _pixel_count;
            IceTSizeType _p;
            IceTSizeType _count;
            IceTSizeType _totalcount= 0;
            IceTSizeType _compressed_size;
            icetTimingCompressBegin();
            _dest = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
            _count= space_bottom*width;
            if ((space_left != 0)||(space_right != 0))
            {
              IceTSizeType _line,_lastline;
              for (_line= space_bottom,_lastline= height-space_top;
                   _line < _lastline; _line++)
              {
                IceTSizeType _x    = space_left;
                IceTSizeType _lastx= width-space_right;
                _count+= space_left;
                while (1)
                {
                  IceTVoid *_runlengths;
                  while ((_x < _lastx)&&(!(_depth[0] < 1.0)))
                  {
                    _x++;
                    _count++;
                    _depth++;
                    _region_count++;
                    if (_region_count >= _region_width){_depth+= _region_x_skip;_region_count= 0;}
                  }
                  if (_x >= _lastx) break;
                  _runlengths                               = _dest;
                  _dest                                    += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                  (((IceTRunLengthType *) (_runlengths))[0])= _count;
                  _totalcount                              += _count;
                  _count                                    = 0;
                  while ((_x < _lastx)&&(_depth[0] < 1.0))
                  {
                    _out   = (IceTFloat *) _dest;
                    _out[0]= _depth[0];
                    _dest += 1*sizeof(IceTFloat);
                    _depth++;
                    _region_count++;
                    if (_region_count >= _region_width){_depth+= _region_x_skip;_region_count= 0;}
                    _count++;
                    _x++;
                  }
                  (((IceTRunLengthType *) (_runlengths))[1])= _count;
                  _totalcount                              += _count;
                  _count                                    = 0;
                  if (_x >= _lastx) break;
                }
                _count+= space_right;
              }
            } else {
              _pixels= (height-space_bottom-space_top)*width;
              _p     = 0;
              while (_p < _pixels)
              {
                IceTVoid *_runlengths= _dest;
                _dest+= ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                while ((_p < _pixels)&&(!(_depth[0] < 1.0)))
                {
                  _p++;
                  _count++;
                  _depth++;
                  _region_count++;
                  if (_region_count >= _region_width){_depth+= _region_x_skip;_region_count= 0;}
                }
                (((IceTRunLengthType *) (_runlengths))[0])= _count;
                _totalcount                              += _count;
                _count                                    = 0;
                while ((_p < _pixels)&&(_depth[0] < 1.0))
                {
                  _out   = (IceTFloat *) _dest;
                  _out[0]= _depth[0];
                  _dest += 1*sizeof(IceTFloat);
                  _depth++;
                  _region_count++;
                  if (_region_count >= _region_width){_depth+= _region_x_skip;_region_count= 0;}
                  _count++;
                  _p++;
                }
                (((IceTRunLengthType *) (_runlengths))[1])= _count;
                _totalcount                              += _count;
                _count                                    = 0;
              }
            }
            _count+= space_top*width;
            if (_count > 0)
            {
              (((IceTRunLengthType *) (_dest))[0])= _count;
              (((IceTRunLengthType *) (_dest))[1])= 0;
              _dest                              += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _totalcount                        += _count;
            }
            _totalcount-= (width)*(space_top+space_bottom);
            _totalcount-= ((height-(space_top+space_bottom))
                           *(space_left+space_right));
            if (_totalcount != (IceTSizeType) _pixel_count)
            {
              char msg[256];
              sprintf(msg,"Total run lengths don't equal pixel count: %d != %d",
                      (int) _totalcount,(int) (_pixel_count));
              icetRaiseDiagnostic(msg,(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/compress_template_body.h",183);
            }
            icetTimingCompressEnd();
            _compressed_size
              = (IceTSizeType)
                ((IceTPointerArithmetic) _dest
                 -(IceTPointerArithmetic) ((IceTInt *) compressed_image.opaque_internals));
            ((IceTInt *) compressed_image.opaque_internals)[6]
              = (IceTInt) _compressed_size;
          }
        } else {
          icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                              "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                              ,246);
        }
      } else if (_depth_format == (IceTEnum) 0xD000)
      {
        icetRaiseDiagnostic("Cannot use Z buffer compression with no" " Z buffer.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                            ,250);
      } else {
        icetRaiseDiagnostic("Encountered invalid depth format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                            ,253);
      }
    } else if (_composite_mode == (IceTEnum) 0x0302)
    {
      if (_depth_format != (IceTEnum) 0xD000)
      {
        icetRaiseDiagnostic("Z buffer ignored during blend compress" " operation.  Output z buffer meaningless.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0003,
                            "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                            ,260);
      }
      if (_color_format == (IceTEnum) 0xC001)
      {
        const IceTUInt *_color;
        IceTUInt       *_out;
        IceTSizeType   _region_count= 0;
        _color = icetImageGetColorcui(raw_image);
        _color+= (screen_viewport[0]+_input_width*(screen_viewport[1]));
        {
          IceTByte     *_dest;
          IceTSizeType _pixels= _pixel_count;
          IceTSizeType _p;
          IceTSizeType _count;
          IceTSizeType _totalcount= 0;
          IceTSizeType _compressed_size;
          icetTimingCompressBegin();
          _dest = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
          _count= space_bottom*width;
          if ((space_left != 0)||(space_right != 0))
          {
            IceTSizeType _line,_lastline;
            for (_line= space_bottom,_lastline= height-space_top;
                 _line < _lastline; _line++)
            {
              IceTSizeType _x    = space_left;
              IceTSizeType _lastx= width-space_right;
              _count+= space_left;
              while (1)
              {
                IceTVoid *_runlengths;
                while ((_x < _lastx)&&(!(((IceTUByte *) _color)[3] != 0x00)))
                {
                  _x++;
                  _count++;
                  _color++;
                  _region_count++;
                  if (_region_count >= _region_width){_color+= _region_x_skip;_region_count= 0;}
                }
                if (_x >= _lastx) break;
                _runlengths                               = _dest;
                _dest                                    += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                (((IceTRunLengthType *) (_runlengths))[0])= _count;
                _totalcount                              += _count;
                _count                                    = 0;
                while ((_x < _lastx)&&(((IceTUByte *) _color)[3] != 0x00))
                {
                  _out   = (IceTUInt *) _dest;
                  _out[0]= _color[0];
                  _dest += sizeof(IceTUInt);
                  _color++;
                  _region_count++;
                  if (_region_count >= _region_width){_color+= _region_x_skip;_region_count= 0;}
                  _count++;
                  _x++;
                }
                (((IceTRunLengthType *) (_runlengths))[1])= _count;
                _totalcount                              += _count;
                _count                                    = 0;
                if (_x >= _lastx) break;
              }
              _count+= space_right;
            }
          } else {
            _pixels= (height-space_bottom-space_top)*width;
            _p     = 0;
            while (_p < _pixels)
            {
              IceTVoid *_runlengths= _dest;
              _dest+= ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              while ((_p < _pixels)&&(!(((IceTUByte *) _color)[3] != 0x00)))
              {
                _p++;
                _count++;
                _color++;
                _region_count++;
                if (_region_count >= _region_width){_color+= _region_x_skip;_region_count= 0;}
              }
              (((IceTRunLengthType *) (_runlengths))[0])= _count;
              _totalcount                              += _count;
              _count                                    = 0;
              while ((_p < _pixels)&&(((IceTUByte *) _color)[3] != 0x00))
              {
                _out   = (IceTUInt *) _dest;
                _out[0]= _color[0];
                _dest += sizeof(IceTUInt);
                _color++;
                _region_count++;
                if (_region_count >= _region_width){_color+= _region_x_skip;_region_count= 0;}
                _count++;
                _p++;
              }
              (((IceTRunLengthType *) (_runlengths))[1])= _count;
              _totalcount                              += _count;
              _count                                    = 0;
            }
          }
          _count+= space_top*width;
          if (_count > 0)
          {
            (((IceTRunLengthType *) (_dest))[0])= _count;
            (((IceTRunLengthType *) (_dest))[1])= 0;
            _dest                              += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
            _totalcount                        += _count;
          }
          _totalcount-= (width)*(space_top+space_bottom);
          _totalcount-= ((height-(space_top+space_bottom))
                         *(space_left+space_right));
          if (_totalcount != (IceTSizeType) _pixel_count)
          {
            char msg[256];
            sprintf(msg,"Total run lengths don't equal pixel count: %d != %d",
                    (int) _totalcount,(int) (_pixel_count));
            icetRaiseDiagnostic(msg,(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/compress_template_body.h",183);
          }
          icetTimingCompressEnd();
          _compressed_size
            = (IceTSizeType)
              ((IceTPointerArithmetic) _dest
               -(IceTPointerArithmetic) ((IceTInt *) compressed_image.opaque_internals));
          ((IceTInt *) compressed_image.opaque_internals)[6]
            = (IceTInt) _compressed_size;
        }
      } else if (_color_format == (IceTEnum) 0xC002)
      {
        const IceTFloat *_color;
        IceTFloat       *_out;
        IceTSizeType    _region_count= 0;
        _color = icetImageGetColorcf(raw_image);
        _color+= 4*((screen_viewport[0]+_input_width*(screen_viewport[1])));
        {
          IceTByte     *_dest;
          IceTSizeType _pixels= _pixel_count;
          IceTSizeType _p;
          IceTSizeType _count;
          IceTSizeType _totalcount= 0;
          IceTSizeType _compressed_size;
          icetTimingCompressBegin();
          _dest = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
          _count= space_bottom*width;
          if ((space_left != 0)||(space_right != 0))
          {
            IceTSizeType _line,_lastline;
            for (_line= space_bottom,_lastline= height-space_top;
                 _line < _lastline; _line++)
            {
              IceTSizeType _x    = space_left;
              IceTSizeType _lastx= width-space_right;
              _count+= space_left;
              while (1)
              {
                IceTVoid *_runlengths;
                while ((_x < _lastx)&&(!(_color[3] != 0.0)))
                {
                  _x++;
                  _count++;
                  _color+= 4;
                  _region_count++;
                  if (_region_count >= _region_width){_color+= 4*_region_x_skip;_region_count= 0;}
                }
                if (_x >= _lastx) break;
                _runlengths                               = _dest;
                _dest                                    += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                (((IceTRunLengthType *) (_runlengths))[0])= _count;
                _totalcount                              += _count;
                _count                                    = 0;
                while ((_x < _lastx)&&(_color[3] != 0.0))
                {
                  _out   = (IceTFloat *) _dest;
                  _out[0]= _color[0];
                  _out[1]= _color[1];
                  _out[2]= _color[2];
                  _out[3]= _color[3];
                  _dest += 4*sizeof(IceTUInt);
                  _color+= 4;
                  _region_count++;
                  if (_region_count >= _region_width){_color+= 4*_region_x_skip;_region_count= 0;}
                  _count++;
                  _x++;
                }
                (((IceTRunLengthType *) (_runlengths))[1])= _count;
                _totalcount                              += _count;
                _count                                    = 0;
                if (_x >= _lastx) break;
              }
              _count+= space_right;
            }
          } else {
            _pixels= (height-space_bottom-space_top)*width;
            _p     = 0;
            while (_p < _pixels)
            {
              IceTVoid *_runlengths= _dest;
              _dest+= ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              while ((_p < _pixels)&&(!(_color[3] != 0.0)))
              {
                _p++;
                _count++;
                _color+= 4;
                _region_count++;
                if (_region_count >= _region_width){_color+= 4*_region_x_skip;_region_count= 0;}
              }
              (((IceTRunLengthType *) (_runlengths))[0])= _count;
              _totalcount                              += _count;
              _count                                    = 0;
              while ((_p < _pixels)&&(_color[3] != 0.0))
              {
                _out   = (IceTFloat *) _dest;
                _out[0]= _color[0];
                _out[1]= _color[1];
                _out[2]= _color[2];
                _out[3]= _color[3];
                _dest += 4*sizeof(IceTUInt);
                _color+= 4;
                _region_count++;
                if (_region_count >= _region_width){_color+= 4*_region_x_skip;_region_count= 0;}
                _count++;
                _p++;
              }
              (((IceTRunLengthType *) (_runlengths))[1])= _count;
              _totalcount                              += _count;
              _count                                    = 0;
            }
          }
          _count+= space_top*width;
          if (_count > 0)
          {
            (((IceTRunLengthType *) (_dest))[0])= _count;
            (((IceTRunLengthType *) (_dest))[1])= 0;
            _dest                              += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
            _totalcount                        += _count;
          }
          _totalcount-= (width)*(space_top+space_bottom);
          _totalcount-= ((height-(space_top+space_bottom))
                         *(space_left+space_right));
          if (_totalcount != (IceTSizeType) _pixel_count)
          {
            char msg[256];
            sprintf(msg,"Total run lengths don't equal pixel count: %d != %d",
                    (int) _totalcount,(int) (_pixel_count));
            icetRaiseDiagnostic(msg,(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/compress_template_body.h",183);
          }
          icetTimingCompressEnd();
          _compressed_size
            = (IceTSizeType)
              ((IceTPointerArithmetic) _dest
               -(IceTPointerArithmetic) ((IceTInt *) compressed_image.opaque_internals));
          ((IceTInt *) compressed_image.opaque_internals)[6]
            = (IceTInt) _compressed_size;
        }
      } else if (_color_format == (IceTEnum) 0xC000)
      {
        IceTUInt *_out;
        icetRaiseDiagnostic("Compressing image with no data.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0003,
                            "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                            ,344);
        _out                               = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
        (((IceTRunLengthType *) (_out))[0])= _pixel_count;
        (((IceTRunLengthType *) (_out))[1])= 0;
        _out++;
        icetSparseImageSetActualSize(compressed_image,_out);
      } else {
        icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                            ,352);
      }
    } else {
      icetRaiseDiagnostic("Encountered invalid composite mode.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                          ,356);
    }
    {char msg_string[256];
     sprintf(msg_string,"Compression: %f%%\n",100.0f-(100.0f*icetSparseImageGetCompressedBufferSize(compressed_image)/icetImageBufferSizeType(_color_format,_depth_format,icetSparseImageGetWidth(compressed_image),icetSparseImageGetHeight(compressed_image))));
     icetRaiseDiagnostic(msg_string,(IceTEnum) 0x00000000,(IceTEnum) 0x0007,
                         "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                         ,363
                         );
    }
  }
}

void icetCompressImage(const IceTImage image,
                       IceTSparseImage compressed_image)
{
  icetCompressSubImage(image,0,icetImageGetNumPixels(image),
                       compressed_image);
  ((IceTInt *) compressed_image.opaque_internals)[3]
    = (IceTInt) icetImageGetWidth(image);
  ((IceTInt *) compressed_image.opaque_internals)[4]
    = (IceTInt) icetImageGetHeight(image);
}

void icetCompressSubImage(const IceTImage image,
                          IceTSizeType offset,IceTSizeType pixels,
                          IceTSparseImage compressed_image)
{
  ICET_TEST_IMAGE_HEADER(image);
  ICET_TEST_SPARSE_IMAGE_HEADER(compressed_image);
  icetSparseImageSetDimensions(compressed_image,pixels,1);
  {
    IceTEnum     _color_format,_depth_format;
    IceTSizeType _pixel_count;
    IceTEnum     _composite_mode;
    icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0028),&_composite_mode);
    _color_format= icetImageGetColorFormat(image);
    _depth_format= icetImageGetDepthFormat(image);
    _pixel_count = pixels;
    if ((icetSparseImageGetColorFormat(compressed_image) != _color_format)
        ||(icetSparseImageGetDepthFormat(compressed_image) != _depth_format)
        )
    {
      icetRaiseDiagnostic("Format of input and output to compress do not match.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                          ,92);
    }
    if (icetSparseImageGetNumPixels(compressed_image) != _pixel_count)
    {
      icetRaiseDiagnostic("Size of input and output to compress do not match.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                          ,105);
    }
    if (_composite_mode == (IceTEnum) 0x0301)
    {
      if (_depth_format == (IceTEnum) 0xD001)
      {
        const IceTFloat *_depth= icetImageGetDepthcf(image);
        _depth+= offset;
        if (_color_format == (IceTEnum) 0xC001)
        {
          const IceTUInt *_color;
          IceTUInt       *_c_out;
          IceTFloat      *_d_out;
          _color = icetImageGetColorcui(image);
          _color+= offset;
          {
            IceTByte     *_dest;
            IceTSizeType _pixels= _pixel_count;
            IceTSizeType _p;
            IceTSizeType _count;
            IceTSizeType _totalcount= 0;
            IceTSizeType _compressed_size;
            icetTimingCompressBegin();
            _dest = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
            _count= 0;
            _p    = 0;
            while (_p < _pixels)
            {
              IceTVoid *_runlengths= _dest;
              _dest+= ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              while ((_p < _pixels)&&(!(_depth[0] < 1.0)))
              {
                _p++;
                _count++;
                _color++;
                _depth++;
              }
              (((IceTRunLengthType *) (_runlengths))[0])= _count;
              _totalcount                              += _count;
              _count                                    = 0;
              while ((_p < _pixels)&&(_depth[0] < 1.0))
              {
                _c_out   = (IceTUInt *) _dest;
                _c_out[0]= _color[0];
                _dest   += sizeof(IceTUInt);
                _d_out   = (IceTFloat *) _dest;
                _d_out[0]= _depth[0];
                _dest   += sizeof(IceTFloat);
                _color++;
                _depth++;
                _count++;
                _p++;
              }
              (((IceTRunLengthType *) (_runlengths))[1])= _count;
              _totalcount                              += _count;
              _count                                    = 0;
            }
            if (_totalcount != (IceTSizeType) _pixel_count)
            {
              char msg[256];
              sprintf(msg,"Total run lengths don't equal pixel count: %d != %d",
                      (int) _totalcount,(int) (_pixel_count));
              icetRaiseDiagnostic(msg,(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/compress_template_body.h",183);
            }
            icetTimingCompressEnd();
            _compressed_size
              = (IceTSizeType)
                ((IceTPointerArithmetic) _dest
                 -(IceTPointerArithmetic) ((IceTInt *) compressed_image.opaque_internals));
            ((IceTInt *) compressed_image.opaque_internals)[6]
              = (IceTInt) _compressed_size;
          }
        } else if (_color_format == (IceTEnum) 0xC002)
        {
          const IceTFloat *_color;
          IceTFloat       *_out;
          _color = icetImageGetColorcf(image);
          _color+= 4*(offset);
          {
            IceTByte     *_dest;
            IceTSizeType _pixels= _pixel_count;
            IceTSizeType _p;
            IceTSizeType _count;
            IceTSizeType _totalcount= 0;
            IceTSizeType _compressed_size;
            icetTimingCompressBegin();
            _dest = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
            _count= 0;
            _p    = 0;
            while (_p < _pixels)
            {
              IceTVoid *_runlengths= _dest;
              _dest+= ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              while ((_p < _pixels)&&(!(_depth[0] < 1.0)))
              {
                _p++;
                _count++;
                _color+= 4;
                _depth++;
              }
              (((IceTRunLengthType *) (_runlengths))[0])= _count;
              _totalcount                              += _count;
              _count                                    = 0;
              while ((_p < _pixels)&&(_depth[0] < 1.0))
              {
                _out   = (IceTFloat *) _dest;
                _out[0]= _color[0];
                _out[1]= _color[1];
                _out[2]= _color[2];
                _out[3]= _color[3];
                _out[4]= _depth[0];
                _dest += 5*sizeof(IceTFloat);
                _color+= 4;
                _depth++;
                _count++;
                _p++;
              }
              (((IceTRunLengthType *) (_runlengths))[1])= _count;
              _totalcount                              += _count;
              _count                                    = 0;
            }
            if (_totalcount != (IceTSizeType) _pixel_count)
            {
              char msg[256];
              sprintf(msg,"Total run lengths don't equal pixel count: %d != %d",
                      (int) _totalcount,(int) (_pixel_count));
              icetRaiseDiagnostic(msg,(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/compress_template_body.h",183);
            }
            icetTimingCompressEnd();
            _compressed_size
              = (IceTSizeType)
                ((IceTPointerArithmetic) _dest
                 -(IceTPointerArithmetic) ((IceTInt *) compressed_image.opaque_internals));
            ((IceTInt *) compressed_image.opaque_internals)[6]
              = (IceTInt) _compressed_size;
          }
        } else if (_color_format == (IceTEnum) 0xC000)
        {
          IceTFloat *_out;
          {
            IceTByte     *_dest;
            IceTSizeType _pixels= _pixel_count;
            IceTSizeType _p;
            IceTSizeType _count;
            IceTSizeType _totalcount= 0;
            IceTSizeType _compressed_size;
            icetTimingCompressBegin();
            _dest = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
            _count= 0;
            _p    = 0;
            while (_p < _pixels)
            {
              IceTVoid *_runlengths= _dest;
              _dest+= ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              while ((_p < _pixels)&&(!(_depth[0] < 1.0)))
              {
                _p++;
                _count++;
                _depth++;
              }
              (((IceTRunLengthType *) (_runlengths))[0])= _count;
              _totalcount                              += _count;
              _count                                    = 0;
              while ((_p < _pixels)&&(_depth[0] < 1.0))
              {
                _out   = (IceTFloat *) _dest;
                _out[0]= _depth[0];
                _dest += 1*sizeof(IceTFloat);
                _depth++;
                _count++;
                _p++;
              }
              (((IceTRunLengthType *) (_runlengths))[1])= _count;
              _totalcount                              += _count;
              _count                                    = 0;
            }
            if (_totalcount != (IceTSizeType) _pixel_count)
            {
              char msg[256];
              sprintf(msg,"Total run lengths don't equal pixel count: %d != %d",
                      (int) _totalcount,(int) (_pixel_count));
              icetRaiseDiagnostic(msg,(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/compress_template_body.h",183);
            }
            icetTimingCompressEnd();
            _compressed_size
              = (IceTSizeType)
                ((IceTPointerArithmetic) _dest
                 -(IceTPointerArithmetic) ((IceTInt *) compressed_image.opaque_internals));
            ((IceTInt *) compressed_image.opaque_internals)[6]
              = (IceTInt) _compressed_size;
          }
        } else {
          icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                              "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                              ,246);
        }
      } else if (_depth_format == (IceTEnum) 0xD000)
      {
        icetRaiseDiagnostic("Cannot use Z buffer compression with no" " Z buffer.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                            ,250);
      } else {
        icetRaiseDiagnostic("Encountered invalid depth format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                            ,253);
      }
    } else if (_composite_mode == (IceTEnum) 0x0302)
    {
      if (_depth_format != (IceTEnum) 0xD000)
      {
        icetRaiseDiagnostic("Z buffer ignored during blend compress" " operation.  Output z buffer meaningless.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0003,
                            "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                            ,260);
      }
      if (_color_format == (IceTEnum) 0xC001)
      {
        const IceTUInt *_color;
        IceTUInt       *_out;
        _color = icetImageGetColorcui(image);
        _color+= offset;
        {
          IceTByte     *_dest;
          IceTSizeType _pixels= _pixel_count;
          IceTSizeType _p;
          IceTSizeType _count;
          IceTSizeType _totalcount= 0;
          IceTSizeType _compressed_size;
          icetTimingCompressBegin();
          _dest = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
          _count= 0;
          _p    = 0;
          while (_p < _pixels)
          {
            IceTVoid *_runlengths= _dest;
            _dest+= ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
            while ((_p < _pixels)&&(!(((IceTUByte *) _color)[3] != 0x00)))
            {
              _p++;
              _count++;
              _color++;
            }
            (((IceTRunLengthType *) (_runlengths))[0])= _count;
            _totalcount                              += _count;
            _count                                    = 0;
            while ((_p < _pixels)&&(((IceTUByte *) _color)[3] != 0x00))
            {
              _out   = (IceTUInt *) _dest;
              _out[0]= _color[0];
              _dest += sizeof(IceTUInt);
              _color++;
              _count++;
              _p++;
            }
            (((IceTRunLengthType *) (_runlengths))[1])= _count;
            _totalcount                              += _count;
            _count                                    = 0;
          }
          if (_totalcount != (IceTSizeType) _pixel_count)
          {
            char msg[256];
            sprintf(msg,"Total run lengths don't equal pixel count: %d != %d",
                    (int) _totalcount,(int) (_pixel_count));
            icetRaiseDiagnostic(msg,(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/compress_template_body.h",183);
          }
          icetTimingCompressEnd();
          _compressed_size
            = (IceTSizeType)
              ((IceTPointerArithmetic) _dest
               -(IceTPointerArithmetic) ((IceTInt *) compressed_image.opaque_internals));
          ((IceTInt *) compressed_image.opaque_internals)[6]
            = (IceTInt) _compressed_size;
        }
      } else if (_color_format == (IceTEnum) 0xC002)
      {
        const IceTFloat *_color;
        IceTFloat       *_out;
        _color = icetImageGetColorcf(image);
        _color+= 4*(offset);
        {
          IceTByte     *_dest;
          IceTSizeType _pixels= _pixel_count;
          IceTSizeType _p;
          IceTSizeType _count;
          IceTSizeType _totalcount= 0;
          IceTSizeType _compressed_size;
          icetTimingCompressBegin();
          _dest = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
          _count= 0;
          _p    = 0;
          while (_p < _pixels)
          {
            IceTVoid *_runlengths= _dest;
            _dest+= ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
            while ((_p < _pixels)&&(!(_color[3] != 0.0)))
            {
              _p++;
              _count++;
              _color+= 4;
            }
            (((IceTRunLengthType *) (_runlengths))[0])= _count;
            _totalcount                              += _count;
            _count                                    = 0;
            while ((_p < _pixels)&&(_color[3] != 0.0))
            {
              _out   = (IceTFloat *) _dest;
              _out[0]= _color[0];
              _out[1]= _color[1];
              _out[2]= _color[2];
              _out[3]= _color[3];
              _dest += 4*sizeof(IceTUInt);
              _color+= 4;
              _count++;
              _p++;
            }
            (((IceTRunLengthType *) (_runlengths))[1])= _count;
            _totalcount                              += _count;
            _count                                    = 0;
          }
          if (_totalcount != (IceTSizeType) _pixel_count)
          {
            char msg[256];
            sprintf(msg,"Total run lengths don't equal pixel count: %d != %d",
                    (int) _totalcount,(int) (_pixel_count));
            icetRaiseDiagnostic(msg,(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/compress_template_body.h",183);
          }
          icetTimingCompressEnd();
          _compressed_size
            = (IceTSizeType)
              ((IceTPointerArithmetic) _dest
               -(IceTPointerArithmetic) ((IceTInt *) compressed_image.opaque_internals));
          ((IceTInt *) compressed_image.opaque_internals)[6]
            = (IceTInt) _compressed_size;
        }
      } else if (_color_format == (IceTEnum) 0xC000)
      {
        IceTUInt *_out;
        icetRaiseDiagnostic("Compressing image with no data.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0003,
                            "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                            ,344);
        _out                               = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
        (((IceTRunLengthType *) (_out))[0])= _pixel_count;
        (((IceTRunLengthType *) (_out))[1])= 0;
        _out++;
        icetSparseImageSetActualSize(compressed_image,_out);
      } else {
        icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                            ,352);
      }
    } else {
      icetRaiseDiagnostic("Encountered invalid composite mode.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                          ,356);
    }
    {char msg_string[256];
     sprintf(msg_string,"Compression: %f%%\n",100.0f-(100.0f*icetSparseImageGetCompressedBufferSize(compressed_image)/icetImageBufferSizeType(_color_format,_depth_format,icetSparseImageGetWidth(compressed_image),icetSparseImageGetHeight(compressed_image))));
     icetRaiseDiagnostic(msg_string,(IceTEnum) 0x00000000,(IceTEnum) 0x0007,
                         "/nfshome/lmlediae/git/icet/src/ice-t/compress_func_body.h"
                         ,363
                         );
    }
  }
}

void icetDecompressImage(const IceTSparseImage compressed_image,
                         IceTImage image)
{
  icetImageSetDimensions(image,
                         icetSparseImageGetWidth(compressed_image),
                         icetSparseImageGetHeight(compressed_image));
  icetDecompressSubImage(compressed_image,0,image);
}

void icetDecompressSubImage(const IceTSparseImage compressed_image,
                            IceTSizeType offset,
                            IceTImage image)
{
  ICET_TEST_IMAGE_HEADER(image);
  ICET_TEST_SPARSE_IMAGE_HEADER(compressed_image);
  {
    IceTEnum     _color_format,_depth_format;
    IceTSizeType _pixel_count;
    IceTEnum     _composite_mode;
    icetTimingCompressBegin();
    icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0028),&_composite_mode);
    _color_format= icetSparseImageGetColorFormat(compressed_image);
    _depth_format= icetSparseImageGetDepthFormat(compressed_image);
    _pixel_count = icetSparseImageGetNumPixels(compressed_image);
    if (_color_format != icetImageGetColorFormat(image))
    {
      icetRaiseDiagnostic("Input/output buffers have different color formats.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                          ,80);
    }
    if (_depth_format != icetImageGetDepthFormat(image))
    {
      icetRaiseDiagnostic("Input/output buffers have different depth formats.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                          ,84);
    }
    if (_pixel_count != icetSparseImageGetNumPixels(compressed_image))
    {
      icetRaiseDiagnostic("Unexpected input pixel count.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                          ,89);
    }
    if (_pixel_count > icetImageGetNumPixels(image)-offset)
    {
      icetRaiseDiagnostic("Offset pixels outside range of output image.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                          ,100);
    }
    if (_composite_mode == (IceTEnum) 0x0301)
    {
      if (_depth_format == (IceTEnum) 0xD001)
      {
        IceTFloat *_depth= icetImageGetDepthf(image);
        _depth+= offset;
        if (_color_format == (IceTEnum) 0xC001)
        {
          IceTUInt        *_color;
          const IceTUInt  *_c_in;
          const IceTFloat *_d_in;
          IceTUInt        _background_color;
          _color = icetImageGetColorui(image);
          _color+= offset;
          icetGetIntegerv(((IceTEnum) 0x00000000|(IceTEnum) 0x0006),
                          (IceTInt *) &_background_color);
          {
            const IceTByte *_src;
            IceTSizeType   _pixels;
            IceTSizeType   _p;
            IceTSizeType   _i;
            _pixels= icetSparseImageGetNumPixels(compressed_image);
            _src   = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
            _p     = 0;
            while (_p < _pixels)
            {
              const IceTVoid *_runlengths;
              IceTSizeType   _rl;
              _runlengths= _src;
              _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
              _p        += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                break;
              }
              {IceTSizeType __i;
               for (__i= 0; __i < _rl; __i++)
               {
                 *(_color++)= _background_color;
                 *(_depth++)= 1.0f;
               }
              }
              _rl= (((IceTRunLengthType *) (_runlengths))[1]);
              _p+= _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                break;
              }
              for (_i= 0; _i < _rl; _i++)
              {
                _c_in    = (IceTUInt *) _src;
                _src    += sizeof(IceTUInt);
                _d_in    = (IceTFloat *) _src;
                _src    += sizeof(IceTFloat);
                _color[0]= _c_in[0];
                _depth[0]= _d_in[0];
                _color++;
                _depth++;
              }
            }
          }
        } else if (_color_format == (IceTEnum) 0xC002)
        {
          IceTFloat       *_color;
          const IceTFloat *_c_in;
          const IceTFloat *_d_in;
          IceTFloat       _background_color[4];
          _color = icetImageGetColorf(image);
          _color+= 4*(offset);
          icetGetFloatv(((IceTEnum) 0x00000000|(IceTEnum) 0x0005),_background_color);
          {
            const IceTByte *_src;
            IceTSizeType   _pixels;
            IceTSizeType   _p;
            IceTSizeType   _i;
            _pixels= icetSparseImageGetNumPixels(compressed_image);
            _src   = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
            _p     = 0;
            while (_p < _pixels)
            {
              const IceTVoid *_runlengths;
              IceTSizeType   _rl;
              _runlengths= _src;
              _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
              _p        += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                break;
              }
              {IceTSizeType __i;
               for (__i= 0; __i < _rl; __i++)
               {
                 _color[0]  = _background_color[0];
                 _color[1]  = _background_color[1];
                 _color[2]  = _background_color[2];
                 _color[3]  = _background_color[3];
                 _color    += 4;
                 *(_depth++)= 1.0f;
               }
              }
              _rl= (((IceTRunLengthType *) (_runlengths))[1]);
              _p+= _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                break;
              }
              for (_i= 0; _i < _rl; _i++)
              {
                _c_in    = (IceTFloat *) _src;
                _src    += 4*sizeof(IceTFloat);
                _d_in    = (IceTFloat *) _src;
                _src    += sizeof(IceTFloat);
                _color[0]= _c_in[0];
                _color[1]= _c_in[1];
                _color[2]= _c_in[2];
                _color[3]= _c_in[3];
                _depth[0]= _d_in[0];
                _color  += 4;
                _depth++;
              }
            }
          }
        } else if (_color_format == (IceTEnum) 0xC000)
        {
          const IceTFloat *_d_in;
          {
            const IceTByte *_src;
            IceTSizeType   _pixels;
            IceTSizeType   _p;
            IceTSizeType   _i;
            _pixels= icetSparseImageGetNumPixels(compressed_image);
            _src   = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
            _p     = 0;
            while (_p < _pixels)
            {
              const IceTVoid *_runlengths;
              IceTSizeType   _rl;
              _runlengths= _src;
              _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
              _p        += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                break;
              }
              {IceTSizeType __i;
               for (__i= 0; __i < _rl; __i++)
               {
                 *(_depth++)= 1.0f;
               }
              }
              _rl= (((IceTRunLengthType *) (_runlengths))[1]);
              _p+= _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                break;
              }
              for (_i= 0; _i < _rl; _i++)
              {
                _d_in    = (IceTFloat *) _src;
                _src    += sizeof(IceTFloat);
                _depth[0]= _d_in[0];
                _depth++;
              }
            }
          }
        } else {
          icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                              "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                              ,239);
        }
      } else if (_depth_format == (IceTEnum) 0xD000)
      {
        icetRaiseDiagnostic("Cannot use Z buffer compositing operation with no" " Z buffer.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,243);
      } else {
        icetRaiseDiagnostic("Encountered invalid depth format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,246);
      }
    } else if (_composite_mode == (IceTEnum) 0x0302)
    {
      if (_depth_format != (IceTEnum) 0xD000)
      {
        icetRaiseDiagnostic("Z buffer ignored during blend composite" " operation.  Output z buffer meaningless.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0003,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,253);
      }
      if (_color_format == (IceTEnum) 0xC001)
      {
        IceTUInt       *_color;
        const IceTUInt *_c_in;
        IceTUInt       _background_color;
        _color = icetImageGetColorui(image);
        _color+= offset;
        icetGetIntegerv(((IceTEnum) 0x00000000|(IceTEnum) 0x0006),
                        (IceTInt *) &_background_color);
        {
          const IceTByte *_src;
          IceTSizeType   _pixels;
          IceTSizeType   _p;
          IceTSizeType   _i;
          _pixels= icetSparseImageGetNumPixels(compressed_image);
          _src   = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
          _p     = 0;
          while (_p < _pixels)
          {
            const IceTVoid *_runlengths;
            IceTSizeType   _rl;
            _runlengths= _src;
            _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
            _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
            _p        += _rl;
            if (_p > _pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
              break;
            }
            {IceTSizeType __i;
             for (__i= 0; __i < _rl; __i++)
             {
               *(_color++)= _background_color;
             }
            }
            _rl= (((IceTRunLengthType *) (_runlengths))[1]);
            _p+= _rl;
            if (_p > _pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
              break;
            }
            for (_i= 0; _i < _rl; _i++)
            {
              _c_in    = (IceTUInt *) _src;
              _src    += sizeof(IceTUInt);
              _color[0]= _c_in[0];
              _color++;
            }
          }
        }
      } else if (_color_format == (IceTEnum) 0xC002)
      {
        IceTFloat       *_color;
        const IceTFloat *_c_in;
        IceTFloat       _background_color[4];
        _color = icetImageGetColorf(image);
        _color+= 4*(offset);
        icetGetFloatv(((IceTEnum) 0x00000000|(IceTEnum) 0x0005),_background_color);
        {
          const IceTByte *_src;
          IceTSizeType   _pixels;
          IceTSizeType   _p;
          IceTSizeType   _i;
          _pixels= icetSparseImageGetNumPixels(compressed_image);
          _src   = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
          _p     = 0;
          while (_p < _pixels)
          {
            const IceTVoid *_runlengths;
            IceTSizeType   _rl;
            _runlengths= _src;
            _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
            _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
            _p        += _rl;
            if (_p > _pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
              break;
            }
            {IceTSizeType __i;
             for (__i= 0; __i < _rl; __i++)
             {
               _color[0]= _background_color[0];
               _color[1]= _background_color[1];
               _color[2]= _background_color[2];
               _color[3]= _background_color[3];
               _color  += 4;
             }
            }
            _rl= (((IceTRunLengthType *) (_runlengths))[1]);
            _p+= _rl;
            if (_p > _pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
              break;
            }
            for (_i= 0; _i < _rl; _i++)
            {
              _c_in    = (IceTFloat *) _src;
              _src    += 4*sizeof(IceTFloat);
              _color[0]= _c_in[0];
              _color[1]= _c_in[1];
              _color[2]= _c_in[2];
              _color[3]= _c_in[3];
              _color  += 4;
            }
          }
        }
      } else if (_color_format == (IceTEnum) 0xC000)
      {
        icetRaiseDiagnostic("Decompressing image with no data.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0003,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,349);
      } else {
        icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,352);
      }
    } else {
      icetRaiseDiagnostic("Encountered invalid composite mode.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                          ,356);
    }
    icetTimingCompressEnd();
  }
}

void icetDecompressImageCorrectBackground(const IceTSparseImage compressed_image,
                                          IceTImage image)
{
  icetImageSetDimensions(image,
                         icetSparseImageGetWidth(compressed_image),
                         icetSparseImageGetHeight(compressed_image));
  icetDecompressSubImageCorrectBackground(compressed_image,0,image);
}

void icetDecompressSubImageCorrectBackground(
  const IceTSparseImage compressed_image,
  IceTSizeType offset,
  IceTImage image)
{
  IceTBoolean     need_correction;
  const IceTFloat *background_color;
  const IceTUByte *background_color_word;
  icetGetBooleanv(((IceTEnum) 0x00000080|(IceTEnum) 0x000C),&need_correction);
  if (!need_correction)
  {
    icetDecompressSubImage(compressed_image,offset,image);
  }
  ICET_TEST_IMAGE_HEADER(image);
  ICET_TEST_SPARSE_IMAGE_HEADER(compressed_image);
  background_color     = icetUnsafeStateGetFloat(((IceTEnum) 0x00000080|(IceTEnum) 0x000D));
  background_color_word=
    (IceTUByte *) icetUnsafeStateGetInteger(((IceTEnum) 0x00000080|(IceTEnum) 0x000E));
  {
    IceTEnum     _color_format,_depth_format;
    IceTSizeType _pixel_count;
    IceTEnum     _composite_mode;
    icetTimingCompressBegin();
    icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0028),&_composite_mode);
    _color_format= icetSparseImageGetColorFormat(compressed_image);
    _depth_format= icetSparseImageGetDepthFormat(compressed_image);
    _pixel_count = icetSparseImageGetNumPixels(compressed_image);
    if (_color_format != icetImageGetColorFormat(image))
    {
      icetRaiseDiagnostic("Input/output buffers have different color formats.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                          ,80);
    }
    if (_depth_format != icetImageGetDepthFormat(image))
    {
      icetRaiseDiagnostic("Input/output buffers have different depth formats.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                          ,84);
    }
    if (_pixel_count != icetSparseImageGetNumPixels(compressed_image))
    {
      icetRaiseDiagnostic("Unexpected input pixel count.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                          ,89);
    }
    if (_pixel_count > icetImageGetNumPixels(image)-offset)
    {
      icetRaiseDiagnostic("Offset pixels outside range of output image.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                          ,100);
    }
    if (_composite_mode == (IceTEnum) 0x0301)
    {
      if (_depth_format == (IceTEnum) 0xD001)
      {
        IceTFloat *_depth= icetImageGetDepthf(image);
        _depth+= offset;
        if (_color_format == (IceTEnum) 0xC001)
        {
          IceTUInt        *_color;
          const IceTUInt  *_c_in;
          const IceTFloat *_d_in;
          IceTUInt        _background_color;
          _color = icetImageGetColorui(image);
          _color+= offset;
          icetGetIntegerv(((IceTEnum) 0x00000000|(IceTEnum) 0x0006),
                          (IceTInt *) &_background_color);
          {
            const IceTByte *_src;
            IceTSizeType   _pixels;
            IceTSizeType   _p;
            IceTSizeType   _i;
            _pixels= icetSparseImageGetNumPixels(compressed_image);
            _src   = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
            _p     = 0;
            while (_p < _pixels)
            {
              const IceTVoid *_runlengths;
              IceTSizeType   _rl;
              _runlengths= _src;
              _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
              _p        += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                break;
              }
              {IceTSizeType __i;
               for (__i= 0; __i < _rl; __i++)
               {
                 *(_color++)= _background_color;
                 *(_depth++)= 1.0f;
               }
              }
              _rl= (((IceTRunLengthType *) (_runlengths))[1]);
              _p+= _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                break;
              }
              for (_i= 0; _i < _rl; _i++)
              {
                _c_in    = (IceTUInt *) _src;
                _src    += sizeof(IceTUInt);
                _d_in    = (IceTFloat *) _src;
                _src    += sizeof(IceTFloat);
                _color[0]= _c_in[0];
                _depth[0]= _d_in[0];
                _color++;
                _depth++;
              }
            }
          }
        } else if (_color_format == (IceTEnum) 0xC002)
        {
          IceTFloat       *_color;
          const IceTFloat *_c_in;
          const IceTFloat *_d_in;
          IceTFloat       _background_color[4];
          _color = icetImageGetColorf(image);
          _color+= 4*(offset);
          icetGetFloatv(((IceTEnum) 0x00000000|(IceTEnum) 0x0005),_background_color);
          {
            const IceTByte *_src;
            IceTSizeType   _pixels;
            IceTSizeType   _p;
            IceTSizeType   _i;
            _pixels= icetSparseImageGetNumPixels(compressed_image);
            _src   = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
            _p     = 0;
            while (_p < _pixels)
            {
              const IceTVoid *_runlengths;
              IceTSizeType   _rl;
              _runlengths= _src;
              _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
              _p        += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                break;
              }
              {IceTSizeType __i;
               for (__i= 0; __i < _rl; __i++)
               {
                 _color[0]  = _background_color[0];
                 _color[1]  = _background_color[1];
                 _color[2]  = _background_color[2];
                 _color[3]  = _background_color[3];
                 _color    += 4;
                 *(_depth++)= 1.0f;
               }
              }
              _rl= (((IceTRunLengthType *) (_runlengths))[1]);
              _p+= _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                break;
              }
              for (_i= 0; _i < _rl; _i++)
              {
                _c_in    = (IceTFloat *) _src;
                _src    += 4*sizeof(IceTFloat);
                _d_in    = (IceTFloat *) _src;
                _src    += sizeof(IceTFloat);
                _color[0]= _c_in[0];
                _color[1]= _c_in[1];
                _color[2]= _c_in[2];
                _color[3]= _c_in[3];
                _depth[0]= _d_in[0];
                _color  += 4;
                _depth++;
              }
            }
          }
        } else if (_color_format == (IceTEnum) 0xC000)
        {
          const IceTFloat *_d_in;
          {
            const IceTByte *_src;
            IceTSizeType   _pixels;
            IceTSizeType   _p;
            IceTSizeType   _i;
            _pixels= icetSparseImageGetNumPixels(compressed_image);
            _src   = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
            _p     = 0;
            while (_p < _pixels)
            {
              const IceTVoid *_runlengths;
              IceTSizeType   _rl;
              _runlengths= _src;
              _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
              _p        += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                break;
              }
              {IceTSizeType __i;
               for (__i= 0; __i < _rl; __i++)
               {
                 *(_depth++)= 1.0f;
               }
              }
              _rl= (((IceTRunLengthType *) (_runlengths))[1]);
              _p+= _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                break;
              }
              for (_i= 0; _i < _rl; _i++)
              {
                _d_in    = (IceTFloat *) _src;
                _src    += sizeof(IceTFloat);
                _depth[0]= _d_in[0];
                _depth++;
              }
            }
          }
        } else {
          icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                              "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                              ,239);
        }
      } else if (_depth_format == (IceTEnum) 0xD000)
      {
        icetRaiseDiagnostic("Cannot use Z buffer compositing operation with no" " Z buffer.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,243);
      } else {
        icetRaiseDiagnostic("Encountered invalid depth format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,246);
      }
    } else if (_composite_mode == (IceTEnum) 0x0302)
    {
      if (_depth_format != (IceTEnum) 0xD000)
      {
        icetRaiseDiagnostic("Z buffer ignored during blend composite" " operation.  Output z buffer meaningless.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0003,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,253);
      }
      if (_color_format == (IceTEnum) 0xC001)
      {
        IceTUInt       *_color;
        const IceTUInt *_c_in;
        IceTUInt       _background_color;
        _color = icetImageGetColorui(image);
        _color+= offset;
        icetGetIntegerv(((IceTEnum) 0x00000080|(IceTEnum) 0x000E),
                        (IceTInt *) &_background_color);
        {
          const IceTByte *_src;
          IceTSizeType   _pixels;
          IceTSizeType   _p;
          IceTSizeType   _i;
          _pixels= icetSparseImageGetNumPixels(compressed_image);
          _src   = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
          _p     = 0;
          while (_p < _pixels)
          {
            const IceTVoid *_runlengths;
            IceTSizeType   _rl;
            _runlengths= _src;
            _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
            _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
            _p        += _rl;
            if (_p > _pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
              break;
            }
            {IceTSizeType __i;
             for (__i= 0; __i < _rl; __i++)
             {
               *(_color++)= _background_color;
             }
            }
            _rl= (((IceTRunLengthType *) (_runlengths))[1]);
            _p+= _rl;
            if (_p > _pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
              break;
            }
            for (_i= 0; _i < _rl; _i++)
            {
              _c_in= (IceTUInt *) _src;
              _src+= sizeof(IceTUInt);
              {IceTUInt afactor= 255-(((IceTUByte *) _c_in))[3];
               (((IceTUByte *) _color))[0]= (IceTUByte) (((((IceTUByte *) &_background_color))[0]*afactor)/255+(((IceTUByte *) _c_in))[0]);
               (((IceTUByte *) _color))[1]= (IceTUByte) (((((IceTUByte *) &_background_color))[1]*afactor)/255+(((IceTUByte *) _c_in))[1]);
               (((IceTUByte *) _color))[2]= (IceTUByte) (((((IceTUByte *) &_background_color))[2]*afactor)/255+(((IceTUByte *) _c_in))[2]);
               (((IceTUByte *) _color))[3]= (IceTUByte) (((((IceTUByte *) &_background_color))[3]*afactor)/255+(((IceTUByte *) _c_in))[3]);
              }  _color++;
            }
          }
        }
      } else if (_color_format == (IceTEnum) 0xC002)
      {
        IceTFloat       *_color;
        const IceTFloat *_c_in;
        IceTFloat       _background_color[4];
        _color = icetImageGetColorf(image);
        _color+= 4*(offset);
        icetGetFloatv(((IceTEnum) 0x00000080|(IceTEnum) 0x000D),_background_color);
        {
          const IceTByte *_src;
          IceTSizeType   _pixels;
          IceTSizeType   _p;
          IceTSizeType   _i;
          _pixels= icetSparseImageGetNumPixels(compressed_image);
          _src   = ((IceTVoid *) &(((IceTInt *) compressed_image.opaque_internals)[7]));
          _p     = 0;
          while (_p < _pixels)
          {
            const IceTVoid *_runlengths;
            IceTSizeType   _rl;
            _runlengths= _src;
            _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
            _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
            _p        += _rl;
            if (_p > _pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
              break;
            }
            {IceTSizeType __i;
             for (__i= 0; __i < _rl; __i++)
             {
               _color[0]= _background_color[0];
               _color[1]= _background_color[1];
               _color[2]= _background_color[2];
               _color[3]= _background_color[3];
               _color  += 4;
             }
            }
            _rl= (((IceTRunLengthType *) (_runlengths))[1]);
            _p+= _rl;
            if (_p > _pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
              break;
            }
            for (_i= 0; _i < _rl; _i++)
            {
              _c_in      = (IceTFloat *) _src;
              _src      += 4*sizeof(IceTFloat);
              {IceTFloat afactor= 1.0f-(_c_in)[3];
               (_color)[0]= (_background_color)[0]*afactor+(_c_in)[0];
               (_color)[1]= (_background_color)[1]*afactor+(_c_in)[1];
               (_color)[2]= (_background_color)[2]*afactor+(_c_in)[2];
               (_color)[3]= (_background_color)[3]*afactor+(_c_in)[3];
              }   _color+= 4;
            }
          }
        }
      } else if (_color_format == (IceTEnum) 0xC000)
      {
        icetRaiseDiagnostic("Decompressing image with no data.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0003,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,349);
      } else {
        icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,352);
      }
    } else {
      icetRaiseDiagnostic("Encountered invalid composite mode.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                          ,356);
    }
    icetTimingCompressEnd();
  }
}

void icetComposite(IceTImage destBuffer,const IceTImage srcBuffer,
                   int srcOnTop)
{
  IceTSizeType pixels;
  IceTSizeType i;
  IceTEnum     composite_mode;
  IceTEnum     color_format,depth_format;
  pixels= icetImageGetNumPixels(destBuffer);
  if (pixels != icetImageGetNumPixels(srcBuffer))
  {
    icetRaiseDiagnostic("Source and destination sizes don't match.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,2318);
    return;
  }
  color_format= icetImageGetColorFormat(destBuffer);
  depth_format= icetImageGetDepthFormat(destBuffer);
  if ((color_format != icetImageGetColorFormat(srcBuffer))
      ||(depth_format != icetImageGetDepthFormat(srcBuffer)))
  {
    icetRaiseDiagnostic("Source and destination types don't match.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,2328);
    return;
  }
  icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0028),&composite_mode);
  icetTimingBlendBegin();
  if (composite_mode == (IceTEnum) 0x0301)
  {
    if (depth_format == (IceTEnum) 0xD001)
    {
      const IceTFloat *srcDepthBuffer = icetImageGetDepthf(srcBuffer);
      IceTFloat       *destDepthBuffer= icetImageGetDepthf(destBuffer);
      if (color_format == (IceTEnum) 0xC001)
      {
        const IceTUInt *srcColorBuffer = icetImageGetColorui(srcBuffer);
        IceTUInt       *destColorBuffer= icetImageGetColorui(destBuffer);
        for (i= 0; i < pixels; i++)
        {
          if (srcDepthBuffer[i] < destDepthBuffer[i])
          {
            destDepthBuffer[i]= srcDepthBuffer[i];
            destColorBuffer[i]= srcColorBuffer[i];
          }
        }
      } else if (color_format == (IceTEnum) 0xC002)
      {
        const IceTFloat *srcColorBuffer = icetImageGetColorf(srcBuffer);
        IceTFloat       *destColorBuffer= icetImageGetColorf(destBuffer);
        for (i= 0; i < pixels; i++)
        {
          if (srcDepthBuffer[i] < destDepthBuffer[i])
          {
            destDepthBuffer[i]    = srcDepthBuffer[i];
            destColorBuffer[4*i+0]= srcColorBuffer[4*i+0];
            destColorBuffer[4*i+1]= srcColorBuffer[4*i+1];
            destColorBuffer[4*i+2]= srcColorBuffer[4*i+2];
            destColorBuffer[4*i+3]= srcColorBuffer[4*i+3];
          }
        }
      } else if (color_format == (IceTEnum) 0xC000)
      {
        for (i= 0; i < pixels; i++)
        {
          if (srcDepthBuffer[i] < destDepthBuffer[i])
          {
            destDepthBuffer[i]= srcDepthBuffer[i];
          }
        }
      } else {
        icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                            ,2370);
      }
    } else if (depth_format == (IceTEnum) 0xD000)
    {
      icetRaiseDiagnostic("Cannot use Z buffer compositing operation with no" " Z buffer.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,2374);
    } else {
      icetRaiseDiagnostic("Encountered invalid depth format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,2377);
    }
  } else if (composite_mode == (IceTEnum) 0x0302)
  {
    if (depth_format != (IceTEnum) 0xD000)
    {
      icetRaiseDiagnostic("Z buffer ignored during blend composite" " operation.  Output z buffer meaningless.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0003,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,2383);
    }
    if (color_format == (IceTEnum) 0xC001)
    {
      const IceTUByte *srcColorBuffer = icetImageGetColorcub(srcBuffer);
      IceTUByte       *destColorBuffer= icetImageGetColorub(destBuffer);
      if (srcOnTop)
      {
        for (i= 0; i < pixels; i++)
        {
          {IceTUInt afactor= 255-(srcColorBuffer+i*4)[3];
           (destColorBuffer+i*4)[0]= (IceTUByte) (((destColorBuffer+i*4)[0]*afactor)/255+(srcColorBuffer+i*4)[0]);
           (destColorBuffer+i*4)[1]= (IceTUByte) (((destColorBuffer+i*4)[1]*afactor)/255+(srcColorBuffer+i*4)[1]);
           (destColorBuffer+i*4)[2]= (IceTUByte) (((destColorBuffer+i*4)[2]*afactor)/255+(srcColorBuffer+i*4)[2]);
           (destColorBuffer+i*4)[3]= (IceTUByte) (((destColorBuffer+i*4)[3]*afactor)/255+(srcColorBuffer+i*4)[3]);
          }
        }
      } else {
        for (i= 0; i < pixels; i++)
        {
          {IceTUInt afactor= 255-(destColorBuffer+i*4)[3];
           (destColorBuffer+i*4)[0]= (IceTUByte) (((srcColorBuffer+i*4)[0]*afactor)/255+(destColorBuffer+i*4)[0]);
           (destColorBuffer+i*4)[1]= (IceTUByte) (((srcColorBuffer+i*4)[1]*afactor)/255+(destColorBuffer+i*4)[1]);
           (destColorBuffer+i*4)[2]= (IceTUByte) (((srcColorBuffer+i*4)[2]*afactor)/255+(destColorBuffer+i*4)[2]);
           (destColorBuffer+i*4)[3]= (IceTUByte) (((srcColorBuffer+i*4)[3]*afactor)/255+(destColorBuffer+i*4)[3]);
          }
        }
      }
    } else if (color_format == (IceTEnum) 0xC002)
    {
      const IceTFloat *srcColorBuffer = icetImageGetColorcf(srcBuffer);
      IceTFloat       *destColorBuffer= icetImageGetColorf(destBuffer);
      if (srcOnTop)
      {
        for (i= 0; i < pixels; i++)
        {
          {IceTFloat afactor= 1.0f-(srcColorBuffer+i*4)[3];
           (destColorBuffer+i*4)[0]= (destColorBuffer+i*4)[0]*afactor+(srcColorBuffer+i*4)[0];
           (destColorBuffer+i*4)[1]= (destColorBuffer+i*4)[1]*afactor+(srcColorBuffer+i*4)[1];
           (destColorBuffer+i*4)[2]= (destColorBuffer+i*4)[2]*afactor+(srcColorBuffer+i*4)[2];
           (destColorBuffer+i*4)[3]= (destColorBuffer+i*4)[3]*afactor+(srcColorBuffer+i*4)[3];
          }
        }
      } else {
        for (i= 0; i < pixels; i++)
        {
          {IceTFloat afactor= 1.0f-(destColorBuffer+i*4)[3];
           (destColorBuffer+i*4)[0]= (srcColorBuffer+i*4)[0]*afactor+(destColorBuffer+i*4)[0];
           (destColorBuffer+i*4)[1]= (srcColorBuffer+i*4)[1]*afactor+(destColorBuffer+i*4)[1];
           (destColorBuffer+i*4)[2]= (srcColorBuffer+i*4)[2]*afactor+(destColorBuffer+i*4)[2];
           (destColorBuffer+i*4)[3]= (srcColorBuffer+i*4)[3]*afactor+(destColorBuffer+i*4)[3];
          }
        }
      }
    } else if (color_format == (IceTEnum) 0xC000)
    {
      icetRaiseDiagnostic("Compositing image with no data.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0003,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,2415);
    } else {
      icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                          ,2418);
    }
  } else {
    icetRaiseDiagnostic("Encountered invalid composite mode.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,2422);
  }
  icetTimingBlendEnd();
}

void icetCompressedComposite(IceTImage destBuffer,
                             const IceTSparseImage srcBuffer,
                             int srcOnTop)
{
  if (icetImageGetNumPixels(destBuffer)
      != icetSparseImageGetNumPixels(srcBuffer))
  {
    icetRaiseDiagnostic("Size of input and output buffers do not agree.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,2435);
  }
  icetCompressedSubComposite(destBuffer,0,srcBuffer,srcOnTop);
}

void icetCompressedSubComposite(IceTImage destBuffer,
                                IceTSizeType offset,
                                const IceTSparseImage srcBuffer,
                                int srcOnTop)
{
  icetTimingBlendBegin();
  if (srcOnTop)
  {
    {
      IceTEnum     _color_format,_depth_format;
      IceTSizeType _pixel_count;
      IceTEnum     _composite_mode;
      icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0028),&_composite_mode);
      _color_format= icetSparseImageGetColorFormat(srcBuffer);
      _depth_format= icetSparseImageGetDepthFormat(srcBuffer);
      _pixel_count = icetSparseImageGetNumPixels(srcBuffer);
      if (_color_format != icetImageGetColorFormat(destBuffer))
      {
        icetRaiseDiagnostic("Input/output buffers have different color formats.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,80);
      }
      if (_depth_format != icetImageGetDepthFormat(destBuffer))
      {
        icetRaiseDiagnostic("Input/output buffers have different depth formats.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,84);
      }
      if (_pixel_count != icetSparseImageGetNumPixels(srcBuffer))
      {
        icetRaiseDiagnostic("Unexpected input pixel count.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,89);
      }
      if (_pixel_count > icetImageGetNumPixels(destBuffer)-offset)
      {
        icetRaiseDiagnostic("Offset pixels outside range of output image.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,100);
      }
      if (_composite_mode == (IceTEnum) 0x0301)
      {
        if (_depth_format == (IceTEnum) 0xD001)
        {
          IceTFloat *_depth= icetImageGetDepthf(destBuffer);
          _depth+= offset;
          if (_color_format == (IceTEnum) 0xC001)
          {
            IceTUInt        *_color;
            const IceTUInt  *_c_in;
            const IceTFloat *_d_in;
            IceTUInt        _background_color;
            _color = icetImageGetColorui(destBuffer);
            _color+= offset;
            icetGetIntegerv(((IceTEnum) 0x00000000|(IceTEnum) 0x0006),
                            (IceTInt *) &_background_color);
            {
              const IceTByte *_src;
              IceTSizeType   _pixels;
              IceTSizeType   _p;
              IceTSizeType   _i;
              _pixels= icetSparseImageGetNumPixels(srcBuffer);
              _src   = ((IceTVoid *) &(((IceTInt *) srcBuffer.opaque_internals)[7]));
              _p     = 0;
              while (_p < _pixels)
              {
                const IceTVoid *_runlengths;
                IceTSizeType   _rl;
                _runlengths= _src;
                _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
                _p        += _rl;
                if (_p > _pixels)
                {
                  icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                  break;
                }
                _color+= _rl;
                _depth+= _rl;
                _rl    = (((IceTRunLengthType *) (_runlengths))[1]);
                _p    += _rl;
                if (_p > _pixels)
                {
                  icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                  break;
                }
                for (_i= 0; _i < _rl; _i++)
                {
                  _c_in= (IceTUInt *) _src;
                  _src+= sizeof(IceTUInt);
                  _d_in= (IceTFloat *) _src;
                  _src+= sizeof(IceTFloat);
                  if (_d_in[0] < _depth[0]){_color[0]= _c_in[0];_depth[0]= _d_in[0];}  _color++;
                  _depth++;
                }
              }
            }
          } else if (_color_format == (IceTEnum) 0xC002)
          {
            IceTFloat       *_color;
            const IceTFloat *_c_in;
            const IceTFloat *_d_in;
            IceTFloat       _background_color[4];
            _color = icetImageGetColorf(destBuffer);
            _color+= 4*(offset);
            icetGetFloatv(((IceTEnum) 0x00000000|(IceTEnum) 0x0005),_background_color);
            {
              const IceTByte *_src;
              IceTSizeType   _pixels;
              IceTSizeType   _p;
              IceTSizeType   _i;
              _pixels= icetSparseImageGetNumPixels(srcBuffer);
              _src   = ((IceTVoid *) &(((IceTInt *) srcBuffer.opaque_internals)[7]));
              _p     = 0;
              while (_p < _pixels)
              {
                const IceTVoid *_runlengths;
                IceTSizeType   _rl;
                _runlengths= _src;
                _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
                _p        += _rl;
                if (_p > _pixels)
                {
                  icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                  break;
                }
                _color+= 4*_rl;
                _depth+= _rl;
                _rl    = (((IceTRunLengthType *) (_runlengths))[1]);
                _p    += _rl;
                if (_p > _pixels)
                {
                  icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                  break;
                }
                for (_i= 0; _i < _rl; _i++)
                {
                  _c_in                                                                                                                                   = (IceTFloat *) _src;
                  _src                                                                                                                                   += 4*sizeof(IceTFloat);
                  _d_in                                                                                                                                   = (IceTFloat *) _src;
                  _src                                                                                                                                   += sizeof(IceTFloat);
                  if (_d_in[0] < _depth[0]){_color[0]= _c_in[0];_color[1]= _c_in[1];_color[2]= _c_in[2];_color[3]= _c_in[3];_depth[0]= _d_in[0];}  _color+= 4;
                  _depth++;
                }
              }
            }
          } else if (_color_format == (IceTEnum) 0xC000)
          {
            const IceTFloat *_d_in;
            {
              const IceTByte *_src;
              IceTSizeType   _pixels;
              IceTSizeType   _p;
              IceTSizeType   _i;
              _pixels= icetSparseImageGetNumPixels(srcBuffer);
              _src   = ((IceTVoid *) &(((IceTInt *) srcBuffer.opaque_internals)[7]));
              _p     = 0;
              while (_p < _pixels)
              {
                const IceTVoid *_runlengths;
                IceTSizeType   _rl;
                _runlengths= _src;
                _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
                _p        += _rl;
                if (_p > _pixels)
                {
                  icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                  break;
                }
                _depth+= _rl;
                _rl    = (((IceTRunLengthType *) (_runlengths))[1]);
                _p    += _rl;
                if (_p > _pixels)
                {
                  icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                  break;
                }
                for (_i= 0; _i < _rl; _i++)
                {
                  _d_in= (IceTFloat *) _src;
                  _src+= sizeof(IceTFloat);
                  if (_d_in[0] < _depth[0]){_depth[0]= _d_in[0];}  _depth++;
                }
              }
            }
          } else {
            icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                                "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                                ,239);
          }
        } else if (_depth_format == (IceTEnum) 0xD000)
        {
          icetRaiseDiagnostic("Cannot use Z buffer compositing operation with no" " Z buffer.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                              "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                              ,243);
        } else {
          icetRaiseDiagnostic("Encountered invalid depth format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                              "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                              ,246);
        }
      } else if (_composite_mode == (IceTEnum) 0x0302)
      {
        if (_depth_format != (IceTEnum) 0xD000)
        {
          icetRaiseDiagnostic("Z buffer ignored during blend composite" " operation.  Output z buffer meaningless.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0003,
                              "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                              ,253);
        }
        if (_color_format == (IceTEnum) 0xC001)
        {
          IceTUInt       *_color;
          const IceTUInt *_c_in;
          IceTUInt       _background_color;
          _color = icetImageGetColorui(destBuffer);
          _color+= offset;
          icetGetIntegerv(((IceTEnum) 0x00000080|(IceTEnum) 0x000E),
                          (IceTInt *) &_background_color);
          {
            const IceTByte *_src;
            IceTSizeType   _pixels;
            IceTSizeType   _p;
            IceTSizeType   _i;
            _pixels= icetSparseImageGetNumPixels(srcBuffer);
            _src   = ((IceTVoid *) &(((IceTInt *) srcBuffer.opaque_internals)[7]));
            _p     = 0;
            while (_p < _pixels)
            {
              const IceTVoid *_runlengths;
              IceTSizeType   _rl;
              _runlengths= _src;
              _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
              _p        += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                break;
              }
              _color+= _rl;
              _rl    = (((IceTRunLengthType *) (_runlengths))[1]);
              _p    += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                break;
              }
              for (_i= 0; _i < _rl; _i++)
              {
                _c_in= (IceTUInt *) _src;
                _src+= sizeof(IceTUInt);
                {IceTUInt afactor= 255-(((IceTUByte *) _c_in))[3];
                 (((IceTUByte *) _color))[0]= (IceTUByte) (((((IceTUByte *) _color))[0]*afactor)/255+(((IceTUByte *) _c_in))[0]);
                 (((IceTUByte *) _color))[1]= (IceTUByte) (((((IceTUByte *) _color))[1]*afactor)/255+(((IceTUByte *) _c_in))[1]);
                 (((IceTUByte *) _color))[2]= (IceTUByte) (((((IceTUByte *) _color))[2]*afactor)/255+(((IceTUByte *) _c_in))[2]);
                 (((IceTUByte *) _color))[3]= (IceTUByte) (((((IceTUByte *) _color))[3]*afactor)/255+(((IceTUByte *) _c_in))[3]);
                }  _color++;
              }
            }
          }
        } else if (_color_format == (IceTEnum) 0xC002)
        {
          IceTFloat       *_color;
          const IceTFloat *_c_in;
          IceTFloat       _background_color[4];
          _color = icetImageGetColorf(destBuffer);
          _color+= 4*(offset);
          icetGetFloatv(((IceTEnum) 0x00000080|(IceTEnum) 0x000D),_background_color);
          {
            const IceTByte *_src;
            IceTSizeType   _pixels;
            IceTSizeType   _p;
            IceTSizeType   _i;
            _pixels= icetSparseImageGetNumPixels(srcBuffer);
            _src   = ((IceTVoid *) &(((IceTInt *) srcBuffer.opaque_internals)[7]));
            _p     = 0;
            while (_p < _pixels)
            {
              const IceTVoid *_runlengths;
              IceTSizeType   _rl;
              _runlengths= _src;
              _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
              _p        += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                break;
              }
              _color+= 4*_rl;
              _rl    = (((IceTRunLengthType *) (_runlengths))[1]);
              _p    += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                break;
              }
              for (_i= 0; _i < _rl; _i++)
              {
                _c_in      = (IceTFloat *) _src;
                _src      += 4*sizeof(IceTFloat);
                {IceTFloat afactor= 1.0f-(_c_in)[3];
                 (_color)[0]= (_color)[0]*afactor+(_c_in)[0];
                 (_color)[1]= (_color)[1]*afactor+(_c_in)[1];
                 (_color)[2]= (_color)[2]*afactor+(_c_in)[2];
                 (_color)[3]= (_color)[3]*afactor+(_c_in)[3];
                }   _color+= 4;
              }
            }
          }
        } else if (_color_format == (IceTEnum) 0xC000)
        {
          icetRaiseDiagnostic("Decompressing image with no data.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0003,
                              "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                              ,349);
        } else {
          icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                              "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                              ,352);
        }
      } else {
        icetRaiseDiagnostic("Encountered invalid composite mode.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,356);
      }
    }
  } else {
    {
      IceTEnum     _color_format,_depth_format;
      IceTSizeType _pixel_count;
      IceTEnum     _composite_mode;
      icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0028),&_composite_mode);
      _color_format= icetSparseImageGetColorFormat(srcBuffer);
      _depth_format= icetSparseImageGetDepthFormat(srcBuffer);
      _pixel_count = icetSparseImageGetNumPixels(srcBuffer);
      if (_color_format != icetImageGetColorFormat(destBuffer))
      {
        icetRaiseDiagnostic("Input/output buffers have different color formats.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,80);
      }
      if (_depth_format != icetImageGetDepthFormat(destBuffer))
      {
        icetRaiseDiagnostic("Input/output buffers have different depth formats.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,84);
      }
      if (_pixel_count != icetSparseImageGetNumPixels(srcBuffer))
      {
        icetRaiseDiagnostic("Unexpected input pixel count.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,89);
      }
      if (_pixel_count > icetImageGetNumPixels(destBuffer)-offset)
      {
        icetRaiseDiagnostic("Offset pixels outside range of output image.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,100);
      }
      if (_composite_mode == (IceTEnum) 0x0301)
      {
        if (_depth_format == (IceTEnum) 0xD001)
        {
          IceTFloat *_depth= icetImageGetDepthf(destBuffer);
          _depth+= offset;
          if (_color_format == (IceTEnum) 0xC001)
          {
            IceTUInt        *_color;
            const IceTUInt  *_c_in;
            const IceTFloat *_d_in;
            IceTUInt        _background_color;
            _color = icetImageGetColorui(destBuffer);
            _color+= offset;
            icetGetIntegerv(((IceTEnum) 0x00000000|(IceTEnum) 0x0006),
                            (IceTInt *) &_background_color);
            {
              const IceTByte *_src;
              IceTSizeType   _pixels;
              IceTSizeType   _p;
              IceTSizeType   _i;
              _pixels= icetSparseImageGetNumPixels(srcBuffer);
              _src   = ((IceTVoid *) &(((IceTInt *) srcBuffer.opaque_internals)[7]));
              _p     = 0;
              while (_p < _pixels)
              {
                const IceTVoid *_runlengths;
                IceTSizeType   _rl;
                _runlengths= _src;
                _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
                _p        += _rl;
                if (_p > _pixels)
                {
                  icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                  break;
                }
                _color+= _rl;
                _depth+= _rl;
                _rl    = (((IceTRunLengthType *) (_runlengths))[1]);
                _p    += _rl;
                if (_p > _pixels)
                {
                  icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                  break;
                }
                for (_i= 0; _i < _rl; _i++)
                {
                  _c_in= (IceTUInt *) _src;
                  _src+= sizeof(IceTUInt);
                  _d_in= (IceTFloat *) _src;
                  _src+= sizeof(IceTFloat);
                  if (_d_in[0] < _depth[0]){_color[0]= _c_in[0];_depth[0]= _d_in[0];}  _color++;
                  _depth++;
                }
              }
            }
          } else if (_color_format == (IceTEnum) 0xC002)
          {
            IceTFloat       *_color;
            const IceTFloat *_c_in;
            const IceTFloat *_d_in;
            IceTFloat       _background_color[4];
            _color = icetImageGetColorf(destBuffer);
            _color+= 4*(offset);
            icetGetFloatv(((IceTEnum) 0x00000000|(IceTEnum) 0x0005),_background_color);
            {
              const IceTByte *_src;
              IceTSizeType   _pixels;
              IceTSizeType   _p;
              IceTSizeType   _i;
              _pixels= icetSparseImageGetNumPixels(srcBuffer);
              _src   = ((IceTVoid *) &(((IceTInt *) srcBuffer.opaque_internals)[7]));
              _p     = 0;
              while (_p < _pixels)
              {
                const IceTVoid *_runlengths;
                IceTSizeType   _rl;
                _runlengths= _src;
                _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
                _p        += _rl;
                if (_p > _pixels)
                {
                  icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                  break;
                }
                _color+= 4*_rl;
                _depth+= _rl;
                _rl    = (((IceTRunLengthType *) (_runlengths))[1]);
                _p    += _rl;
                if (_p > _pixels)
                {
                  icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                  break;
                }
                for (_i= 0; _i < _rl; _i++)
                {
                  _c_in                                                                                                                                   = (IceTFloat *) _src;
                  _src                                                                                                                                   += 4*sizeof(IceTFloat);
                  _d_in                                                                                                                                   = (IceTFloat *) _src;
                  _src                                                                                                                                   += sizeof(IceTFloat);
                  if (_d_in[0] < _depth[0]){_color[0]= _c_in[0];_color[1]= _c_in[1];_color[2]= _c_in[2];_color[3]= _c_in[3];_depth[0]= _d_in[0];}  _color+= 4;
                  _depth++;
                }
              }
            }
          } else if (_color_format == (IceTEnum) 0xC000)
          {
            const IceTFloat *_d_in;
            {
              const IceTByte *_src;
              IceTSizeType   _pixels;
              IceTSizeType   _p;
              IceTSizeType   _i;
              _pixels= icetSparseImageGetNumPixels(srcBuffer);
              _src   = ((IceTVoid *) &(((IceTInt *) srcBuffer.opaque_internals)[7]));
              _p     = 0;
              while (_p < _pixels)
              {
                const IceTVoid *_runlengths;
                IceTSizeType   _rl;
                _runlengths= _src;
                _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
                _p        += _rl;
                if (_p > _pixels)
                {
                  icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                  break;
                }
                _depth+= _rl;
                _rl    = (((IceTRunLengthType *) (_runlengths))[1]);
                _p    += _rl;
                if (_p > _pixels)
                {
                  icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                  break;
                }
                for (_i= 0; _i < _rl; _i++)
                {
                  _d_in= (IceTFloat *) _src;
                  _src+= sizeof(IceTFloat);
                  if (_d_in[0] < _depth[0]){_depth[0]= _d_in[0];}  _depth++;
                }
              }
            }
          } else {
            icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                                "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                                ,239);
          }
        } else if (_depth_format == (IceTEnum) 0xD000)
        {
          icetRaiseDiagnostic("Cannot use Z buffer compositing operation with no" " Z buffer.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                              "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                              ,243);
        } else {
          icetRaiseDiagnostic("Encountered invalid depth format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                              "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                              ,246);
        }
      } else if (_composite_mode == (IceTEnum) 0x0302)
      {
        if (_depth_format != (IceTEnum) 0xD000)
        {
          icetRaiseDiagnostic("Z buffer ignored during blend composite" " operation.  Output z buffer meaningless.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0003,
                              "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                              ,253);
        }
        if (_color_format == (IceTEnum) 0xC001)
        {
          IceTUInt       *_color;
          const IceTUInt *_c_in;
          IceTUInt       _background_color;
          _color = icetImageGetColorui(destBuffer);
          _color+= offset;
          icetGetIntegerv(((IceTEnum) 0x00000080|(IceTEnum) 0x000E),
                          (IceTInt *) &_background_color);
          {
            const IceTByte *_src;
            IceTSizeType   _pixels;
            IceTSizeType   _p;
            IceTSizeType   _i;
            _pixels= icetSparseImageGetNumPixels(srcBuffer);
            _src   = ((IceTVoid *) &(((IceTInt *) srcBuffer.opaque_internals)[7]));
            _p     = 0;
            while (_p < _pixels)
            {
              const IceTVoid *_runlengths;
              IceTSizeType   _rl;
              _runlengths= _src;
              _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
              _p        += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                break;
              }
              _color+= _rl;
              _rl    = (((IceTRunLengthType *) (_runlengths))[1]);
              _p    += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                break;
              }
              for (_i= 0; _i < _rl; _i++)
              {
                _c_in= (IceTUInt *) _src;
                _src+= sizeof(IceTUInt);
                {IceTUInt afactor= 255-(((IceTUByte *) _color))[3];
                 (((IceTUByte *) _color))[0]= (IceTUByte) (((((IceTUByte *) _c_in))[0]*afactor)/255+(((IceTUByte *) _color))[0]);
                 (((IceTUByte *) _color))[1]= (IceTUByte) (((((IceTUByte *) _c_in))[1]*afactor)/255+(((IceTUByte *) _color))[1]);
                 (((IceTUByte *) _color))[2]= (IceTUByte) (((((IceTUByte *) _c_in))[2]*afactor)/255+(((IceTUByte *) _color))[2]);
                 (((IceTUByte *) _color))[3]= (IceTUByte) (((((IceTUByte *) _c_in))[3]*afactor)/255+(((IceTUByte *) _color))[3]);
                }  _color++;
              }
            }
          }
        } else if (_color_format == (IceTEnum) 0xC002)
        {
          IceTFloat       *_color;
          const IceTFloat *_c_in;
          IceTFloat       _background_color[4];
          _color = icetImageGetColorf(destBuffer);
          _color+= 4*(offset);
          icetGetFloatv(((IceTEnum) 0x00000080|(IceTEnum) 0x000D),_background_color);
          {
            const IceTByte *_src;
            IceTSizeType   _pixels;
            IceTSizeType   _p;
            IceTSizeType   _i;
            _pixels= icetSparseImageGetNumPixels(srcBuffer);
            _src   = ((IceTVoid *) &(((IceTInt *) srcBuffer.opaque_internals)[7]));
            _p     = 0;
            while (_p < _pixels)
            {
              const IceTVoid *_runlengths;
              IceTSizeType   _rl;
              _runlengths= _src;
              _src      += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              _rl        = (((IceTRunLengthType *) (_runlengths))[0]);
              _p        += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",58);
                break;
              }
              _color+= 4*_rl;
              _rl    = (((IceTRunLengthType *) (_runlengths))[1]);
              _p    += _rl;
              if (_p > _pixels)
              {
                icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/decompress_template_body.h",67);
                break;
              }
              for (_i= 0; _i < _rl; _i++)
              {
                _c_in      = (IceTFloat *) _src;
                _src      += 4*sizeof(IceTFloat);
                {IceTFloat afactor= 1.0f-(_color)[3];
                 (_color)[0]= (_c_in)[0]*afactor+(_color)[0];
                 (_color)[1]= (_c_in)[1]*afactor+(_color)[1];
                 (_color)[2]= (_c_in)[2]*afactor+(_color)[2];
                 (_color)[3]= (_c_in)[3]*afactor+(_color)[3];
                }   _color+= 4;
              }
            }
          }
        } else if (_color_format == (IceTEnum) 0xC000)
        {
          icetRaiseDiagnostic("Decompressing image with no data.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0003,
                              "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                              ,349);
        } else {
          icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                              "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                              ,352);
        }
      } else {
        icetRaiseDiagnostic("Encountered invalid composite mode.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/decompress_func_body.h"
                            ,356);
      }
    }
  }
  icetTimingBlendEnd();
}

void icetCompressedCompressedComposite(const IceTSparseImage front_buffer,
                                       const IceTSparseImage back_buffer,
                                       IceTSparseImage dest_buffer)
{
  if (icetSparseImageEqual(front_buffer,back_buffer)
      ||icetSparseImageEqual(front_buffer,dest_buffer)
      ||icetSparseImageEqual(back_buffer,dest_buffer))
  {
    icetRaiseDiagnostic("Detected reused buffer in" " compressed-compressed composite.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,2478);
  }
  icetTimingBlendBegin();
  {
    IceTEnum _color_format;
    IceTEnum _depth_format;
    IceTEnum _composite_mode;
    icetGetEnumv(((IceTEnum) 0x00000000|(IceTEnum) 0x0028),&_composite_mode);
    _color_format= icetSparseImageGetColorFormat(front_buffer);
    _depth_format= icetSparseImageGetDepthFormat(front_buffer);
    if ((_color_format != icetSparseImageGetColorFormat(back_buffer))
        ||(_color_format != icetSparseImageGetColorFormat(dest_buffer))
        ||(_depth_format != icetSparseImageGetDepthFormat(back_buffer))
        ||(_depth_format != icetSparseImageGetDepthFormat(dest_buffer))
        )
    {
      icetRaiseDiagnostic("Input buffers do not agree for compressed-compressed" " composite.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_func_body.h"
                          ,62);
    }
    if (_composite_mode == (IceTEnum) 0x0301)
    {
      if (_depth_format == (IceTEnum) 0xD001)
      {
        if (_color_format == (IceTEnum) 0xC001)
        {
          {
            const IceTByte *_front;
            const IceTByte *_back;
            IceTByte       *_dest;
            IceTVoid       *_dest_runlengths;
            IceTSizeType   _num_pixels;
            IceTSizeType   _pixel;
            IceTSizeType   _front_num_inactive;
            IceTSizeType   _front_num_active;
            IceTSizeType   _back_num_inactive;
            IceTSizeType   _back_num_active;
            IceTSizeType   _dest_num_active;
            _num_pixels= icetSparseImageGetNumPixels(front_buffer);
            if (_num_pixels != icetSparseImageGetNumPixels(back_buffer))
            {
              icetRaiseDiagnostic("Input buffers do not agree for compressed-compressed" " composite.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                                  "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_template_body.h"
                                  ,60);
            }
            icetSparseImageSetDimensions(
              dest_buffer,
              icetSparseImageGetWidth(front_buffer),
              icetSparseImageGetHeight(back_buffer));
            _front             = ((IceTVoid *) &(((IceTInt *) front_buffer.opaque_internals)[7]));
            _back              = ((IceTVoid *) &(((IceTInt *) back_buffer.opaque_internals)[7]));
            _dest              = ((IceTVoid *) &(((IceTInt *) dest_buffer.opaque_internals)[7]));
            _dest_runlengths   = ((void *) 0);
            _pixel             = 0;
            _front_num_inactive= _front_num_active= 0;
            _back_num_inactive = _back_num_active= 0;
            _dest_num_active   = 0;
            while (_pixel < _num_pixels)
            {
              while ((_front_num_active == 0)
                     &&((_front_num_inactive+_pixel) < _num_pixels))
              {
                _front_num_inactive+= (((IceTRunLengthType *) (_front))[0]);
                _front_num_active   = (((IceTRunLengthType *) (_front))[1]);
                _front             += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              }
              while ((_back_num_active == 0)
                     &&((_back_num_inactive+_pixel) < _num_pixels))
              {
                _back_num_inactive+= (((IceTRunLengthType *) (_back))[0]);
                _back_num_active   = (((IceTRunLengthType *) (_back))[1]);
                _back             += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              }
              {
                IceTSizeType _dest_num_inactive
                  = ((_front_num_inactive) < (_back_num_inactive) ? (_front_num_inactive) : (_back_num_inactive));
                if (_dest_num_inactive > 0)
                {
                  if (_dest_runlengths != ((void *) 0))
                  {
                    (((IceTRunLengthType *) (_dest_runlengths))[1])= _dest_num_active;
                    _dest_num_active                               = 0;
                  }
                  _dest_runlengths                               = _dest;
                  _dest                                         += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                  _pixel                                        += _dest_num_inactive;
                  _front_num_inactive                           -= _dest_num_inactive;
                  _back_num_inactive                            -= _dest_num_inactive;
                  (((IceTRunLengthType *) (_dest_runlengths))[0])= _dest_num_inactive;
                } else {
                  if (_dest_runlengths == ((void *) 0))
                  {
                    _dest_runlengths                               = _dest;
                    _dest                                         += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                    (((IceTRunLengthType *) (_dest_runlengths))[0])= 0;
                  }
                }
              }
              if ((0 < _front_num_inactive)&&(0 < _back_num_active))
              {
                IceTSizeType _num_to_copy
                                    = ((_front_num_inactive) < (_back_num_active) ? (_front_num_inactive) : (_back_num_active));
                _front_num_inactive-= _num_to_copy;
                _back_num_active   -= _num_to_copy;
                _dest_num_active   += _num_to_copy;
                _pixel             += _num_to_copy;
                memcpy(_dest,_back,(sizeof(IceTUInt)+sizeof(IceTFloat))*_num_to_copy);
                _dest+= (sizeof(IceTUInt)+sizeof(IceTFloat))*_num_to_copy;
                _back+= (sizeof(IceTUInt)+sizeof(IceTFloat))*_num_to_copy;
              }
              if ((0 < _back_num_inactive)&&(0 < _front_num_active))
              {
                IceTSizeType _num_to_copy
                                   = ((_back_num_inactive) < (_front_num_active) ? (_back_num_inactive) : (_front_num_active));
                _back_num_inactive-= _num_to_copy;
                _front_num_active -= _num_to_copy;
                _dest_num_active  += _num_to_copy;
                _pixel            += _num_to_copy;
                memcpy(_dest,_front,(sizeof(IceTUInt)+sizeof(IceTFloat))*_num_to_copy);
                _dest += (sizeof(IceTUInt)+sizeof(IceTFloat))*_num_to_copy;
                _front+= (sizeof(IceTUInt)+sizeof(IceTFloat))*_num_to_copy;
              }
              if ((_front_num_inactive == 0)&&(_back_num_inactive == 0))
              {
                IceTSizeType _num_to_composite
                                  = ((_front_num_active) < (_back_num_active) ? (_front_num_active) : (_back_num_active));
                _front_num_active-= _num_to_composite;
                _back_num_active -= _num_to_composite;
                _dest_num_active += _num_to_composite;
                _pixel           += _num_to_composite;
                for (; 0 < _num_to_composite; _num_to_composite--)
                {
                  {const IceTUInt  *src1_color;
                   const IceTFloat *src1_depth;
                   const IceTUInt  *src2_color;
                   const IceTFloat *src2_depth;
                   IceTUInt        *dest_color;
                   IceTFloat       *dest_depth;
                   src1_color= (IceTUInt *) _front;
                   _front   += sizeof(IceTUInt);
                   src1_depth= (IceTFloat *) _front;
                   _front   += sizeof(IceTFloat);
                   src2_color= (IceTUInt *) _back;
                   _back    += sizeof(IceTUInt);
                   src2_depth= (IceTFloat *) _back;
                   _back    += sizeof(IceTFloat);
                   dest_color= (IceTUInt *) _dest;
                   _dest    += sizeof(IceTUInt);
                   dest_depth= (IceTFloat *) _dest;
                   _dest    += sizeof(IceTFloat);
                   if (src1_depth[0] < src2_depth[0]){dest_color[0]= src1_color[0];dest_depth[0]= src1_depth[0];} else {dest_color[0]= src2_color[0];dest_depth[0]= src2_depth[0];}}
                }
              }
            }
            if (_dest_runlengths != ((void *) 0))
            {
              (((IceTRunLengthType *) (_dest_runlengths))[1])= _dest_num_active;
            }
            if (_pixel != _num_pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_template_body.h",164);
            }
            {
              IceTPointerArithmetic _buffer_begin
                = (IceTPointerArithmetic) ((IceTInt *) dest_buffer.opaque_internals);
              IceTPointerArithmetic _buffer_end
                = (IceTPointerArithmetic) _dest;
              IceTPointerArithmetic _compressed_size= _buffer_end-_buffer_begin;
              ((IceTInt *) dest_buffer.opaque_internals)
              [6]
                = (IceTInt) _compressed_size;
            }
          }
        } else if (_color_format == (IceTEnum) 0xC002)
        {
          {
            const IceTByte *_front;
            const IceTByte *_back;
            IceTByte       *_dest;
            IceTVoid       *_dest_runlengths;
            IceTSizeType   _num_pixels;
            IceTSizeType   _pixel;
            IceTSizeType   _front_num_inactive;
            IceTSizeType   _front_num_active;
            IceTSizeType   _back_num_inactive;
            IceTSizeType   _back_num_active;
            IceTSizeType   _dest_num_active;
            _num_pixels= icetSparseImageGetNumPixels(front_buffer);
            if (_num_pixels != icetSparseImageGetNumPixels(back_buffer))
            {
              icetRaiseDiagnostic("Input buffers do not agree for compressed-compressed" " composite.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                                  "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_template_body.h"
                                  ,60);
            }
            icetSparseImageSetDimensions(
              dest_buffer,
              icetSparseImageGetWidth(front_buffer),
              icetSparseImageGetHeight(back_buffer));
            _front             = ((IceTVoid *) &(((IceTInt *) front_buffer.opaque_internals)[7]));
            _back              = ((IceTVoid *) &(((IceTInt *) back_buffer.opaque_internals)[7]));
            _dest              = ((IceTVoid *) &(((IceTInt *) dest_buffer.opaque_internals)[7]));
            _dest_runlengths   = ((void *) 0);
            _pixel             = 0;
            _front_num_inactive= _front_num_active= 0;
            _back_num_inactive = _back_num_active= 0;
            _dest_num_active   = 0;
            while (_pixel < _num_pixels)
            {
              while ((_front_num_active == 0)
                     &&((_front_num_inactive+_pixel) < _num_pixels))
              {
                _front_num_inactive+= (((IceTRunLengthType *) (_front))[0]);
                _front_num_active   = (((IceTRunLengthType *) (_front))[1]);
                _front             += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              }
              while ((_back_num_active == 0)
                     &&((_back_num_inactive+_pixel) < _num_pixels))
              {
                _back_num_inactive+= (((IceTRunLengthType *) (_back))[0]);
                _back_num_active   = (((IceTRunLengthType *) (_back))[1]);
                _back             += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              }
              {
                IceTSizeType _dest_num_inactive
                  = ((_front_num_inactive) < (_back_num_inactive) ? (_front_num_inactive) : (_back_num_inactive));
                if (_dest_num_inactive > 0)
                {
                  if (_dest_runlengths != ((void *) 0))
                  {
                    (((IceTRunLengthType *) (_dest_runlengths))[1])= _dest_num_active;
                    _dest_num_active                               = 0;
                  }
                  _dest_runlengths                               = _dest;
                  _dest                                         += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                  _pixel                                        += _dest_num_inactive;
                  _front_num_inactive                           -= _dest_num_inactive;
                  _back_num_inactive                            -= _dest_num_inactive;
                  (((IceTRunLengthType *) (_dest_runlengths))[0])= _dest_num_inactive;
                } else {
                  if (_dest_runlengths == ((void *) 0))
                  {
                    _dest_runlengths                               = _dest;
                    _dest                                         += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                    (((IceTRunLengthType *) (_dest_runlengths))[0])= 0;
                  }
                }
              }
              if ((0 < _front_num_inactive)&&(0 < _back_num_active))
              {
                IceTSizeType _num_to_copy
                                    = ((_front_num_inactive) < (_back_num_active) ? (_front_num_inactive) : (_back_num_active));
                _front_num_inactive-= _num_to_copy;
                _back_num_active   -= _num_to_copy;
                _dest_num_active   += _num_to_copy;
                _pixel             += _num_to_copy;
                memcpy(_dest,_back,(5*sizeof(IceTFloat))*_num_to_copy);
                _dest+= (5*sizeof(IceTFloat))*_num_to_copy;
                _back+= (5*sizeof(IceTFloat))*_num_to_copy;
              }
              if ((0 < _back_num_inactive)&&(0 < _front_num_active))
              {
                IceTSizeType _num_to_copy
                                   = ((_back_num_inactive) < (_front_num_active) ? (_back_num_inactive) : (_front_num_active));
                _back_num_inactive-= _num_to_copy;
                _front_num_active -= _num_to_copy;
                _dest_num_active  += _num_to_copy;
                _pixel            += _num_to_copy;
                memcpy(_dest,_front,(5*sizeof(IceTFloat))*_num_to_copy);
                _dest += (5*sizeof(IceTFloat))*_num_to_copy;
                _front+= (5*sizeof(IceTFloat))*_num_to_copy;
              }
              if ((_front_num_inactive == 0)&&(_back_num_inactive == 0))
              {
                IceTSizeType _num_to_composite
                                  = ((_front_num_active) < (_back_num_active) ? (_front_num_active) : (_back_num_active));
                _front_num_active-= _num_to_composite;
                _back_num_active -= _num_to_composite;
                _dest_num_active += _num_to_composite;
                _pixel           += _num_to_composite;
                for (; 0 < _num_to_composite; _num_to_composite--)
                {
                  {const IceTFloat *src1_color;
                   const IceTFloat *src1_depth;
                   const IceTFloat *src2_color;
                   const IceTFloat *src2_depth;
                   IceTFloat       *dest_color;
                   IceTFloat       *dest_depth;
                   src1_color= (IceTFloat *) _front;
                   _front   += 4*sizeof(IceTUInt);
                   src1_depth= (IceTFloat *) _front;
                   _front   += sizeof(IceTFloat);
                   src2_color= (IceTFloat *) _back;
                   _back    += 4*sizeof(IceTUInt);
                   src2_depth= (IceTFloat *) _back;
                   _back    += sizeof(IceTFloat);
                   dest_color= (IceTFloat *) _dest;
                   _dest    += 4*sizeof(IceTUInt);
                   dest_depth= (IceTFloat *) _dest;
                   _dest    += sizeof(IceTFloat);
                   if (src1_depth[0] < src2_depth[0]){dest_color[0]= src1_color[0];dest_color[1]= src1_color[1];dest_color[2]= src1_color[2];dest_color[3]= src1_color[3];dest_depth[0]= src1_depth[0];} else {dest_color[0]= src2_color[0];dest_color[1]= src2_color[1];dest_color[2]= src2_color[2];dest_color[3]= src2_color[3];dest_depth[0]= src2_depth[0];}}
                }
              }
            }
            if (_dest_runlengths != ((void *) 0))
            {
              (((IceTRunLengthType *) (_dest_runlengths))[1])= _dest_num_active;
            }
            if (_pixel != _num_pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_template_body.h",164);
            }
            {
              IceTPointerArithmetic _buffer_begin
                = (IceTPointerArithmetic) ((IceTInt *) dest_buffer.opaque_internals);
              IceTPointerArithmetic _buffer_end
                = (IceTPointerArithmetic) _dest;
              IceTPointerArithmetic _compressed_size= _buffer_end-_buffer_begin;
              ((IceTInt *) dest_buffer.opaque_internals)
              [6]
                = (IceTInt) _compressed_size;
            }
          }
        } else if (_color_format == (IceTEnum) 0xC000)
        {
          {
            const IceTByte *_front;
            const IceTByte *_back;
            IceTByte       *_dest;
            IceTVoid       *_dest_runlengths;
            IceTSizeType   _num_pixels;
            IceTSizeType   _pixel;
            IceTSizeType   _front_num_inactive;
            IceTSizeType   _front_num_active;
            IceTSizeType   _back_num_inactive;
            IceTSizeType   _back_num_active;
            IceTSizeType   _dest_num_active;
            _num_pixels= icetSparseImageGetNumPixels(front_buffer);
            if (_num_pixels != icetSparseImageGetNumPixels(back_buffer))
            {
              icetRaiseDiagnostic("Input buffers do not agree for compressed-compressed" " composite.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                                  "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_template_body.h"
                                  ,60);
            }
            icetSparseImageSetDimensions(
              dest_buffer,
              icetSparseImageGetWidth(front_buffer),
              icetSparseImageGetHeight(back_buffer));
            _front             = ((IceTVoid *) &(((IceTInt *) front_buffer.opaque_internals)[7]));
            _back              = ((IceTVoid *) &(((IceTInt *) back_buffer.opaque_internals)[7]));
            _dest              = ((IceTVoid *) &(((IceTInt *) dest_buffer.opaque_internals)[7]));
            _dest_runlengths   = ((void *) 0);
            _pixel             = 0;
            _front_num_inactive= _front_num_active= 0;
            _back_num_inactive = _back_num_active= 0;
            _dest_num_active   = 0;
            while (_pixel < _num_pixels)
            {
              while ((_front_num_active == 0)
                     &&((_front_num_inactive+_pixel) < _num_pixels))
              {
                _front_num_inactive+= (((IceTRunLengthType *) (_front))[0]);
                _front_num_active   = (((IceTRunLengthType *) (_front))[1]);
                _front             += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              }
              while ((_back_num_active == 0)
                     &&((_back_num_inactive+_pixel) < _num_pixels))
              {
                _back_num_inactive+= (((IceTRunLengthType *) (_back))[0]);
                _back_num_active   = (((IceTRunLengthType *) (_back))[1]);
                _back             += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              }
              {
                IceTSizeType _dest_num_inactive
                  = ((_front_num_inactive) < (_back_num_inactive) ? (_front_num_inactive) : (_back_num_inactive));
                if (_dest_num_inactive > 0)
                {
                  if (_dest_runlengths != ((void *) 0))
                  {
                    (((IceTRunLengthType *) (_dest_runlengths))[1])= _dest_num_active;
                    _dest_num_active                               = 0;
                  }
                  _dest_runlengths                               = _dest;
                  _dest                                         += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                  _pixel                                        += _dest_num_inactive;
                  _front_num_inactive                           -= _dest_num_inactive;
                  _back_num_inactive                            -= _dest_num_inactive;
                  (((IceTRunLengthType *) (_dest_runlengths))[0])= _dest_num_inactive;
                } else {
                  if (_dest_runlengths == ((void *) 0))
                  {
                    _dest_runlengths                               = _dest;
                    _dest                                         += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                    (((IceTRunLengthType *) (_dest_runlengths))[0])= 0;
                  }
                }
              }
              if ((0 < _front_num_inactive)&&(0 < _back_num_active))
              {
                IceTSizeType _num_to_copy
                                    = ((_front_num_inactive) < (_back_num_active) ? (_front_num_inactive) : (_back_num_active));
                _front_num_inactive-= _num_to_copy;
                _back_num_active   -= _num_to_copy;
                _dest_num_active   += _num_to_copy;
                _pixel             += _num_to_copy;
                memcpy(_dest,_back,(sizeof(IceTFloat))*_num_to_copy);
                _dest+= (sizeof(IceTFloat))*_num_to_copy;
                _back+= (sizeof(IceTFloat))*_num_to_copy;
              }
              if ((0 < _back_num_inactive)&&(0 < _front_num_active))
              {
                IceTSizeType _num_to_copy
                                   = ((_back_num_inactive) < (_front_num_active) ? (_back_num_inactive) : (_front_num_active));
                _back_num_inactive-= _num_to_copy;
                _front_num_active -= _num_to_copy;
                _dest_num_active  += _num_to_copy;
                _pixel            += _num_to_copy;
                memcpy(_dest,_front,(sizeof(IceTFloat))*_num_to_copy);
                _dest += (sizeof(IceTFloat))*_num_to_copy;
                _front+= (sizeof(IceTFloat))*_num_to_copy;
              }
              if ((_front_num_inactive == 0)&&(_back_num_inactive == 0))
              {
                IceTSizeType _num_to_composite
                                  = ((_front_num_active) < (_back_num_active) ? (_front_num_active) : (_back_num_active));
                _front_num_active-= _num_to_composite;
                _back_num_active -= _num_to_composite;
                _dest_num_active += _num_to_composite;
                _pixel           += _num_to_composite;
                for (; 0 < _num_to_composite; _num_to_composite--)
                {
                  {const IceTFloat *src1_depth;
                   const IceTFloat *src2_depth;
                   IceTFloat       *dest_depth;
                   src1_depth= (IceTFloat *) _front;
                   _front   += sizeof(IceTFloat);
                   src2_depth= (IceTFloat *) _back;
                   _back    += sizeof(IceTFloat);
                   dest_depth= (IceTFloat *) _dest;
                   _dest    += sizeof(IceTFloat);
                   if (src1_depth[0] < src2_depth[0]){dest_depth[0]= src1_depth[0];} else {dest_depth[0]= src2_depth[0];}}
                }
              }
            }
            if (_dest_runlengths != ((void *) 0))
            {
              (((IceTRunLengthType *) (_dest_runlengths))[1])= _dest_num_active;
            }
            if (_pixel != _num_pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_template_body.h",164);
            }
            {
              IceTPointerArithmetic _buffer_begin
                = (IceTPointerArithmetic) ((IceTInt *) dest_buffer.opaque_internals);
              IceTPointerArithmetic _buffer_end
                = (IceTPointerArithmetic) _dest;
              IceTPointerArithmetic _compressed_size= _buffer_end-_buffer_begin;
              ((IceTInt *) dest_buffer.opaque_internals)
              [6]
                = (IceTInt) _compressed_size;
            }
          }
        } else {
          icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                              "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_func_body.h"
                              ,162);
        }
      } else if (_depth_format == (IceTEnum) 0xD000)
      {
        icetRaiseDiagnostic("Cannot use Z buffer compositing operation with no" " Z buffer.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_func_body.h"
                            ,166);
      } else {
        icetRaiseDiagnostic("Encountered invalid depth format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_func_body.h"
                            ,169);
      }
    } else if (_composite_mode == (IceTEnum) 0x0302)
    {
      if (_depth_format == (IceTEnum) 0xD000)
      {
        if (_color_format == (IceTEnum) 0xC001)
        {
          {
            const IceTByte *_front;
            const IceTByte *_back;
            IceTByte       *_dest;
            IceTVoid       *_dest_runlengths;
            IceTSizeType   _num_pixels;
            IceTSizeType   _pixel;
            IceTSizeType   _front_num_inactive;
            IceTSizeType   _front_num_active;
            IceTSizeType   _back_num_inactive;
            IceTSizeType   _back_num_active;
            IceTSizeType   _dest_num_active;
            _num_pixels= icetSparseImageGetNumPixels(front_buffer);
            if (_num_pixels != icetSparseImageGetNumPixels(back_buffer))
            {
              icetRaiseDiagnostic("Input buffers do not agree for compressed-compressed" " composite.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                                  "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_template_body.h"
                                  ,60);
            }
            icetSparseImageSetDimensions(
              dest_buffer,
              icetSparseImageGetWidth(front_buffer),
              icetSparseImageGetHeight(back_buffer));
            _front             = ((IceTVoid *) &(((IceTInt *) front_buffer.opaque_internals)[7]));
            _back              = ((IceTVoid *) &(((IceTInt *) back_buffer.opaque_internals)[7]));
            _dest              = ((IceTVoid *) &(((IceTInt *) dest_buffer.opaque_internals)[7]));
            _dest_runlengths   = ((void *) 0);
            _pixel             = 0;
            _front_num_inactive= _front_num_active= 0;
            _back_num_inactive = _back_num_active= 0;
            _dest_num_active   = 0;
            while (_pixel < _num_pixels)
            {
              while ((_front_num_active == 0)
                     &&((_front_num_inactive+_pixel) < _num_pixels))
              {
                _front_num_inactive+= (((IceTRunLengthType *) (_front))[0]);
                _front_num_active   = (((IceTRunLengthType *) (_front))[1]);
                _front             += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              }
              while ((_back_num_active == 0)
                     &&((_back_num_inactive+_pixel) < _num_pixels))
              {
                _back_num_inactive+= (((IceTRunLengthType *) (_back))[0]);
                _back_num_active   = (((IceTRunLengthType *) (_back))[1]);
                _back             += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              }
              {
                IceTSizeType _dest_num_inactive
                  = ((_front_num_inactive) < (_back_num_inactive) ? (_front_num_inactive) : (_back_num_inactive));
                if (_dest_num_inactive > 0)
                {
                  if (_dest_runlengths != ((void *) 0))
                  {
                    (((IceTRunLengthType *) (_dest_runlengths))[1])= _dest_num_active;
                    _dest_num_active                               = 0;
                  }
                  _dest_runlengths                               = _dest;
                  _dest                                         += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                  _pixel                                        += _dest_num_inactive;
                  _front_num_inactive                           -= _dest_num_inactive;
                  _back_num_inactive                            -= _dest_num_inactive;
                  (((IceTRunLengthType *) (_dest_runlengths))[0])= _dest_num_inactive;
                } else {
                  if (_dest_runlengths == ((void *) 0))
                  {
                    _dest_runlengths                               = _dest;
                    _dest                                         += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                    (((IceTRunLengthType *) (_dest_runlengths))[0])= 0;
                  }
                }
              }
              if ((0 < _front_num_inactive)&&(0 < _back_num_active))
              {
                IceTSizeType _num_to_copy
                                    = ((_front_num_inactive) < (_back_num_active) ? (_front_num_inactive) : (_back_num_active));
                _front_num_inactive-= _num_to_copy;
                _back_num_active   -= _num_to_copy;
                _dest_num_active   += _num_to_copy;
                _pixel             += _num_to_copy;
                memcpy(_dest,_back,(sizeof(IceTUInt))*_num_to_copy);
                _dest+= (sizeof(IceTUInt))*_num_to_copy;
                _back+= (sizeof(IceTUInt))*_num_to_copy;
              }
              if ((0 < _back_num_inactive)&&(0 < _front_num_active))
              {
                IceTSizeType _num_to_copy
                                   = ((_back_num_inactive) < (_front_num_active) ? (_back_num_inactive) : (_front_num_active));
                _back_num_inactive-= _num_to_copy;
                _front_num_active -= _num_to_copy;
                _dest_num_active  += _num_to_copy;
                _pixel            += _num_to_copy;
                memcpy(_dest,_front,(sizeof(IceTUInt))*_num_to_copy);
                _dest += (sizeof(IceTUInt))*_num_to_copy;
                _front+= (sizeof(IceTUInt))*_num_to_copy;
              }
              if ((_front_num_inactive == 0)&&(_back_num_inactive == 0))
              {
                IceTSizeType _num_to_composite
                                  = ((_front_num_active) < (_back_num_active) ? (_front_num_active) : (_back_num_active));
                _front_num_active-= _num_to_composite;
                _back_num_active -= _num_to_composite;
                _dest_num_active += _num_to_composite;
                _pixel           += _num_to_composite;
                for (; 0 < _num_to_composite; _num_to_composite--)
                {
                  {const IceTUInt *front_color;
                   const IceTUInt *back_color;
                   IceTUInt       *dest_color;
                   front_color= (IceTUInt *) _front;
                   _front    += sizeof(IceTUInt);
                   back_color = (IceTUInt *) _back;
                   _back     += sizeof(IceTUInt);
                   dest_color = (IceTUInt *) _dest;
                   _dest     += sizeof(IceTUInt);
                   {IceTUInt afactor= 255-((const IceTUByte *) front_color)[3];
  ((IceTUByte *) dest_color)[0]= (IceTUByte) ((((const IceTUByte *) back_color)[0]*afactor)/255+((const IceTUByte *) front_color)[0]);
  ((IceTUByte *) dest_color)[1]= (IceTUByte) ((((const IceTUByte *) back_color)[1]*afactor)/255+((const IceTUByte *) front_color)[1]);
  ((IceTUByte *) dest_color)[2]= (IceTUByte) ((((const IceTUByte *) back_color)[2]*afactor)/255+((const IceTUByte *) front_color)[2]);
  ((IceTUByte *) dest_color)[3]= (IceTUByte) ((((const IceTUByte *) back_color)[3]*afactor)/255+((const IceTUByte *) front_color)[3]);
                   }}
                }
              }
            }
            if (_dest_runlengths != ((void *) 0))
            {
              (((IceTRunLengthType *) (_dest_runlengths))[1])= _dest_num_active;
            }
            if (_pixel != _num_pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_template_body.h",164);
            }
            {
              IceTPointerArithmetic _buffer_begin
                = (IceTPointerArithmetic) ((IceTInt *) dest_buffer.opaque_internals);
              IceTPointerArithmetic _buffer_end
                = (IceTPointerArithmetic) _dest;
              IceTPointerArithmetic _compressed_size= _buffer_end-_buffer_begin;
              ((IceTInt *) dest_buffer.opaque_internals)
              [6]
                = (IceTInt) _compressed_size;
            }
          }
        } else if (_color_format == (IceTEnum) 0xC002)
        {
          {
            const IceTByte *_front;
            const IceTByte *_back;
            IceTByte       *_dest;
            IceTVoid       *_dest_runlengths;
            IceTSizeType   _num_pixels;
            IceTSizeType   _pixel;
            IceTSizeType   _front_num_inactive;
            IceTSizeType   _front_num_active;
            IceTSizeType   _back_num_inactive;
            IceTSizeType   _back_num_active;
            IceTSizeType   _dest_num_active;
            _num_pixels= icetSparseImageGetNumPixels(front_buffer);
            if (_num_pixels != icetSparseImageGetNumPixels(back_buffer))
            {
              icetRaiseDiagnostic("Input buffers do not agree for compressed-compressed" " composite.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                                  "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_template_body.h"
                                  ,60);
            }
            icetSparseImageSetDimensions(
              dest_buffer,
              icetSparseImageGetWidth(front_buffer),
              icetSparseImageGetHeight(back_buffer));
            _front             = ((IceTVoid *) &(((IceTInt *) front_buffer.opaque_internals)[7]));
            _back              = ((IceTVoid *) &(((IceTInt *) back_buffer.opaque_internals)[7]));
            _dest              = ((IceTVoid *) &(((IceTInt *) dest_buffer.opaque_internals)[7]));
            _dest_runlengths   = ((void *) 0);
            _pixel             = 0;
            _front_num_inactive= _front_num_active= 0;
            _back_num_inactive = _back_num_active= 0;
            _dest_num_active   = 0;
            while (_pixel < _num_pixels)
            {
              while ((_front_num_active == 0)
                     &&((_front_num_inactive+_pixel) < _num_pixels))
              {
                _front_num_inactive+= (((IceTRunLengthType *) (_front))[0]);
                _front_num_active   = (((IceTRunLengthType *) (_front))[1]);
                _front             += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              }
              while ((_back_num_active == 0)
                     &&((_back_num_inactive+_pixel) < _num_pixels))
              {
                _back_num_inactive+= (((IceTRunLengthType *) (_back))[0]);
                _back_num_active   = (((IceTRunLengthType *) (_back))[1]);
                _back             += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
              }
              {
                IceTSizeType _dest_num_inactive
                  = ((_front_num_inactive) < (_back_num_inactive) ? (_front_num_inactive) : (_back_num_inactive));
                if (_dest_num_inactive > 0)
                {
                  if (_dest_runlengths != ((void *) 0))
                  {
                    (((IceTRunLengthType *) (_dest_runlengths))[1])= _dest_num_active;
                    _dest_num_active                               = 0;
                  }
                  _dest_runlengths                               = _dest;
                  _dest                                         += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                  _pixel                                        += _dest_num_inactive;
                  _front_num_inactive                           -= _dest_num_inactive;
                  _back_num_inactive                            -= _dest_num_inactive;
                  (((IceTRunLengthType *) (_dest_runlengths))[0])= _dest_num_inactive;
                } else {
                  if (_dest_runlengths == ((void *) 0))
                  {
                    _dest_runlengths                               = _dest;
                    _dest                                         += ((IceTSizeType) (2*sizeof(IceTRunLengthType)));
                    (((IceTRunLengthType *) (_dest_runlengths))[0])= 0;
                  }
                }
              }
              if ((0 < _front_num_inactive)&&(0 < _back_num_active))
              {
                IceTSizeType _num_to_copy
                                    = ((_front_num_inactive) < (_back_num_active) ? (_front_num_inactive) : (_back_num_active));
                _front_num_inactive-= _num_to_copy;
                _back_num_active   -= _num_to_copy;
                _dest_num_active   += _num_to_copy;
                _pixel             += _num_to_copy;
                memcpy(_dest,_back,(4*sizeof(IceTFloat))*_num_to_copy);
                _dest+= (4*sizeof(IceTFloat))*_num_to_copy;
                _back+= (4*sizeof(IceTFloat))*_num_to_copy;
              }
              if ((0 < _back_num_inactive)&&(0 < _front_num_active))
              {
                IceTSizeType _num_to_copy
                                   = ((_back_num_inactive) < (_front_num_active) ? (_back_num_inactive) : (_front_num_active));
                _back_num_inactive-= _num_to_copy;
                _front_num_active -= _num_to_copy;
                _dest_num_active  += _num_to_copy;
                _pixel            += _num_to_copy;
                memcpy(_dest,_front,(4*sizeof(IceTFloat))*_num_to_copy);
                _dest += (4*sizeof(IceTFloat))*_num_to_copy;
                _front+= (4*sizeof(IceTFloat))*_num_to_copy;
              }
              if ((_front_num_inactive == 0)&&(_back_num_inactive == 0))
              {
                IceTSizeType _num_to_composite
                                  = ((_front_num_active) < (_back_num_active) ? (_front_num_active) : (_back_num_active));
                _front_num_active-= _num_to_composite;
                _back_num_active -= _num_to_composite;
                _dest_num_active += _num_to_composite;
                _pixel           += _num_to_composite;
                for (; 0 < _num_to_composite; _num_to_composite--)
                {
                  {const IceTFloat *front_color;
                   const IceTFloat *back_color;
                   IceTFloat       *dest_color;
                   front_color= (IceTFloat *) _front;
                   _front    += 4*sizeof(IceTUInt);
                   back_color = (IceTFloat *) _back;
                   _back     += 4*sizeof(IceTUInt);
                   dest_color = (IceTFloat *) _dest;
                   _dest     += 4*sizeof(IceTUInt);
                   {IceTFloat afactor= 1.0f-(front_color)[3];
  (dest_color)[0]= (back_color)[0]*afactor+(front_color)[0];
  (dest_color)[1]= (back_color)[1]*afactor+(front_color)[1];
  (dest_color)[2]= (back_color)[2]*afactor+(front_color)[2];
  (dest_color)[3]= (back_color)[3]*afactor+(front_color)[3];
                   }}
                }
              }
            }
            if (_dest_runlengths != ((void *) 0))
            {
              (((IceTRunLengthType *) (_dest_runlengths))[1])= _dest_num_active;
            }
            if (_pixel != _num_pixels)
            {
              icetRaiseDiagnostic("Corrupt compressed image.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,"/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_template_body.h",164);
            }
            {
              IceTPointerArithmetic _buffer_begin
                = (IceTPointerArithmetic) ((IceTInt *) dest_buffer.opaque_internals);
              IceTPointerArithmetic _buffer_end
                = (IceTPointerArithmetic) _dest;
              IceTPointerArithmetic _compressed_size= _buffer_end-_buffer_begin;
              ((IceTInt *) dest_buffer.opaque_internals)
              [6]
                = (IceTInt) _compressed_size;
            }
          }
        } else if (_color_format == (IceTEnum) 0xC000)
        {
          icetRaiseDiagnostic("Compositing image with no data.",(IceTEnum) 0xFFFFFFFB,(IceTEnum) 0x0003,
                              "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_func_body.h"
                              ,218);
          icetClearSparseImage(dest_buffer);
        } else {
          icetRaiseDiagnostic("Encountered invalid color format.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                              "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_func_body.h"
                              ,222);
        }
      } else {
        icetRaiseDiagnostic("Cannot use blend composite with a depth buffer.",(IceTEnum) 0xFFFFFFFA,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_func_body.h"
                            ,226);
      }
    } else {
      icetRaiseDiagnostic("Encountered invalid composite mode.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                          "/nfshome/lmlediae/git/icet/src/ice-t/cc_composite_func_body.h"
                          ,230);
    }
  }
  icetTimingBlendEnd();
}

void icetImageCorrectBackground(IceTImage image)
{
  IceTBoolean  need_correction;
  IceTSizeType num_pixels;
  IceTEnum     color_format;
  icetGetBooleanv(((IceTEnum) 0x00000080|(IceTEnum) 0x000C),&need_correction);
  if (!need_correction){return;}
  num_pixels  = icetImageGetNumPixels(image);
  color_format= icetImageGetColorFormat(image);
  icetTimingBlendBegin();
  if (color_format == (IceTEnum) 0xC001)
  {
    IceTUByte    *color= icetImageGetColorub(image);
    IceTInt      background_color_word;
    IceTUByte    *bc;
    IceTSizeType p;
    icetGetIntegerv(((IceTEnum) 0x00000080|(IceTEnum) 0x000E),
                    &background_color_word);
    bc= (IceTUByte *) (&background_color_word);
    for (p= 0; p < num_pixels; p++)
    {
      {IceTUInt afactor= 255-(color)[3];
       (color)[0]= (IceTUByte) (((bc)[0]*afactor)/255+(color)[0]);
       (color)[1]= (IceTUByte) (((bc)[1]*afactor)/255+(color)[1]);
       (color)[2]= (IceTUByte) (((bc)[2]*afactor)/255+(color)[2]);
       (color)[3]= (IceTUByte) (((bc)[3]*afactor)/255+(color)[3]);
      }
      color+= 4;
    }
  } else if (color_format == (IceTEnum) 0xC002)
  {
    IceTFloat    *color= icetImageGetColorf(image);
    IceTFloat    background_color[4];
    IceTSizeType p;
    icetGetFloatv(((IceTEnum) 0x00000080|(IceTEnum) 0x000D),background_color);
    for (p= 0; p < num_pixels; p++)
    {
      {IceTFloat afactor= 1.0f-(color)[3];
       (color)[0]= (background_color)[0]*afactor+(color)[0];
       (color)[1]= (background_color)[1]*afactor+(color)[1];
       (color)[2]= (background_color)[2]*afactor+(color)[2];
       (color)[3]= (background_color)[3]*afactor+(color)[3];
      }
      color+= 4;
    }
  } else {
    icetRaiseDiagnostic("Encountered invalid color buffer type" " with color blending.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                        "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                        ,2532);
  }
  icetTimingBlendEnd();
}

void icetClearImageTrueBackground(IceTImage image)
{
  IceTFloat true_background[4];
  IceTInt   true_background_word;
  IceTFloat original_background[4];
  IceTInt   original_background_word;
  icetGetFloatv(((IceTEnum) 0x00000080|(IceTEnum) 0x000D),true_background);
  icetGetIntegerv(((IceTEnum) 0x00000080|(IceTEnum) 0x000E),&true_background_word);
  icetGetFloatv(((IceTEnum) 0x00000000|(IceTEnum) 0x0005),original_background);
  icetGetIntegerv(((IceTEnum) 0x00000000|(IceTEnum) 0x0006),&original_background_word);
  icetStateSetFloatv(((IceTEnum) 0x00000000|(IceTEnum) 0x0005),4,true_background);
  icetStateSetInteger(((IceTEnum) 0x00000000|(IceTEnum) 0x0006),true_background_word);
  icetClearImage(image);
  icetStateSetFloatv(((IceTEnum) 0x00000000|(IceTEnum) 0x0005),4,original_background);
  icetStateSetInteger(((IceTEnum) 0x00000000|(IceTEnum) 0x0006),original_background_word);
}



static IceTImage generateTile(int tile,
                              IceTInt *screen_viewport,
                              IceTInt *target_viewport,
                              IceTImage tile_buffer)
{


    IceTInt r=-1;
    icetGetIntegerv(ICET_RANK, &r);
  printf("\n+ [%d] generateTile\n", r);


  IceTBoolean use_prerender;
  icetGetBooleanv(((IceTEnum) 0x00000080|(IceTEnum) 0x0022),&use_prerender);
  if (use_prerender)
  {
    return prerenderedTile(tile,screen_viewport,target_viewport);
  } else {
    return renderTile(tile,screen_viewport,target_viewport,tile_buffer);
  }
}

static IceTImage renderTile(int tile,
                            IceTInt *screen_viewport,
                            IceTInt *target_viewport,
                            IceTImage tile_buffer)
{
  printf("renderTile \n\n");
  const IceTInt        *contained_viewport;
  const IceTInt        *tile_viewport;
  const IceTBoolean    *contained_mask;
  IceTInt              physical_width,physical_height;
  IceTBoolean          use_floating_viewport;
  IceTDrawCallbackType drawfunc;
  IceTVoid             *value;
  IceTInt              readback_viewport[4];
  IceTImage            render_buffer;
  IceTDouble           projection_matrix[16];
  IceTDouble           modelview_matrix[16];
  IceTFloat            background_color[4];
  {char msg_string[256];
   sprintf(msg_string,"Rendering tile %d",tile);
   icetRaiseDiagnostic(msg_string,(IceTEnum) 0x00000000,(IceTEnum) 0x0007,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",2599);
  }
  contained_viewport   = icetUnsafeStateGetInteger(((IceTEnum) 0x00000080|(IceTEnum) 0x0003));
  tile_viewport        = icetUnsafeStateGetInteger(((IceTEnum) 0x00000000|(IceTEnum) 0x0011))+4*tile;
  contained_mask       = icetUnsafeStateGetBoolean(((IceTEnum) 0x00000080|(IceTEnum) 0x0008));
  use_floating_viewport= icetIsEnabled(((IceTEnum) 0x00000140|(IceTEnum) 0x0001));
  icetGetIntegerv(((IceTEnum) 0x00000000|(IceTEnum) 0x0007),&physical_width);
  icetGetIntegerv(((IceTEnum) 0x00000000|(IceTEnum) 0x0008),&physical_height);
  {char msg_string[256];
   sprintf(msg_string,"contained viewport: %d %d %d %d",(int) contained_viewport[0],(int) contained_viewport[1],(int) contained_viewport[2],(int) contained_viewport[3]);
   icetRaiseDiagnostic(msg_string,(IceTEnum) 0x00000000,(IceTEnum) 0x0007,
                       "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                       ,2610
                       );
  }
  {char msg_string[256];
   sprintf(msg_string,"tile viewport: %d %d %d %d",(int) tile_viewport[0],(int) tile_viewport[1],(int) tile_viewport[2],(int) tile_viewport[3]);
   icetRaiseDiagnostic(msg_string,(IceTEnum) 0x00000000,(IceTEnum) 0x0007,
                       "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                       ,2613
                       );
  }
  render_buffer= tile_buffer;
  if (!contained_mask[tile]
      ||(contained_viewport[0]+contained_viewport[2] < tile_viewport[0])
      ||(contained_viewport[1]+contained_viewport[3] < tile_viewport[1])
      ||(contained_viewport[0] > tile_viewport[0]+tile_viewport[2])
      ||(contained_viewport[1] > tile_viewport[1]+tile_viewport[3]))
  {
    icetRaiseDiagnostic("Case 0: geometry completely outside tile.",(IceTEnum) 0x00000000,(IceTEnum) 0x0007,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",2623);
    readback_viewport[0]= screen_viewport[0]= target_viewport[0]= 0;
    readback_viewport[1]= screen_viewport[1]= target_viewport[1]= 0;
    readback_viewport[2]= screen_viewport[2]= target_viewport[2]= 0;
    readback_viewport[3]= screen_viewport[3]= target_viewport[3]= 0;
    if (!icetIsEnabled(((IceTEnum) 0x00000140|(IceTEnum) 0x0007)))
    {
      return tile_buffer;
    } else {
      icetProjectTile(tile,projection_matrix);
    }
  } else if ((contained_viewport[0] >= tile_viewport[0])
             &&(contained_viewport[1] >= tile_viewport[1])
             &&(contained_viewport[2]+contained_viewport[0]
                <= tile_viewport[2]+tile_viewport[0])
             &&(contained_viewport[3]+contained_viewport[1]
                <= tile_viewport[3]+tile_viewport[1]))
  {
    icetRaiseDiagnostic("Case 1: geometry fits entirely within tile.",(IceTEnum) 0x00000000,(IceTEnum) 0x0007,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",2643);
    icetProjectTile(tile,projection_matrix);
    icetStateSetIntegerv(((IceTEnum) 0x00000080|(IceTEnum) 0x0020),4,tile_viewport);
    screen_viewport[0]        = target_viewport[0]
                              = contained_viewport[0]-tile_viewport[0];
    screen_viewport[1]        = target_viewport[1]
                              = contained_viewport[1]-tile_viewport[1];
    screen_viewport[2]        = target_viewport[2]= contained_viewport[2];
    screen_viewport[3]        = target_viewport[3]= contained_viewport[3];
    readback_viewport[0]      = screen_viewport[0];
    readback_viewport[1]      = screen_viewport[1];
    readback_viewport[2]      = screen_viewport[2];
    readback_viewport[3]      = screen_viewport[3];
  } else if (!use_floating_viewport
             ||(contained_viewport[2] > physical_width)
             ||(contained_viewport[3] > physical_height))
  {
    icetRaiseDiagnostic("Case 2: Can't use floating viewport.",(IceTEnum) 0x00000000,(IceTEnum) 0x0007,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",2664);
    icetProjectTile(tile,projection_matrix);
    icetStateSetIntegerv(((IceTEnum) 0x00000080|(IceTEnum) 0x0020),4,tile_viewport);
    if (contained_viewport[0] <= tile_viewport[0])
    {
      screen_viewport[0]        = target_viewport[0]= 0;
      screen_viewport[2]        = target_viewport[2]
                                = ((tile_viewport[2]) < (contained_viewport[0]+contained_viewport[2]-tile_viewport[0]) ? (tile_viewport[2]) : (contained_viewport[0]+contained_viewport[2]-tile_viewport[0]))
      ;
    } else {
      screen_viewport[0]        = target_viewport[0]
                                = contained_viewport[0]-tile_viewport[0];
      screen_viewport[2]        = target_viewport[2]
                                = ((contained_viewport[2]) < (tile_viewport[0]+tile_viewport[2]-contained_viewport[0]) ? (contained_viewport[2]) : (tile_viewport[0]+tile_viewport[2]-contained_viewport[0]))
      ;
    }
    if (contained_viewport[1] <= tile_viewport[1])
    {
      screen_viewport[1]        = target_viewport[1]= 0;
      screen_viewport[3]        = target_viewport[3]
                                = ((tile_viewport[3]) < (contained_viewport[1]+contained_viewport[3]-tile_viewport[1]) ? (tile_viewport[3]) : (contained_viewport[1]+contained_viewport[3]-tile_viewport[1]))
      ;
    } else {
      screen_viewport[1]        = target_viewport[1]
                                = contained_viewport[1]-tile_viewport[1];
      screen_viewport[3]        = target_viewport[3]
                                = ((contained_viewport[3]) < (tile_viewport[1]+tile_viewport[3]-contained_viewport[1]) ? (contained_viewport[3]) : (tile_viewport[1]+tile_viewport[3]-contained_viewport[1]))
      ;
    }
    readback_viewport[0]= screen_viewport[0];
    readback_viewport[1]= screen_viewport[1];
    readback_viewport[2]= screen_viewport[2];
    readback_viewport[3]= screen_viewport[3];
  } else {
    IceTDouble viewport_project_matrix[16];
    IceTDouble global_projection_matrix[16];
    IceTInt    rendered_viewport[4];
    icetRaiseDiagnostic("Case 3: Using floating viewport.",(IceTEnum) 0x00000000,(IceTEnum) 0x0007,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",2707);
    rendered_viewport[0]= contained_viewport[0];
    rendered_viewport[1]= contained_viewport[1];
    rendered_viewport[2]= physical_width;
    rendered_viewport[3]= physical_height;
    readback_viewport[0]= 0;
    readback_viewport[1]= 0;
    readback_viewport[2]= contained_viewport[2];
    readback_viewport[3]= contained_viewport[3];
    if (contained_viewport[0] < tile_viewport[0])
    {
      screen_viewport[0]= tile_viewport[0]-contained_viewport[0];
      screen_viewport[2]= ((contained_viewport[2]-screen_viewport[0]) < (tile_viewport[2]) ? (contained_viewport[2]-screen_viewport[0]) : (tile_viewport[2]))
      ;
      target_viewport[0]= 0;
      target_viewport[2]= screen_viewport[2];
    } else {
      target_viewport[0]= contained_viewport[0]-tile_viewport[0];
      target_viewport[2]= ((tile_viewport[2]-target_viewport[0]) < (contained_viewport[2]) ? (tile_viewport[2]-target_viewport[0]) : (contained_viewport[2]))
      ;
      screen_viewport[0]= 0;
      screen_viewport[2]= target_viewport[2];
    }
    if (contained_viewport[1] < tile_viewport[1])
    {
      screen_viewport[1]= tile_viewport[1]-contained_viewport[1];
      screen_viewport[3]= ((contained_viewport[3]-screen_viewport[1]) < (tile_viewport[3]) ? (contained_viewport[3]-screen_viewport[1]) : (tile_viewport[3]))
      ;
      target_viewport[1]= 0;
      target_viewport[3]= screen_viewport[3];
    } else {
      target_viewport[1]= contained_viewport[1]-tile_viewport[1];
      target_viewport[3]= ((tile_viewport[3]-target_viewport[1]) < (contained_viewport[3]) ? (tile_viewport[3]-target_viewport[1]) : (contained_viewport[3]))
      ;
      screen_viewport[1]= 0;
      screen_viewport[3]= target_viewport[3];
    }
    render_buffer= getRenderBuffer();
    if (icetStateGetTime(((IceTEnum) 0x00000080|(IceTEnum) 0x0020))
        > icetStateGetTime(((IceTEnum) 0x00000080|(IceTEnum) 0x0000)))
    {
      const IceTInt *old_rendered_viewport
        = icetUnsafeStateGetInteger(((IceTEnum) 0x00000080|(IceTEnum) 0x0020));
      IceTBoolean   old_rendered_viewport_valid
        = ((old_rendered_viewport[0] == rendered_viewport[0])
           ||(old_rendered_viewport[1] == rendered_viewport[1])
           ||(old_rendered_viewport[2] == rendered_viewport[2])
           ||(old_rendered_viewport[3] == rendered_viewport[3]));
      if (!old_rendered_viewport_valid)
      {
        icetRaiseDiagnostic("Rendered floating viewport became invalidated.",(IceTEnum) 0xFFFFFFFF,(IceTEnum) 0x0001,
                            "/nfshome/lmlediae/git/icet/src/ice-t/image.c"
                            ,2769);
      } else {
        icetRaiseDiagnostic("Already rendered floating viewport.",(IceTEnum) 0x00000000,(IceTEnum) 0x0007,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",2771);
        return render_buffer;
      }
    }
    icetStateSetIntegerv(((IceTEnum) 0x00000080|(IceTEnum) 0x0020),4,rendered_viewport);
    icetGetViewportProject(rendered_viewport[0],rendered_viewport[1],
                           rendered_viewport[2],rendered_viewport[3],
                           viewport_project_matrix);
    icetGetDoublev(((IceTEnum) 0x00000080|(IceTEnum) 0x0001),global_projection_matrix);
    icetMatrixMultiply(projection_matrix,
                       viewport_project_matrix,
                       global_projection_matrix);
  }
  if ((icetImageGetWidth(render_buffer) != physical_width)
      ||(icetImageGetHeight(render_buffer) != physical_height))
  {
    render_buffer= getRenderBuffer();
  }
  icetGetDoublev(((IceTEnum) 0x00000080|(IceTEnum) 0x0002),modelview_matrix);
  icetGetFloatv(((IceTEnum) 0x00000000|(IceTEnum) 0x0005),background_color);
  icetGetPointerv(((IceTEnum) 0x00000000|(IceTEnum) 0x0060),&value);
  drawfunc= (IceTDrawCallbackType) value;
  icetRaiseDiagnostic("Calling draw function.",(IceTEnum) 0x00000000,(IceTEnum) 0x0007,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",2802);
  icetTimingRenderBegin();
  (*drawfunc)(projection_matrix,modelview_matrix,background_color,
              readback_viewport,render_buffer);
  icetTimingRenderEnd();
  return render_buffer;
}

static IceTImage prerenderedTile(int tile,
                                 IceTInt *screen_viewport,
                                 IceTInt *target_viewport)
{
  const IceTInt *contained_viewport;
  const IceTInt *tile_viewport;
  {char msg_string[256];
   sprintf(msg_string,"Getting viewport for tile %d in prerendered image",tile);
   icetRaiseDiagnostic(msg_string,(IceTEnum) 0x00000000,(IceTEnum) 0x0007,"/nfshome/lmlediae/git/icet/src/ice-t/image.c",2818);
  }
  contained_viewport= icetUnsafeStateGetInteger(((IceTEnum) 0x00000080|(IceTEnum) 0x0003));
  tile_viewport     = icetUnsafeStateGetInteger(((IceTEnum) 0x00000000|(IceTEnum) 0x0011))+4*tile;
  icetIntersectViewports(tile_viewport,contained_viewport,screen_viewport);
  target_viewport[0]= screen_viewport[0]-tile_viewport[0];
  target_viewport[1]= screen_viewport[1]-tile_viewport[1];
  target_viewport[2]= screen_viewport[2];
  target_viewport[3]= screen_viewport[3];
  return icetRetrieveStateImage(((IceTEnum) 0x00000080|(IceTEnum) 0x0021));
}

static IceTImage getRenderBuffer(void)
{
  if (icetStateGetTime(((IceTEnum) 0x00000080|(IceTEnum) 0x0021))
      > icetStateGetTime(((IceTEnum) 0x00000080|(IceTEnum) 0x0000)))
  {
    return icetRetrieveStateImage(((IceTEnum) 0x00000080|(IceTEnum) 0x0021));
  } else {
    IceTInt dim[2];
    icetGetIntegerv(((IceTEnum) 0x00000000|(IceTEnum) 0x0007),&dim[0]);
    icetGetIntegerv(((IceTEnum) 0x00000000|(IceTEnum) 0x0008),&dim[1]);
    return icetGetStateBufferImage(((IceTEnum) 0x00000080|(IceTEnum) 0x0021),dim[0],dim[1]);
  }
}
